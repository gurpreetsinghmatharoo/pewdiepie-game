/// @func data_save
/// @desc Saves variables to a buffer
/// @arg <strings>

/// "string" for variable
/// "|string" for list
/// "?string" for map
/// "&string" for object

//Init
var buff = buffer_create(1024, buffer_grow, 1);

//Characters (Move to macros)
var charList = "|";
var charMap = "?";
var charObj = "&";

//Loop
for(var i=0; i<argument_count; i++){
	//Get string
	var str = argument[i];
	var strLen = string_length(str);
	var firstChar = string_copy(str, 1, 1);
	
	var varStr = str;
	if (firstChar==charList || firstChar==charMap || firstChar==charObj) varStr = string_copy(str, 2, strLen-1);
	var varInst = id;
	
	//Object
	if (firstChar==charObj){
		var variable = asset_get_index(varStr);
	}
	else{
		//Loop to check for period
		for(var c=1; c<=string_length(varStr); c++){
			var char = string_copy(varStr, c, 1);
			
			if (char=="."){
				//Get instance
				varInst = string_copy(varStr, 1, char-1);
				
				//Global
				if (varInst == "global"){
					varInst = -1;
				}
				else{
					varInst = asset_get_index(varInst);
				}
				
				//Get variable
				varStr = string_copy(varStr, char, string_length(varStr)-char);
				
				//Break
				break;
			}
		}
		
		//Get variable value
		var variable;
		if (varInst==-1){
			variable = variable_global_get(varStr);
		}
		else{
			variable = variable_instance_get(varInst, varStr);
		}
	}
	
	//Save to buffer
	switch(firstChar){
		//List
		case charList:
			buffer_write(buff, buffer_string, ds_list_write(variable));
		break;
		
		//Map
		case charMap:
			buffer_write(buff, buffer_string, ds_map_write(variable));
		break;
		
		//Object
		case charObj:
			for(var j; j<instance_number(variable); j++){
				var inst = instance_find(variable, j);
				buffer_write(buff, buffer_s16, inst.x);
				buffer_write(buff, buffer_s16, inst.y);
			}
		break;
		
		//Variable
		case default:
			buffer_write(buff, buffer_s16, variable);
		break;
	}
}

//Save
buffer_save(buff, "save.buff");
buffer_delete(buff);