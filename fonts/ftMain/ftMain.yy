{
    "id": "2b862cb6-7c0a-4d12-a03e-aa5179d5ffb8",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "ftMain",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Joystix",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d6f8f160-502e-4f8b-9637-17226dfad0e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ea335ac6-2c7a-47aa-ba35-a5d74cbd6c72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 13,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 101,
                "y": 77
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "377ce1dc-a2df-47f2-814e-65274f5e970b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 13,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 110,
                "y": 77
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cfc36ac9-a012-4a1d-bd9b-40cc67458724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 13,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 47,
                "y": 92
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "55b4da6a-dbf9-465c-b36b-be2aafbc225c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "db854b6b-9dc0-44cf-af85-510b2a25fe3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 56,
                "y": 92
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d4f687b9-fe35-4291-8956-5ae976dae4d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 62
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6e72e04a-5a2a-4f4b-a3b1-cd68fb91e7ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 13,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 76,
                "y": 107
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "738d6276-efde-45e1-86eb-ae9fdfc37140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 13,
                "offset": 4,
                "shift": 9,
                "w": 5,
                "x": 29,
                "y": 107
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "14a80ce7-1ef8-4ce0-be03-05ddf26314ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 13,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 64,
                "y": 107
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1ef1d1b9-6de7-48e3-81b2-3546609ceed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "826fd569-2129-47b8-8f4c-0abeb6cc7fe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 92
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "472a58dc-07fd-412b-98a9-59c68adb310a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 91,
                "y": 107
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "cf5115a2-88f5-4291-b6a4-7c0e475236e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 13,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d46f9f7c-c36a-4c12-b818-883abd3d109f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 13,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 96,
                "y": 107
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6ac945bc-fb77-4fb8-ac45-3ab37ba64634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "eb418793-5325-4621-b886-c8faca065d78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c735b9e3-0a3c-4caf-b3e7-b0f6d9bfe803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "46e2321d-52e7-4bce-916c-04417b0709fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 47
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d7ac5ecf-1cbe-4298-82cc-2120464a4652",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 47
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a8d49e63-883a-47f8-b3bc-dd02958ea0a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "66dbaa48-e766-4691-9757-dad447749a8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 47
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1df8ee5c-751b-4d57-adc8-473b12123573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "13f08538-7ba4-4940-9a35-0517e5940cc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 62
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "76af4032-149e-40c8-a34f-fa425339c73a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 62
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9a5248ed-a9ef-4cd8-82de-fd3946d57dcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e9da3f4b-7cf6-4456-87e3-85fae3bf751d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 13,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 81,
                "y": 107
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2a42ad05-2ed6-4d8d-abdb-782613d8a766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 86,
                "y": 107
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "dbd5e3c3-2c8b-41fb-af85-c33c65327800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 110,
                "y": 92
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "fc99caff-d172-4a6b-91b6-5c2895f096ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 101,
                "y": 92
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5e817cd9-1074-4951-b675-ed3d228044f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 92,
                "y": 92
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c0a1ee3e-efb7-4ea0-a659-6aba5d48e550",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 77
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7d66dc49-c797-4554-a7b5-b86eeabb5516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 77
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b87d527c-d684-45f1-b029-e3eb74d9265e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "bad08145-8a9a-427a-a7e4-661567339df3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 77
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "49801e9d-111b-40c4-8162-518e0fcd67d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 77
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "6f7e8ed2-336a-4f12-aa0f-62172bae83da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 77
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "56068ec8-076f-45bd-996e-0ef678fa1c53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 77
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b44346bb-9c44-4bea-b1de-d38839849fab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 77
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f2b18188-095f-4435-8261-aeff158fe9fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 77
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ce14f72b-e83b-40bc-b2aa-3761bdafc010",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "15006739-57d9-433b-864b-5a90eee3161a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 65,
                "y": 92
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "8c553fb3-58ff-45c5-bca7-0df791a79eca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 47
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3af45cad-9ce1-4648-9ad2-d5fbd669d6d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 47
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1977cdb7-8efc-4fd9-96a7-0e7fe8f74b93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 17
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e50252a9-8577-478f-856c-e3de2a4d7a29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 17
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "aba7d61d-6f0e-4fc7-976f-e90463004600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 17
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a839e069-7730-40a4-842e-640e21b5930e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 47
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2c67afed-b3a6-44c1-8a0d-11a21cc3db35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 17
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "523506c3-b679-4f3d-bba4-36701562a86d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 17
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4907953d-b6a4-4ff0-82b7-8fb5a61ba54f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 17
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a2cc9646-a8b6-4d09-8272-1c041cc14b81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "14d468f3-f488-42ea-a53e-8799f7992df5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 83,
                "y": 92
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "edd07c8c-6878-407a-9305-9c353e92a005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 17
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a700b5f7-c077-4765-8ff4-ae71c8e502d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a0cf7265-346e-4b4f-be5f-ab97193e38fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "38634847-58a7-4698-839d-2a063dcd3048",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "91f1bde5-7ef1-4de2-996b-b36a6af7a5ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 74,
                "y": 92
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f5a16b73-60ad-4fd1-8996-66c423bdc8c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7d952e69-ae5c-4ebb-a6a6-37f19bd3aa57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 13,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 36,
                "y": 107
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f01f1c9d-b7c0-4f4f-8e41-4ab1ce82f8b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ca1f7f13-4580-4c39-b134-aaefb128927a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 43,
                "y": 107
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1fae4544-f74e-4846-af01-5cb873795b2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 13,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 107
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4935a27e-0dc2-4abe-a711-edd4f851d669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "bb17650e-0189-4599-bf40-f71b141363b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 13,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 70,
                "y": 107
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "af11b725-c3e5-4194-afef-b79f9cd0eeb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3e3737fd-0b47-4a1f-89a3-443e62bec090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "697eddfc-1971-41ca-826f-70d66aa242e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5f277982-bbbf-4326-81b8-5d8ecea24609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 17
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "697180aa-2944-4425-92a0-aecf44680aed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 17
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5ede2209-7b0c-4d57-b8c3-56b2aefbd86a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 17
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "cec511e9-3825-4f02-bf97-7674f7a3ad0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 47
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a1e23e37-2219-4335-88c1-c889a568579d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 47
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d374886b-de14-40fa-84ac-b8af91a28d70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 92
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "31dc173c-81e0-413e-8ea1-f9be434c6092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 47
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ba505620-1894-4978-83d7-4e0064e54a89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 47
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "726e3451-5977-47c5-89f6-7938233fd19f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "cbf5dedc-038c-41f0-8794-1857ff1d79f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 32
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8310beeb-8e3d-4bdf-95c0-3c839d40ffb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 32
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "7c9bef80-5f4f-49e9-945c-444ff8779083",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "71e43eee-4502-4922-a9ff-b173a44b5cde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c5084c12-3b0a-4089-a48f-6ee161dfa0eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "fbeba602-8dff-48d4-a894-4ec34df1f66a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "42773f22-38f6-46a3-a7dd-149a26b6c5ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 32
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "44f49b28-30b6-4501-a51c-ea98e06e0d80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 92
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d9833e87-d65b-48ac-aa86-fd96ee053103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "beef3ace-54a9-4880-9a90-33c1672e39fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "03d6f81e-b40c-4bcf-b383-e2e500b4788d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 32
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "face0408-e389-4325-924b-ffc09b72f4f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1ebff854-e374-46e5-ae25-575b40af54ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 92
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "600ac6b3-2401-42e1-ac29-3be7cbb617ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 17
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ac02f0f7-a037-494a-b8aa-8d34a4c77726",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 13,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 57,
                "y": 107
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c3bec2c5-0b8a-4f4a-9ac7-5991dbb6f416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 13,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 101,
                "y": 107
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ade13a8e-107c-4543-88ef-c092c5cabd55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 50,
                "y": 107
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b687cc59-7a4a-47bc-9c1e-4d116baf76ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 107
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 8,
    "styleName": "Monospace",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}