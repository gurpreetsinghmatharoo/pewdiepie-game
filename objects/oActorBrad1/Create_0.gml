/// @description 
event_inherited();

//Sprites
sprite[st.idle] = sBrad1_Idle;
sprite[st.move] = sBrad1_Move;
sprite[st.hurt] = sBrad1_Hurt;

defaultMask = sprite[st.idle];
mask_index = defaultMask;