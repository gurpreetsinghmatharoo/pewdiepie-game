{
    "id": "7daca8bb-33a2-4c31-aa08-bc8e45ad0845",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oActorEdgar",
    "eventList": [
        {
            "id": "14690cf7-6e2c-43b4-a88a-a6f76e433771",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7daca8bb-33a2-4c31-aa08-bc8e45ad0845"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4698213c-a7f1-4bc4-b49d-4db42d357a5f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
    "visible": true
}