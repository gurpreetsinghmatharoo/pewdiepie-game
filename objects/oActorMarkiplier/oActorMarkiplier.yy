{
    "id": "f887ef28-6cc8-4f6c-b4b3-1781d85aacb0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oActorMarkiplier",
    "eventList": [
        {
            "id": "cf259f41-1414-40c7-ad11-ee8385913503",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f887ef28-6cc8-4f6c-b4b3-1781d85aacb0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4698213c-a7f1-4bc4-b49d-4db42d357a5f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d511615f-ed66-431b-816f-626802de6662",
    "visible": true
}