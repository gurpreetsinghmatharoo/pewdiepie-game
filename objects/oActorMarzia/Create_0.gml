/// @description 
event_inherited();

//Sprites
sprite[st.idle] = sMarzia_Idle;
sprite[st.move] = sMarzia_Move;
sprite[st.alert] = sMarzia_Alert;

defaultMask = sprite[st.idle];
mask_index = defaultMask;