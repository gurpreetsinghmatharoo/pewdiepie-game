{
    "id": "26a84d9d-61e0-478e-923e-b4329f9cce21",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oActorMarzia",
    "eventList": [
        {
            "id": "1451bad5-b331-43c4-b0bb-5ea646fb1438",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "26a84d9d-61e0-478e-923e-b4329f9cce21"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4698213c-a7f1-4bc4-b49d-4db42d357a5f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "24d9f45e-fdde-4529-81c7-603846c2a5b2",
    "visible": true
}