/// @description 
//Textbox scale
if (talk){
	tbScaleY = lerp(tbScaleY, 1, 0.1);
}
else{
	tbScaleY = lerp(tbScaleY, 0, 0.2);
}

//Draw textbox
if (tbScaleY > 0.05){
	var tbState = msg_dynamic_multi(msg, global.textButton, spr_msg_grid, spr_msg_arrow, spr_msg_more, tbScaleY);
	
	//Line end
	if (tbState==1){
		lineCD++;
		
		if (lineCD > lineWait){
			line++;
			lineCD = 0;
			char = 0;
		}
	}
	//Message end
	else if (tbState==2){
		msgCD++;
		
		if (msgCD > msgWait){
			msgDone = true;
			msgCD = 0;
			
			if (sayAsync){
				msgDone = false;
				sayAsync = false;
				talk = false;
				lineCD = 0;
				msg_stop();
			}
		}
	}
}
else{
	msg_stop();
}

//Debug
if (global.debug>=2){
	draw_values(x, y-64, msgDone, "msgDone");
}