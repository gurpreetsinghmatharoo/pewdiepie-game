/// @description 
//Vars
groundedPrev = grounded;
grounded = collision(0, 1);

//States
switch(state){
	//Normal
	case st.idle: case st.move: case st.jump: case st.push: case st.walkMarzia:
		//Follow
		if (object_index==oActorEdgar && instance_exists(oActorPoods)){
			var diff = oActorPoods.x - x;
			
			if (abs(diff) > 32){
				hor = sign(diff);
			}
			else{
				hor = 0;
			}
		}
	
		//Movement
		hsp = lerp(hsp, hor, 0.1);
		
		//Jump
		var jDetect = 8;
		if (collision(hsp*jDetect, 0) && !collision(hsp*jDetect, -jumpSpeed * 8)){
			vsp = -jumpSpeed;
			
			scale_set(1, 1.2, 30);
		}
		
		//Functions
		playerGravity();
		playerCollisions();
		
		//Apply
		x += hsp;
		y += vsp;
		travelX += hsp;
		travelY += vsp;
		
		//Animation
		if (state!=st.walkMarzia){
			if (!grounded){
				state = st.jump;
				if (abs(hsp) && !faceForced) image_xscale = sign(hsp);
			}
			else if (abs(hsp)){
				//Get collision
				var col = collision(sign(hsp), 0);
			
				//Pushing
				if (col>1 && object_get_parent(col.object_index)==oPushableParent){
					state = st.push;
				}
				//Move
				else{
					state = st.move;
				}
			
				if (!faceForced) image_xscale = sign(hsp);
			}
			else state = st.idle;
		}
		else{
			//Flip
			if (abs(hsp)) image_xscale = sign(hsp);
		}
	break;
	
	//Pick marzia
	case st.pickMarzia:
		//Remove Marzia actor
		if (floor(image_index)==1){
			instance_destroy(oActorMarzia);
		}
		
		//End
		if (animation_end()){
			state_set(st.walkMarzia);
		}
	break;
	
	//Chair enter
	case st.chairenter:
		if (animation_end()) state_set(st.chair);
	break;
	
	//Chair exit
	case st.chairexit:
		if (animation_end()){
			state_set(st.idle);
			image_speed = 1;
		}
	break;
	
	//Chair
	case st.chair: case st.chairdown: case st.chairup: case st.chairlie:
		//Movement
		hsp = lerp(hsp, hor*1.3, 0.1);
	
		//Gravity
		playerGravity();

		//Jump
		var jDetect = 8;
		if (collision(hsp*jDetect, 0) && !collision(hsp*jDetect, -jumpSpeed * 8)){
			vsp = -jumpSpeed;
			
			scale_set(1, 1.2, 30);
		}
		
		//Hit ground scale
		if (grounded && !groundedPrev){
			scale_set(1.4, 0.6, 5);
		}

		//Collisions
		enemyInteraction();
		playerCollisions();

		//Apply
		x += hsp;
		y += vsp;
		travelX += hsp;
		travelY += vsp;
		
		//State Move
		if ((state==st.chair || state==st.chairup) && abs(hsp)){
			state_set(st.chairdown);
		}
		else if (state==st.chairdown && animation_end()){
			state_set(st.chairlie);
		}
		
		//State Stop
		if ((state==st.chairlie || state==st.chairdown) && !abs(hsp)){
			state_set(st.chairup);
			image_index = sprite_get_number(sprite[st.chairup])-2;
			image_speed = -1;
		}
		else if (state==st.chairup && animation_end()){
			state_set(st.chair);
			image_speed = 1;
		}
		
		//Scale
		image_xscale = abs(hsp) ? sign(hsp) : image_xscale;
		
		chairParticles();
	break;
}

//Sprite
sprite_index = sprite[state];
		
//Move async
if (moveAsync && abs(travelX) >= abs(targetXMove)){
	hor = 0;
	travelX = 0;
	travelY = 0;
	targetXMove = 0;
	targetYMove = 0;
	moveAsync = false;
}

//Cooldowns
chairPartCD -= chairPartCD > 0;

//Walk sound
soundWalk();