{
    "id": "4698213c-a7f1-4bc4-b49d-4db42d357a5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oActorParent",
    "eventList": [
        {
            "id": "062802e5-235c-4f99-9f37-9eddd951bdb0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4698213c-a7f1-4bc4-b49d-4db42d357a5f"
        },
        {
            "id": "99c0f743-2c30-4849-806e-450898cf8373",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4698213c-a7f1-4bc4-b49d-4db42d357a5f"
        },
        {
            "id": "42ee364a-6934-46b8-91bc-525689602a33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4698213c-a7f1-4bc4-b49d-4db42d357a5f"
        },
        {
            "id": "f38ddd1f-e6e0-43a5-98ff-823ff5b29951",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "4698213c-a7f1-4bc4-b49d-4db42d357a5f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "21133b5e-cc44-4641-93a2-2c41b6488b40",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}