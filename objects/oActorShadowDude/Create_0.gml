/// @description 
event_inherited();

//Props
moveSpeedOrg = 0.6;
moveSpeed = moveSpeedOrg;
image_speed = 0.5;

//Sprites
sprite[st.idle] = sShadowDude_Idle;
sprite[st.move] = sShadowDude_Move;
sprite[st.pickMarzia] = sShadowDude_MarziaBegin;
sprite[st.walkMarzia] = sShadowDude_MarziaWalk;

defaultMask = sprite[st.idle];
mask_index = defaultMask;