{
    "id": "5330e420-4d5f-4196-81d7-8b4509ac5a0d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oActorShadowDude",
    "eventList": [
        {
            "id": "c9d60893-4cc7-43c5-904d-0fbe60cde034",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5330e420-4d5f-4196-81d7-8b4509ac5a0d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4698213c-a7f1-4bc4-b49d-4db42d357a5f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a513a291-b89b-4e14-8e02-b7392cf157ce",
    "visible": true
}