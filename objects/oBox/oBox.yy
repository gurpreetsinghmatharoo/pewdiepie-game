{
    "id": "a7e73be2-902d-4cdd-ba28-17380aa4c1cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBox",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "94e1431c-16ce-4e98-9fe5-9a284f64a9fd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c5d6abf1-ffc9-4516-b088-cc41b863c103",
    "visible": true
}