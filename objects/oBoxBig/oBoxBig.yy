{
    "id": "30cb9598-f504-41cd-ada7-e514908d1f83",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBoxBig",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "94e1431c-16ce-4e98-9fe5-9a284f64a9fd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "124e4812-e4d2-4d8e-a2af-14e5163f9390",
    "visible": true
}