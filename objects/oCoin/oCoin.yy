{
    "id": "aff2672c-1670-46dc-85c3-c1e60653875f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCoin",
    "eventList": [
        {
            "id": "b3e2c3df-d6bd-4f8f-a230-f33be8f2ff5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aff2672c-1670-46dc-85c3-c1e60653875f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "77ccf39d-3123-4d1c-95f6-d8c95eccde37",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3ffc403-89b7-47eb-9ad2-4d5fbb93cbcf",
    "visible": true
}