/// @description 
//Props
maxHsp = 4;
rotate = true;
rotateSpeed = 4;

scaleXRaw = 1;
scaleYRaw = 1;
scaleSpeed = 0.1;

//Vars
hsp = 0;
vsp = 0;

createdInCol = false;

scaleX = 1;
scaleY = 1;

rotation = 0;

collected = false;

timer = -1;