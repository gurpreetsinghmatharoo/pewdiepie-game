/// @description 
//Gravity
//if (grav){
//	vsp += grav;
//}

//Created in collision
if (createdInCol && !collision(0, 0)){
	createdInCol = false;
}

//Rotation
if (rotate) rotation += rotateSpeed;

image_xscale = dsin(rotation);

//Move on timer
if (timer>=0){
	//Reduce movement to 0
	hsp = lerp(hsp, 0, 0.1);

	//Gravity
	if (vsp < 10) vsp += 0.5;

	//Collisions
	playerCollisions();

	//Apply
	x += hsp;
	y += vsp;
}

//Timer
timer -= timer>0;

if (timer==0){
	instance_destroy();
}

//Timer alpha
if (timer >= 0 && timer < room_speed){
	image_alpha = (timer mod 8)/4;
}

//Collected
if (collected){
	//Scale
	image_xscale += 0.075;
	image_yscale = image_xscale;
	
	//Alpha
	image_alpha -= 0.075;
	
	//Destroy
	if (image_alpha < 0) instance_destroy();
}

//Scaling
scaleX = lerp(scaleX, scaleXRaw, scaleSpeed);
scaleY = lerp(scaleY, scaleYRaw, scaleSpeed);