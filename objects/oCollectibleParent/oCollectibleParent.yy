{
    "id": "77ccf39d-3123-4d1c-95f6-d8c95eccde37",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCollectibleParent",
    "eventList": [
        {
            "id": "cec1df2f-2a36-4524-9026-b465c243010e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "77ccf39d-3123-4d1c-95f6-d8c95eccde37"
        },
        {
            "id": "2f3a0eaf-9824-4764-bca3-8aeb66b50cae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "77ccf39d-3123-4d1c-95f6-d8c95eccde37"
        },
        {
            "id": "989d9ae5-1891-4730-8633-7694cde074c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "77ccf39d-3123-4d1c-95f6-d8c95eccde37"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}