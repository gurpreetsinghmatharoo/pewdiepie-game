/// @description 
draw_set_font(ftCtx);

if (stepRan){
	//Vars
	var w = width*ratio;
	var h = height*ratio;
	var lH = listH*ratio;
	
	//Background
	draw_9slice(x+1, y+1, w, h, sContextMenu, 0, 1, ratio, 0, 0.5);
	draw_9slice(x, y, w, h, sContextMenu, 0, -1, ratio, 0);

	//Draw list
	for(var i=0; i<array_length_1d(options); i++){
		var xx = x;
		var yy = y+(listH*ratio)*i;
	
		draw_set_alpha((mouseOpt==i && mouseIn)/2 + 0.5);
		draw_set_valign(fa_middle);
		
		//Draw name based on type
		if (type==ctxT.root)
		draw_text_transformed(xx + 2, yy + lH/2, global.ctxName[i], ratio, ratio, 0);
		else if (type==ctxT.objs)
		draw_text_transformed(xx + 2, yy + lH/2, object_get_name(i), ratio, ratio, 0);
		else if (type==ctxT.rooms)
		draw_text_transformed(xx + 2, yy + lH/2, room_get_name(i), ratio, ratio, 0);
		
		draw_set_valign(fa_top);
		draw_set_alpha(1);
		
		//More menu arrow
		if (type==ctxT.root){
			if (in(options[i], ctx.addInst, ctx.changeRoom)){
				draw_sprite_ext(sMoreArrow, 0, x+w, yy, ratio, ratio, 0, -1, 1);
			}
		}
	}
}

draw_set_font(global.defaultFont);