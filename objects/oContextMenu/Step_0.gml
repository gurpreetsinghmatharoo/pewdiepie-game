/// @description 
stepRan = true;

//Vars
var w = width*ratio;
var h = height*ratio;
var lH = listH*ratio;

//Mouse pos
mouseX = mouse_x;
mouseY = mouse_y;
var mouseRelY = mouseY - y;

mouseOpt = mouseRelY div (listH*ratio);
mouseClick = mouse_check_button_pressed(mb_left);
mouseIn = point_in_rectangle(mouse_x, mouse_y, x, y, x+w, y+h);

//Click
if (mouseClick && mouseOpt >= 0 && mouseIn){
	switch(type){
		//Root options
		case ctxT.root:
			switch(mouseOpt){
				case ctx.addInst:
					//Create array with object IDs
					var arr = [];
					var i = 0;
					
					while(object_exists(i)){
						var arrSize = array_length_1d(arr);
						arr[arrSize] = i;
						i++;
					}
					
					//Create menu
					child = ctxCreate(x + width*ratio, y + lH*mouseOpt, ctxT.objs, arr);
					child.parent = id;
				break;
			
				case ctx.removeInst:
					var inst = instance_position(x, y, all);
				
					if (instance_exists(inst)){
						instance_destroy(inst);
					}
					
					ctxDestroyBegin();
				break;
				
				case ctx.changeRoom:
					//Create array with room IDs
					var arr = [];
					var i = 0;
					
					while(room_exists(i)){
						var arrSize = array_length_1d(arr);
						arr[arrSize] = i;
						i++;
					}
					
					//Create menu
					child = ctxCreate(x + width*ratio, y + lH*mouseOpt, ctxT.rooms, arr);
					child.parent = id;
				break;
				
				case ctx.ctsEnd:
					//End cutscene
					cutsceneEndForce();
					
					ctxDestroyBegin();
				break;
				
				case ctx.stopCam:
					global.ctsCamX = camera_get_view_x(view_camera);
					global.ctsCamY = camera_get_view_y(view_camera);
					
					ctxDestroyBegin();
				break;
				
				case ctx.close:
					ctxDestroyBegin();
				break;
			}
		break;
		
		//Objs
		case ctxT.objs:
			//Spawn object instance
			instance_create_layer(parent.x, parent.y, "Instances_0", mouseOpt);
			
			//Destroy
			ctxDestroy();
		break;
		
		//Room
		case ctxT.rooms:
			//End cutscene
			if (global.ctsPos >= 0){
				cutsceneEndForce();
			}
		
			//Change room
			room_goto(mouseOpt);
			
			//Destroy
			ctxDestroy();
		break;
	}
}