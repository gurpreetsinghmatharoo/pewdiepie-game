{
    "id": "00d8c4c6-b44d-405c-9c9b-96d29638a256",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oContextMenu",
    "eventList": [
        {
            "id": "8eeb11b4-1ed2-4a2a-8f12-9248379632df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "00d8c4c6-b44d-405c-9c9b-96d29638a256"
        },
        {
            "id": "3e94d119-e04d-4992-b69e-dd9be49e71f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "00d8c4c6-b44d-405c-9c9b-96d29638a256"
        },
        {
            "id": "216cb665-a28d-4742-91a7-0952bd23b86b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "00d8c4c6-b44d-405c-9c9b-96d29638a256"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}