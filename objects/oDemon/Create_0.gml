/// @description 
//Parent
event_inherited();

//State
state = st.idle;

//Sprites
sprite[st.idle] = sDemon_Idle;
sprite[st.move] = sDemon_Move;
sprite[st.dead] = sDemon_Idle;

mask_index = sprite[st.idle];

//Props
moveSpeed = 1;
jumpSpeed = 6;
gravSpeed = 1;
gravInterval = 2;

//Vars
hp = 2;
attack = 10;
coinLoss = 5;