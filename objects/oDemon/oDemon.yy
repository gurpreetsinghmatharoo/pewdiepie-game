{
    "id": "9de3b4f5-75c1-4905-a172-88c911b8208a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDemon",
    "eventList": [
        {
            "id": "27175fc1-4222-4ac9-b53e-776e46c7cabe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9de3b4f5-75c1-4905-a172-88c911b8208a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fa474298-8866-45e3-a93d-ba8a50eb81dc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "92c4c860-632e-4d45-ae1d-8e826bdbd6ad",
    "visible": true
}