/// @description 
//Out of bounds
var col = collision(0, 0);
if (x<-64 || x>room_width+64 || y<-64 || y>room_height+64 || col){
	instance_destroy(id, col);
}

//Movement
hsp = lengthdir_x(spd, dir);
vsp = lengthdir_y(spd, dir);

//Move
x += hsp;
y += vsp;