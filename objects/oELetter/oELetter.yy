{
    "id": "0cc6ee4e-2abc-4713-93fa-4aa94ceb7f23",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oELetter",
    "eventList": [
        {
            "id": "a0071c3f-902a-4794-8603-b6863f2afb21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0cc6ee4e-2abc-4713-93fa-4aa94ceb7f23"
        },
        {
            "id": "0f06cf0f-1726-44f7-8337-8a2697289c0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0cc6ee4e-2abc-4713-93fa-4aa94ceb7f23"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b0ef090d-9934-42ba-8ab4-fd3028117e93",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6a2e3165-2ae7-4611-8745-5896a2dee010",
    "visible": true
}