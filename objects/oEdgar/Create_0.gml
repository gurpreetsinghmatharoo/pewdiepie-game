/// @description 
event_inherited();

//Cutscenes
//actor = oActorEdgar;

//States & Stats
state = st.idle;
hp = 100;
following = oPlayer;

//Sprites
edgarSpriteInit();

//Props
moveSpeed = 2;
jumpSpeed = 6;
gravSpeed = 1;
gravInterval = 2;

lerpSpeed = 0.2;

distToPlayer = 16;
jumpDistToPlayer = 16;
fallDistToPlayer = 16;

keyRelX = 0;
keyRelY = bbox_top-y;