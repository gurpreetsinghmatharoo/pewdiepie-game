/// @description 
//Vars
hsp = 0;
vsp = 0;
flap = 0; //Just for the collisions also used by the flying enemies

moveDir = choose(1, -1);

gravCooldown = 0;

grounded = false;
groundedPrev = false;

canFly = false;

createdInCol = false;

//Boost
boostSpeed = 8;

boostX = 0;
boostY = 0;

//Scaling
scaleX = 1;
scaleY = 1;
scaleXRaw = 1;
scaleYRaw = 1;
scaleSpeed = 0.1;

//White
whiteAlpha = 0;