/// @description 
playerDraw();

//White
if (whiteAlpha>0){
	shader_set(shWhite);
	
	draw_sprite_ext(sprite_index, image_index, x, y,
		scaleX*image_xscale, scaleY, image_angle, image_blend, whiteAlpha);
		
	shader_reset();
}

//draw_text(x, y, id);