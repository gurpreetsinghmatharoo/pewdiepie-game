/// @description 
//Vars
grounded = collision(0, 1);

//Created in collision
if (createdInCol && !collision(0, 0)){
	createdInCol = false;
}

//States
switch(state){
	case st.idle: case st.move: case st.jump:
		//Movement
		hsp = moveDir * moveSpeed;
		hsp += boostX;
	
		//Gravity
		playerGravity();

		//Jump
		//if (global.jump){// && grounded){
		//    vsp = -jumpSpeed;
			
		//	//Scale
		//	scale_set(1, 1.2, 30);
		//}
		
		//Hit ground scale
		if (grounded && !groundedPrev){
			scale_set(1.4, 0.6, 5);
		}

		//Collisions
		enemyCollisions();

		//Apply
		x += hsp;
		y += vsp;
		
		//State
		//if (!grounded) state = st.jump;
		if (hsp!=0) state = st.move;
		else state = st.idle;
		
		//Scale
		image_xscale = state==st.move ? sign(hsp) : image_xscale;
		
		//Hurt
		enemyHurtCode();
	break;
	
	case st.dead:
		scaleXRaw = 0;
		if (scaleX < 0.05){
			instance_destroy();
		}
	break;
}

//Sprite
sprite_index = sprite[state];

//Scaling
scaleX = lerp(scaleX, scaleXRaw, scaleSpeed);
scaleY = lerp(scaleY, scaleYRaw, scaleSpeed);

//Boost
boostX = lerp(boostX, 0, 0.1);

//White
whiteAlpha -= (whiteAlpha>0)/10;

//Dead
if (hp<=0 && state!=st.dead){
	state_set(st.dead);
	
	//Particles
	poof_particles_spread(sDemonPoof, 5, 45);
			
	//Sound
	audio_play_sound(sndDie, 1, 0);
}