{
    "id": "fa474298-8866-45e3-a93d-ba8a50eb81dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemies",
    "eventList": [
        {
            "id": "934a05aa-0b46-41a7-bddd-fb8815f6c886",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fa474298-8866-45e3-a93d-ba8a50eb81dc"
        },
        {
            "id": "e699f20c-7c59-44c2-bfe4-9ac2138dcaa5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "fa474298-8866-45e3-a93d-ba8a50eb81dc"
        },
        {
            "id": "cf12ffb5-a75d-47ad-9e18-b1f45f1d3e90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "fa474298-8866-45e3-a93d-ba8a50eb81dc"
        },
        {
            "id": "f74f3757-f46b-464c-859c-2a5ed0d91bae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fa474298-8866-45e3-a93d-ba8a50eb81dc"
        },
        {
            "id": "c282b4e3-7284-4c4e-b06d-bec023eb76be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fa474298-8866-45e3-a93d-ba8a50eb81dc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}