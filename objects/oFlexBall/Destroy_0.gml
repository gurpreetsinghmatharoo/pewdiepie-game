/// @description 
//Add particle
ds_list_add(oFlexGrid.list, [x, y, irandom(sprite_get_number(sFlexPart)-1), 0.1]);

//Set tile
var xInCell = x mod PL_SIZE;
var yInCell = y mod PL_SIZE;
var detectionRadius = PL_SIZE*0.75;

var dist = point_distance(PL_SIZE/2, PL_SIZE/2, xInCell, yInCell);

if (dist < detectionRadius){
	var td = tilemap_get_at_pixel(global.Tiles_Lines, x, y);
	if (td<=0) tilemap_set_at_pixel(global.Tiles_Lines, 8, x, y);
}

//Check
if (instance_exists(oPowerSource)) {
	with (oPowerSource) CD = 0;
}