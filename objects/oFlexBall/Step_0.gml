/// @description 
//Out of bounds
var col = collision(0, 0) || collision_circle(x, y, radius, oCollision, 0, 1);
if (x<-64 || x>room_width+64 || y<-64 || y>room_height+64 || col){
	instance_destroy(id, col);
}

//Move
x += hsp;
y += vsp;

//Angle
image_angle = point_direction(0, 0, hsp, vsp);