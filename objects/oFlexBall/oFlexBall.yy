{
    "id": "4c2634f8-02ea-4dd4-8b07-fef485edee34",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFlexBall",
    "eventList": [
        {
            "id": "9ccd8a1f-6ad6-48f1-b272-862220ed8d43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4c2634f8-02ea-4dd4-8b07-fef485edee34"
        },
        {
            "id": "dc8e5467-0488-48fb-86ed-4aedb30b9ede",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4c2634f8-02ea-4dd4-8b07-fef485edee34"
        },
        {
            "id": "5463b0bb-b081-4a6e-9c70-0f7c2ee38f50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4c2634f8-02ea-4dd4-8b07-fef485edee34"
        },
        {
            "id": "7e1426ea-1625-4af7-b236-59a4cc8ec409",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "4c2634f8-02ea-4dd4-8b07-fef485edee34"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "421fde70-58d5-41a0-aa88-770f5634460e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "270b16ed-2b56-45c5-8c4f-88ffece8a999",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "$FF2300D3",
            "varName": "color[0]",
            "varType": 7
        },
        {
            "id": "591f3dfb-d10c-4c63-a25a-968032b37c81",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "$FFCE5323",
            "varName": "color[1]",
            "varType": 7
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}