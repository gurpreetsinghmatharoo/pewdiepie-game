/// @description Save surface to buffer
if (surface_exists(surf)){
	buffer_get_surface(buff, surf, 0, 0, 0);
}