/// @description 
//Destroy data structures
ds_list_destroy(list);
ds_grid_destroy(grid);

//Surfaces
if (surface_exists(outputSurf)){
	surface_free(outputSurf);
}

if (surface_exists(surf)){
	surface_free(surf);
}

//Buffer
if (buffer_exists(buff)){
	buffer_delete(buff);
}