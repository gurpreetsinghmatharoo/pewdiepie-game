/// @description 
//Particle list
list = ds_list_create();

enum ePr{
	x,
	y,
	index,
	scale
}

//Surface
surf = -1;
buff = -1;

//tileSurf = -1;
outputSurf = -1;

//Flex grid
grid = ds_grid_create(room_width div PL_SIZE, room_height div PL_SIZE);
isEmpty = true;