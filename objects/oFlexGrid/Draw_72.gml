/// @description 
if (isEmpty) exit;

//Surface
if (!surface_exists(surf)){
	surf = surface_create(room_width, room_height);
	
	//Create buffer
	if (!buffer_exists(buff)){
		buff = buffer_create(room_width*room_height*4, buffer_fixed, 1);
		buffer_get_surface(buff, surf, 0, 0, 0);
		
		//Set alarm for regular saving
		alarm[0] = room_speed;
	}
	else{
		buffer_set_surface(buff, surf, 0, 0, 0);
	}
}

//Other surfaces
//if (!surface_exists(tileSurf)){
//	tileSurf = surface_create(CAM.WIDTH, CAM.HEIGHT);
//}
if (!surface_exists(outputSurf)){
	outputSurf = surface_create(CAM.WIDTH*CAM.SCALE, CAM.HEIGHT*CAM.SCALE);
}

//Draw particles
surface_set_target(surf);

for(var i=0; i<ds_list_size(list); i++){
	var arr = list[| i];
	
	draw_sprite_ext(sFlexPart,
					arr[ePr.index],
					arr[ePr.x], arr[ePr.y],
					arr[ePr.scale], arr[ePr.scale],
					0, -1, 1);
}

surface_reset_target();