/// @description 
//List particles
for(var i=0; i<ds_list_size(list); i++){
	if (isEmpty) isEmpty = false;
	
	//Array
	var arr = list[| i];
	
	//Scale it up
	arr[@ ePr.scale] = lerp(arr[ePr.scale], 2, 0.1);
	
	//Delete
	if (arr[ePr.scale] >= 1.98){
		ds_list_delete(list, i);
		i--;
	}
}