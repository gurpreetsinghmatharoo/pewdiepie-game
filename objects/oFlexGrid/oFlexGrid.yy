{
    "id": "f90e5fb9-dd78-4f28-af09-6e4af7d94ddc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFlexGrid",
    "eventList": [
        {
            "id": "268eb69f-1104-42e1-a890-3ce87fcff5bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f90e5fb9-dd78-4f28-af09-6e4af7d94ddc"
        },
        {
            "id": "1955d46e-d5f6-410a-a5a1-bf02b7719d98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 8,
            "m_owner": "f90e5fb9-dd78-4f28-af09-6e4af7d94ddc"
        },
        {
            "id": "89a77e30-2f85-4361-b4d9-4f9a5d95c232",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f90e5fb9-dd78-4f28-af09-6e4af7d94ddc"
        },
        {
            "id": "e90dc7f5-b9a9-48d0-822e-0254db4bcf66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f90e5fb9-dd78-4f28-af09-6e4af7d94ddc"
        },
        {
            "id": "28f10c8e-e0d6-42ca-82df-f1cb990e635a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f90e5fb9-dd78-4f28-af09-6e4af7d94ddc"
        },
        {
            "id": "35fa7161-c986-4c9b-95a1-163cce47a748",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "f90e5fb9-dd78-4f28-af09-6e4af7d94ddc"
        },
        {
            "id": "0d7c205c-d85d-4aaa-831a-3fd8e1ff5769",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f90e5fb9-dd78-4f28-af09-6e4af7d94ddc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}