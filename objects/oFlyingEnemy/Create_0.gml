/// @description 
event_inherited();

//State
state = st.idle;

//Sprites
sprite[st.idle] = sE_Idle;
sprite[st.flap] = sE_Flap;
sprite[st.dead] = sE_Idle;

mask_index = sprite[st.idle];

//Props
flySpeed = 1.5;
flapSpeed = 3; flapDecay = 0.05;
gravSpeed = 1;
gravInterval = 2;

//Vars
hp = 1;
attack = 10;
coinLoss = 5;
flapCD = 0;

canFly = true;

//Movement
spd = 0;
dir = 0;
flap = 0;
targetDir = random(360);

rangeOuter = 200;
rangeInner = 24;
turn = 0.1;

//Shooting
shootCD = 0;
shootDelay = 60;
shootChance = 10;
shootError = 20;