/// @description 
//Vars
grounded = collision(0, 1);
var plrExist = instance_exists(oPlayer);
var originDist = point_distance(x, y, xstart, ystart);
var plrInRange = false;

//Movement direction
var _dist = plrExist ? distance_to_object(oPlayer) : 10000;
var _sight = plrExist ? line_of_sight(x, y, oPlayer.x, oPlayer.y) : false;

if (_dist < rangeOuter && _dist > rangeInner && _sight){
	//Turn
	var _dir = point_direction(x, y, oPlayer.x, oPlayer.y);
	dir += angle_difference(_dir, dir) * turn;
	
	spd = flySpeed;
	
	plrInRange = true;
}
else{
	//Go back inside if out of origin range
	var outOfOrigin = originDist > originRange;
	if (outOfOrigin){
		targetDir = point_direction(x, y, xstart, ystart);
	}
	//Set random target direction based on chance
	else if (irandom(20)==0){
		targetDir = random(360);
	}
	
	//Turn
	dir += angle_difference(targetDir, dir) * turn;
	
	//Always move when out of origin range
	if (outOfOrigin){
		spd = flySpeed;
	}
	//Inside origin range
	else{
		//Stop at random
		if (abs(spd) && irandom(30)==0){
			spd = 0;
		}
	
		//Start at random
		if (spd==0 && irandom(10)==0){
			spd = flySpeed;
		}
	}
}

//States
switch(state){
	case st.idle: case st.flap:
		enemyFly();

		//Flap
		var closeToGround = !line_of_sight(x, y, x, y+32, 8);
		if ((irandom(4)==0 && state==st.idle && !flapCD) || (closeToGround && state==st.idle)){
			flapCD = 20;
			state_set(st.flap);
		}

		//Collisions
		enemyCollisions();

		//Apply
		x += hsp;
		y += vsp + flap;
		
		//Scale
		image_xscale = hsp!=0 ? sign(hsp) : image_xscale;
		
		//Hurt
		enemyHurtCode();
		
		//Flap speed
		if (state==st.flap && floor(image_index==3)){
			flap = -flapSpeed;
		}
		
		//Back to idle
		if (state==st.flap && animation_end()){
			state_set(st.idle);
		}
		
		//Shoot
		if (plrInRange && !shootCD && irandom(shootChance)==0){
			var _dir = point_direction(x, y, oPlayer.x, oPlayer.y) + irandom_range(-shootError, shootError);
			
			var inst = instance_create_layer(x, y, "Instances_0", oELetter);
			inst.spd = inst.moveSpeed;
			inst.dir = _dir;
			
			//Scale
			scaleX = 2;
			scaleY = 2;
			scaleXRaw = 1;
			scaleYRaw = 1;
			
			//CD
			shootCD = shootDelay;
		}
	break;
	
	case st.dead:
		scaleXRaw = 0;
		if (scaleX < 0.05){
			instance_destroy();
		}
	break;
}

//Sprite
sprite_index = sprite[state];

//Scaling
scaleX = lerp(scaleX, scaleXRaw, scaleSpeed);
scaleY = lerp(scaleY, scaleYRaw, scaleSpeed);

//Boost
boostX = lerp(boostX, 0, 0.1);
boostY = lerp(boostY, 0, 0.1);

//White
whiteAlpha -= (whiteAlpha>0)/10;

//Flap Speed
flap -= (abs(flap)>0)*flapDecay*sign(flap);

//Flap Countdown
flapCD -= (flapCD>0) * (state==st.idle);

//Shoot Countdown
shootCD -= shootCD>0;

//Dead
if (hp<=0 && state!=st.dead){
	state_set(st.dead);
	
	//Particles
	poof_particles_spread(sDemonPoof, 5, 45);
}