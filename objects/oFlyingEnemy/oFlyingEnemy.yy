{
    "id": "4b81fdd1-10d9-4d30-b218-40f319caa371",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFlyingEnemy",
    "eventList": [
        {
            "id": "69868f96-803f-418d-bed4-a1248397d20f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4b81fdd1-10d9-4d30-b218-40f319caa371"
        },
        {
            "id": "bd60f0d2-943a-4e25-8bef-9b2fbcaeea66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4b81fdd1-10d9-4d30-b218-40f319caa371"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fa474298-8866-45e3-a93d-ba8a50eb81dc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "648dd4c1-d10c-4135-a898-36e5152fd920",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "256",
            "varName": "originRange",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "835badd2-3333-4e06-8450-7f567317a0e5",
    "visible": true
}