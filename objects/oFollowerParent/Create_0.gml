/// @description 
//Vars
//Scaling
scaleX = 1;
scaleY = 1;
scaleXRaw = 1;
scaleYRaw = 1;
scaleSpeed = 0.1;

//Vars
hsp = 0;
vsp = 0;

boostX = 0;
boostY = 0;

grounded = false;
groundedPrev = false;

gravCooldown = 0;

pickupInst = noone;

createdInCol = false;

//Distanced from player
maxPlayerDist = 64;
maxPlayerTime = room_speed*2;
curPlayerTime = 0; //Time the follower has stayed away from the player