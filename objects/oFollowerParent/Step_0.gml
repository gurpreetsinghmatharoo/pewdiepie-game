/// @description 
grounded = collision(0, 1);

//States
switch(state){
	case st.idle: case st.move:
		var moveCond = (distance_to_object(following)>distToPlayer) || following.object_index!=oPlayer;
		var hor = sign(following.x - x) * moveCond;

		hsp = lerp(hsp, hor * moveSpeed, lerpSpeed);
		
		//var jump = abs(hor) && collision(hsp*8, 0) && collision(0, 1);
		var jump = abs(hor) && collision(0, 1) && following.bbox_bottom < bbox_top-jumpDistToPlayer;

		playerGravity();
		
		//Go through one way tiles
		var onewayBelow = tilemap_collision(global.Tiles_Oneway, x, bbox_bottom+1);
		var floorBelow = collision(0, 1, false);
		y += gravSpeed * (oPlayer.bbox_top > bbox_bottom+fallDistToPlayer) * bool(onewayBelow) * !(floorBelow);

		//Jump
		if (jump){
		    vsp = -jumpSpeed;
			
			//Scale
			scale_set(0.8, 1.4, 15);
		}
		
		//Hit ground scale
		if (grounded && !groundedPrev){
			scale_set(1.4, 0.6, 5);
		}

		playerCollisions();

		//Apply
		x += hsp;
		y += vsp;
		
		//State
		if (abs(hsp)) state = st.move;
		else state = st.idle;
		
		//Follow key
		var nearKey = instance_nearest(x, y, oKeyParent);
		var nearGate = instance_nearest(x, y, oGateParent);
		if (nearKey && pickupInst==noone && nearKey.state==keySt.idle){
			var xDist = abs(nearKey.x - x);
			var yDist = abs(nearKey.y - y);
			
			if (xDist < 64 && yDist <= jumpDistToPlayer){
				following = nearKey;
			}
			else{
				following = oPlayer;
			}
		}
		//Follow gate
		else if (nearGate && pickupInst && nearGate.state==doorSt.locked
			&& (nearGate==pickupInst.doorID || nearGate.object_index==pickupInst.doorID)){
			var xDist = abs(nearGate.x - x);
			var yDist = abs(nearGate.bbox_bottom - y);
			
			if (xDist < 128 && yDist <= jumpDistToPlayer){
				following = nearGate;
			}
			else{
				following = oPlayer;
			}
		}
		//Follow player
		else{
			following = oPlayer;
		}
		
		//Touch key
		if (place_meeting(x, y, nearKey) && pickupInst==noone 
		&& nearKey.state==keySt.idle && abs(nearKey.x-x)<4){
			state_set(st.pickup);
			pickupInst = nearKey;
			pickupInst.state = keySt.picked;
			pickupInst.stickTo = id;
		}
		
		//Distanced from player
		var playerDist = distance_to_object(oPlayer);
		
		if (playerDist > maxPlayerDist){
			//Add to time
			curPlayerTime++;
			
			//Reached limit
			if (curPlayerTime > maxPlayerTime && oPlayer.grounded){
				//Teleport position
				var xx = oPlayer.x;
				var yy = oPlayer.y - 8;
				
				//Find new place
				var xx1 = xx;
				var xx2 = xx;
				
				while (collision(xx - x, yy - y)){
					//To right
					xx1++;
					
					if (!collision(xx1 - x, yy - y)){
						xx = xx1;
						break;
					}
					
					//To left
					xx2--;
					
					if (!collision(xx2 - x, yy - y)){
						xx = xx2;
						break;
					}
				}
				
				//Teleport to player
				x = xx;
				y = yy;
				
				//Particle
				poof_particles_spread(sMagicPart, 10, 20);
				
				//Reset timer
				curPlayerTime = 0;
			}
		}
	break;
	
	//Pickup
	case st.pickup:
		//Key distance from point
		//var keyDist = point_distance(x+keyRelX, y+keyRelY, pickupInst.x, pickupInst.bbox_bottom);
	
		//End
		if (animation_end()){
			state_set(st.idle);
		}
	break;
}

//Sprite
sprite_index = sprite[state];
image_xscale = state==st.move ? sign(hsp) : image_xscale;

//Scaling
scaleX = lerp(scaleX, scaleXRaw, scaleSpeed);
scaleY = lerp(scaleY, scaleYRaw, scaleSpeed);

//Check pickupInst
if (!instance_exists(pickupInst)){
	pickupInst = noone;
}

//Collectibles
getCollectibles();