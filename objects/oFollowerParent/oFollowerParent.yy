{
    "id": "a4c575eb-0de1-4ad3-959e-5ffad73fa355",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFollowerParent",
    "eventList": [
        {
            "id": "8bd99341-6dbc-4b2b-9ccc-20e42e7fdaa7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a4c575eb-0de1-4ad3-959e-5ffad73fa355"
        },
        {
            "id": "d1fc57bd-17f6-4c1e-afc8-76febf385698",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "a4c575eb-0de1-4ad3-959e-5ffad73fa355"
        },
        {
            "id": "5abd378e-ee49-47d2-9974-0e0a603fb679",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a4c575eb-0de1-4ad3-959e-5ffad73fa355"
        },
        {
            "id": "b93cea46-e29e-4e6d-83fd-57eb25e8196e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a4c575eb-0de1-4ad3-959e-5ffad73fa355"
        },
        {
            "id": "00c22068-79c2-4976-9ef1-b3d21c285901",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a4c575eb-0de1-4ad3-959e-5ffad73fa355"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "21133b5e-cc44-4641-93a2-2c41b6488b40",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}