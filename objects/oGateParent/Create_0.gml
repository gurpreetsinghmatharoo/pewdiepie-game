/// @description 
state = doorSt.locked;

//Vars
unlockTime = 0;
unlockAlphaRaw = 0;
unlockAlpha = 0;

org_angle = image_angle;

//Sprite info
var sprW = sprite_width;
var sprH = sprite_get_height(sprite_index);
var cellH = sprH/3;

//New sprite's surface (Frame 0)
var surf = surface_create(sprite_width, sprite_height);

surface_set_target(surf);

//Top part
draw_sprite_part(sprite_index, 0, 0, 0, sprW, cellH, 0, 0);

//Middle part
for(var i=cellH; i<sprite_height; i+=cellH){
	draw_sprite_part(sprite_index, 0, 0, cellH, sprW, cellH, 0, i);
}

//Bottom part
draw_sprite_part(sprite_index, 0, 0, sprH-cellH, sprW, cellH, 0, sprite_height-cellH);

surface_reset_target();

//New sprite
var sprite = sprite_create_from_surface(surf, 0, 0, sprite_width, sprite_height, 0, 0, sprite_width/2, 0);
image_index = sprite_index;

//New sprite's surface (Frame 1)
surface_set_target(surf);
draw_clear_alpha(0, 0);

//Top part
draw_sprite_part(sprite_index, 1, 0, 0, sprW, cellH, 0, 0);

//Middle part
for(var i=cellH; i<sprite_height; i+=cellH){
	draw_sprite_part(sprite_index, 1, 0, cellH, sprW, cellH, 0, i);
}

//Bottom part
draw_sprite_part(sprite_index, 1, 0, sprH-cellH, sprW, cellH, 0, sprite_height-cellH);

surface_reset_target();

//Add frame
sprite_add_from_surface(sprite, surf, 0, 0, sprite_width, sprite_height, 0, 0);

//Apply sprite
sprite_index = sprite;

//Remove surface
surface_free(surf);

//Reset scale
image_yscale = 1;