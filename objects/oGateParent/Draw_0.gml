/// @description 
draw_sprite_ext(sprite_index, 0, x, y, 1, 1, image_angle, -1, 1);
draw_sprite_ext(sprite_index, 1, x, y, 1, 1, image_angle, -1, unlockAlpha);

//Debug
if (global.debug >= 3){
	//Mask
	draw_rectangle_color(bbox_left, bbox_top, bbox_right, bbox_bottom, 1, 2, 1, 2, 1);
	
	//Position
	draw_circle_color(x, y, 4, c_blue, c_aqua, 0);
	
	//Center
	draw_circle_color(x, y+sprite_height/2, 4, c_black, c_gray, 0);
}