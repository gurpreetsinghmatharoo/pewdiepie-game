/// @description 
//States
switch(state){
	case doorSt.locked:
	
	break;
	
	case doorSt.unlocked:
		unlockTime++;
		
		//Open
		if (unlockTime>60){
			image_angle += sign(angle_difference(org_angle + unlockRotation, image_angle))*2;
			unlockAlphaRaw = 1;
			
			//Snap
			if (abs(image_angle - (org_angle + unlockRotation))==1){
				image_angle = org_angle + unlockRotation;
			}
		}
		//Blink
		else{
			unlockAlphaRaw = (unlockTime mod 20)/10;
		}
	break;
}

//Unlock alpha
unlockAlpha = lerp(unlockAlpha, unlockAlphaRaw, 0.3);