{
    "id": "dd046f0c-63c4-4461-843e-c46894593510",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGateParent",
    "eventList": [
        {
            "id": "79fef505-7ded-4ed2-b36b-c2d6cb4f1074",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd046f0c-63c4-4461-843e-c46894593510"
        },
        {
            "id": "9bbe3dad-ac5f-4f9d-b093-0a7dc3c40381",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dd046f0c-63c4-4461-843e-c46894593510"
        },
        {
            "id": "bcf50785-a51b-4280-8e57-798a81b007da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dd046f0c-63c4-4461-843e-c46894593510"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "eaebb985-ad2f-4530-a1fb-c1c3b11e2748",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "f3546706-1d60-421c-aec0-d1b5652f4f78",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": true,
            "rangeMax": 180,
            "rangeMin": -180,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "unlockRotation",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}