/// @description 
//Rotation
var sinF = sin(current_time/400);
angleAdd = sinF * rotMax;

//Collision
var playerCol = place_meeting(x, y, oPlayer);

//Scale if in contact
if (active && playerCol){
	image_xscale = lerp(image_xscale, 1.2, 0.2);
	textAlpha = lerp(textAlpha, 1, 0.2);
}
else{
	image_xscale = lerp(image_xscale, 1, 0.2);
	textAlpha = lerp(textAlpha, 0, 0.2);
}
image_yscale = image_xscale;

//Collect
if (active && playerCol && global.Ekey && itemType>=0 && stateNormal()){
	collected = true;
	//global.inv[global.equipped, item.type] = itemType;
	//global.inv[global.equipped, item.hp] = 1;
	global.itemSelected = itemType;
	oPlayer.state = st.itemselect;
}

//Collected
if (collected){
	image_alpha-=0.1;
	if (image_alpha<0){
		instance_destroy();
	}
}

//Sprite
sprite_index = global.items[itemType, pr.sprite];

//Active
if (active){
	image_blend = c_white;
}
else{
	image_blend = c_gray;
}

//Chair
if (room==rmMartiplier && itemType==type.chair){
	var prog = progressGet(room);
	
	if (prog >= 2){
		active = true;
	}
	else{
		active = false;
	}
}