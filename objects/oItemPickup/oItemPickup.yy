{
    "id": "0122cc2b-655d-4cce-a4ed-0d25fd77cf54",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oItemPickup",
    "eventList": [
        {
            "id": "8fc8a8e5-7c4e-4406-a4ce-a45b723151fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0122cc2b-655d-4cce-a4ed-0d25fd77cf54"
        },
        {
            "id": "0252c87a-3047-4f7b-88dd-a9b6a32771b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0122cc2b-655d-4cce-a4ed-0d25fd77cf54"
        },
        {
            "id": "3571310a-b612-408a-a2d7-614603483781",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0122cc2b-655d-4cce-a4ed-0d25fd77cf54"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "4e260f35-8fc0-4f47-88d5-3a7c7c40b21e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "-1",
            "varName": "itemType",
            "varType": 1
        },
        {
            "id": "62e0f6f4-beee-4619-bbee-a886e4475c9d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "active",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "2e674412-fa85-416f-adaf-6723541277f1",
    "visible": true
}