/// @description 
//States
switch(state){
	//Idle
	case keySt.idle:
		//Gravity
		playerGravity();
		playerCollisions();
		
		y += vsp;
	break;
	
	//Picked
	case keySt.picked:
		//Move
		if (instance_exists(stickTo)){
			if (stickTo.state==st.pickup && stickTo.image_index>3){
				x = lerp(x, stickTo.x + stickTo.keyRelX, 0.1);
				y = lerp(y, (stickTo.y + stickTo.keyRelY) - (bbox_bottom-y), 0.1);
			}
			else if (stickTo.state!=st.pickup){
				x = stickTo.x + stickTo.keyRelX;
				y = (stickTo.y + stickTo.keyRelY) - (bbox_bottom-y);
			}
		}
		
		//Unlocked
		var doorInst = collision_circle(x, y, 32, doorID, false, false);
		if (doorInst && doorInst.state==doorSt.locked){
			doorOpened = doorInst;
			doorInst.state = doorSt.unlocked;
			state_set(keySt.used);
		}
	break;
	
	//Used
	case keySt.used:
		image_xscale = lerp(image_xscale, 0, 0.1);
		image_yscale = lerp(image_yscale, 1.5, 0.1);
		
		x = lerp(x, doorOpened.x, 0.1);
		y = lerp(y, doorOpened.y + doorOpened.sprite_height/2, 0.1);
		
		if (image_xscale < 0.1) instance_destroy();
	break;
}