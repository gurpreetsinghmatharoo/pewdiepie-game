/// @desc Room Start

//Audio play
if (!audio_is_playing(sndMusic) && global.roomPlace[room]==rmType.exterior){
	audio_play_sound(sndMusic, 1, 1);
}

//Audio stop
if (audio_is_playing(sndMusic) && global.roomPlace[room]!=rmType.exterior){
	audio_stop_sound(sndMusic);
}

#region Main
//Create flex grid manager
instance_create_layer(0, 0, "Instances_1", oFlexGrid);

//Layer script
layer_script_begin("Tiles_Main", layerFlexPartBegin);
layer_script_end("Tiles_Main", layerFlexPartEnd);

//Context menu
ctxMenu = noone;

//Room variables
global.roomTime = 0;
global.restarting = false;

//Debug vars
lx1 = 0;
ly1 = 0;
lx2 = 0;
ly2 = 0;
lstate = 0;
#endregion

#region Cutscene System
//Vars
global.ctsCamX = undefined;
global.ctsCamX = undefined;
#endregion

#region GUI
//Font
global.defaultFont = ftMain;
draw_set_font(global.defaultFont);

//Healthbar
hpShow = 100;

hpWidth = sprite_get_width(sHealthbar);
hpHeight = sprite_get_height(sHealthbar);

//Black bars
blackBarHeight = CAM.HEIGHT / 6;
blackBarY = -1;
#endregion

#region Graphics

#region Camera
//Camera
view_enabled = true;
view_visible[0] = true;

var width = CAM.WIDTH, height = CAM.HEIGHT, scale = CAM.SCALE;

var cam = camera_create_view(0, 0, width, height, 0, -1, -1, -1, width/2, height/2);
view_set_camera(0, cam);

window_set_size(width*scale, height*scale);
surface_resize(application_surface, width*scale, height*scale);

//window_set_position(display_get_width()/2-(width*scale)/2, display_get_height()/2-(height*scale)/2);
display_set_gui_size(width, height);

//Shake
global.shakeX = 0;
global.shakeY = 0;
global.shakeFalloff = 0;
#endregion

///Backgrounds
//Room type
roomType = global.roomTypes[room];
roomPlace = global.roomPlace[room];
var baseBack = global.roomBack[roomType, 3];

//Constant info
backWidth = sprite_get_width(baseBack);
backHeight = sprite_get_height(baseBack);

//Clouds
windX = 1;

cloudBackX = 0;
cloudFrontX = 0;

cloudBackSpeed = 0.1;
cloudFrontSpeed = 0.2;

cloudBackHSP = 0;
cloudFrontHSP = 0;

#region Tiles & BK
//Tiles
global.Tiles_Main = layer_tilemap_get_id("Tiles_Main");
global.Tiles_Oneway = layer_tilemap_get_id("Tiles_Oneway");

global.Tiles_Main_CellSize = tilemap_get_tile_width(global.Tiles_Main);
global.Tiles_Oneway_CellSize = tilemap_get_tile_width(global.Tiles_Oneway);

global.Tiles_Lines = layer_tilemap_get_id("Tiles_Lines");

//Background
layer_set_visible("Color", false);

global.Back_3 = layer_background_create(layer_create(1030, "Back_3"), global.roomBack[roomType, 3]);
layer_background_htiled(global.Back_3, 1);
global.Back_2 = layer_background_create(layer_create(1020, "Back_2"), global.roomBack[roomType, 2]);
layer_background_htiled(global.Back_2, 1);
global.Back_1 = layer_background_create(layer_create(1010, "Back_1"), global.roomBack[roomType, 1]);
layer_background_htiled(global.Back_1, 1);
global.Back_0 = layer_background_create(layer_create(1000, "Back_0"), global.roomBack[roomType, 0]);
layer_background_htiled(global.Back_0, 1);
#endregion

//Fading
global.fadeState = fdSt.start;
global.fadeAlpha = 1;
global.fadeSurf = -1;
#endregion

#region Player
//Info
global.playerHP = 100;
#endregion

#region Inventory
//Inventory
global.maxInv = 2;
global.equipped = 0;

var i=0;
repeat(global.maxInv){
	//Inventory
	global.inv[i, item.type] = -1;
	global.inv[i, item.hp] = 1;
	global.inv[i, item.usecd] = 0;
	global.inv[i, item.refillcd] = 0;
	
	//GUI
	buttonScale[i] = 1;
	buttonHP[i] = 1;
	keyScale[i] = 1;
	
	//Loop
	i++;
}

//Add inventory to map
global.map[? "inv"] = global.inv;

//GUI
cellSize = 32;
global.stretch9 = true;

//GUI properties
global.keySelectScale = 2;
#endregion