/// @description 
//Collision test
if (global.colTest){
	//Offset
	var hor = keyboard_check(vk_pagedown) - keyboard_check(vk_delete);
	var ver = keyboard_check(vk_end) - keyboard_check(vk_home);
	var shift = keyboard_check(vk_shift);
	
	var offX = (hor*0.25)*(1+shift);
	var offY = (ver*0.25)*(1+shift);
	
	//Tiles
	var tileCol = tilemap_collision(global.Tiles_Main, mouse_x+offX, mouse_y+offY);
	
	//Objects
	var objCol = position_meeting(mouse_x+offX, mouse_y+offY, oCollision);
	
	//Draw
	//gpu_set_blendmode_ext(bm_inv_dest_color, bm_zero);
	draw_point(mouse_x+offX, mouse_y+offY);
	//gpu_set_blendmode(bm_normal);
	
	draw_values(mouse_x, mouse_y, tileCol, "Tile: ", objCol, "Object: ");
}