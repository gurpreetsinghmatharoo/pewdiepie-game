/// @description 
//Vars
var mouseX = device_mouse_x_to_gui(0);
var mouseY = device_mouse_y_to_gui(0);

#region Cutscenes
//Black bar height
if (global.ctsPos >= 0){
	//Black bar Y
	blackBarY = lerp(blackBarY, blackBarHeight, 0.1);
}
else{
	//Black bar Y
	blackBarY = lerp(blackBarY, -1, 0.1);
}
	
//Draw black bars
if (blackBarY > 0){
	//Color
	draw_set_color(c_black);
		
	//Top
	draw_rectangle(0, 0, CAM.WIDTH, blackBarY, 0);
		
	//Bottom
	draw_rectangle(0, CAM.HEIGHT-blackBarY, CAM.WIDTH, CAM.HEIGHT, 0);
		
	//Reset color
	draw_set_color(-1);
}
#endregion

//Check
if (global.GUIEnabled && instance_exists(oPlayer)){

#region Health
//Healthbar
var hpX = 4;
var hpY = 4;
hpShow = lerp(hpShow, global.playerHP, 0.2);

var hpWidthNew = (hpShow/100)*hpWidth;

draw_sprite(sHealthbar, 1, hpX, hpY);
draw_sprite_part(sHealthbar, 0, 0, 0, hpWidthNew, hpHeight, hpX, hpY);
#endregion

#region Collectibles
//Coins
var coinX = hpX;
var coinY = hpY*2 + hpHeight;

draw_sprite(sCoin, 0, coinX + 6, coinY + 6);
draw_text(coinX + 16, coinY, global.collectVar[coll.coin]);
#endregion

#region Inventory
//Inventory
for(var i=0; i<global.maxInv; i++){
	//Get item
	var item = global.inv[i, item.type];
	var hp = global.inv[i, item.hp];
	
	//Position
	var cX = invX + i*cellSize;
	var cY = invY;
	
	//Scale
	buttonScale[i] = lerp(buttonScale[i], 1, 0.1);
	
	draw_9slice(cX, cY, cellSize, cellSize, sItemButton, 0, -1, buttonScale[i]);
	
	if (item>=0){
		//Draw sprite
		var spr = global.items[item, pr.sprite];
		draw_sprite(spr, 0, cX + cellSize/2, cY + cellSize/2);
		
		//Draw HP
		if (hp<1){
			buttonHP[i] = lerp(buttonHP[i], hp, 0.2);
		
			var hpH = 4;
			draw_9slice(cX, cY + cellSize - hpH, buttonHP[i]*cellSize, hpH, sItemHP, 0);
		}
		
		//Draw key (O/P)
	}
	
	//Keys
	var lerpSpeed = 0.1;
	if (keyScale[i]>global.keySelectScale*0.9) lerpSpeed = 0.02;
	keyScale[i] = lerp(keyScale[i], 1+(oPlayer.state==st.itemselect)/2, lerpSpeed);
	
	if (i==0){
		draw_sprite_ext(sItemKeyO, 0, cX-1, cY+cellSize/2, keyScale[i], keyScale[i], 0, -1, 1);
	}
	else if (i==1){
		draw_sprite_ext(sItemKeyP, 0, cX+cellSize, cY+cellSize/2, keyScale[i], keyScale[i], 0, -1, 1);
	}
	
	//Choose a slot
	if (oPlayer.state == st.itemselect){
		draw_text_center(CAM.WIDTH/2, CAM.HEIGHT - (cellSize + 8*max(0, keyScale[i]-1)), "Choose a slot",
			.5, .5, 0);
	}
}
#endregion

}

#region Debug
//Power lines debugging
//draw_text(mouseX, mouseY-16, "Line Status: " + string(PL_get_active(mouse_x, mouse_y)));
//var active = PL_get_active(mouse_x, mouse_y);
//if (mouse_check_button_pressed(mb_left) && active>=0){
//	PL_set_active(mouse_x, mouse_y, !active)
//}

if (global.debug){
	//Room type
	draw_set_halign(fa_right);
	
	var _s = "";
	_s += "Room Type: " + string(roomType);
	
	draw_text(CAM.WIDTH - 16, 16, _s);
	
	draw_set_halign(fa_left);
	
	//Cutscene
	//if (global.ctsPos >= 0){
	//	var arr = global.ctsActors;
	//	for(var i=0; i<array_length_2d(arr, global.ctsType); i++){
	//		draw_text(32, 32+i*24, object_get_name(arr[global.ctsType, i]));
	//	}
	//}
	
	draw_text(32, 32, "Actors: " + string(instance_number(oActorParent)));
}

#endregion