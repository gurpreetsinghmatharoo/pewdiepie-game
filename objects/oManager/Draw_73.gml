/// @description 
//Shortcuts debug
if (global.shortcuts){
	//Distance test
	if (global.distTest){
		draw_set_color(c_lime);
		draw_line_width(lx1, ly1, lx2, ly2, 2);
		draw_set_color(c_white);
		
		var dist = point_distance(lx1, ly1, lx2, ly2);
		
		draw_text(lx1 + 16, ly1 + 4, dist);
	}
}