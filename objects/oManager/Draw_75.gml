/// @description Fade surface
//var fadeModifier = (global.fadeState==fdSt.start) - (global.fadeState==fdSt.finish);

//Finish
if (global.fadeState==fdSt.finish){
	//Black
	if (global.fadeAlpha < 1){
		draw_clear(0);
	}
	
	//Get scale
	var sX = CAM.WIDTH / surface_get_width(application_surface);
	var sY = CAM.HEIGHT / surface_get_height(application_surface);
	
	//Draw
	if (surface_exists(global.fadeSurf))
		draw_surface_ext(global.fadeSurf, 0, 0, sX, sY, 0, -1, global.fadeAlpha);
	
	//Subtract
	var fadeAdd = 1/FADETIME;
	global.fadeAlpha -= fadeAdd;
}

//Start
if (global.fadeState==fdSt.start){
	//Draw
	draw_set_color_alpha(0, global.fadeAlpha);
	draw_rectangle(0, 0, CAM.WIDTH, CAM.HEIGHT, 0);
	draw_set_color_alpha(-1, 1);
	
	//Subtract
	var fadeAdd = 1/FADETIME;
	global.fadeAlpha -= fadeAdd;
	
	//End
	if (global.fadeAlpha <=0){
		global.fadeState = fdSt.none;
		global.fadeAlpha = 1;
	}
}