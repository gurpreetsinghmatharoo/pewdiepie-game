/// @description 
//Is the level being restarted or is it being loaded through a teleporter?
var restarted = !file_exists(global.FNLevelChange);

#region Temp loading
if (!restarted){
	ini_open(global.FNLevelChange);
	
	//Main
	var flagCreatePlayer = ini_read_real("main", "flagCreatePlayer", 0);
	var targetRoom = ini_read_real("target", "room", noone);
	var targetInst = ini_read_real("target", "inst", noone);
	
	//Errors
	if (targetRoom!=room){
		show_debug_message("Room is different from the target room.");
	}
	else if (!instance_exists(targetInst)){
		show_debug_message("Target spawn point does not exist");
	}
	
	else if (flagCreatePlayer){
		//Remove player
		if (instance_exists(oPlayer)){
			if (instance_exists(oPlayer.follower)) instance_destroy(oPlayer.follower);
			
			instance_destroy(oPlayer);
		}
		
		//Create player
		instance_create_layer(targetInst.x, targetInst.y, "Instances_0", oPlayer);
		
		//Player
		oPlayer.hsp = ini_read_real("player", "hsp", 0);
		oPlayer.vsp = ini_read_real("player", "vsp", 0);
		oPlayer.boostX = ini_read_real("player", "boostX", 0);
		oPlayer.boostY = ini_read_real("player", "boostY", 0);
		oPlayer.state = ini_read_real("player", "state", 0);
		global.playerHP = ini_read_real("player", "hp", 100);
		
		//Spawn speeds
		targetInst.image_alpha = 0.5; //To indicate that the player's speeds can be set from that instance
		
		//Follower
		oPlayer.follower = instance_create_layer(oPlayer.x, oPlayer.y,
								oPlayer.layer, ini_read_real("follower", "obj", oEdgar));
		
		oPlayer.follower.hp = ini_read_real("follower", "hp", 100);
		
		//Inventory
		for(var i=0; i<global.maxInv; i++){
			for(var j=0; j<array_length_2d(global.inv, i); j++){
				global.inv[i, j] = ini_read_real("inv", string(i)+string(j), 0);
			}
		}
	}
	
	//Close and delete
	ini_close();
	file_delete(global.FNLevelChange);
}
#endregion

#region Room state
roomLoad(restarted);
log("---------------------LOADED----------------------");
roomSave();
#endregion

#region Cutscenes
//Always play the first cutscene
cutsceneAt(cts.opening, 0, rmHomeInside);

if (global.ctsEnabled){
	//Openings
	cutsceneAt(cts.outside0, 0, rmHome);
	
	//Demo end
	cutsceneAt(cts.demoEnd, 0, rmW1_2);
}
#endregion

#region Move camera to player
//Vars
var camW = camera_get_view_width(view_camera);
var camH = camera_get_view_height(view_camera);

//To follow
var toFollow = oPlayer;
if (!instance_exists(toFollow)) toFollow = oActorPoods;
	
if (instance_exists(toFollow)){
	//Camera
	var cX, cY;
	cX = toFollow.x - camW/2;
	cY = toFollow.y - camH/2;
	cX = clamp(cX, 0, room_width-camW);
	cY = clamp(cY, 0, room_height-camH);

	camera_set_view_pos(view_camera, cX, cY);
}
#endregion

#region Previous room stats
///Save
//Map data
//global.prevMap = ds_map_create();
//ds_map_copy(global.prevMap, global.map);

//Player position
//if (instance_exists(oPlayer)){
//	global.playerPrevX = oPlayer.x;
//	global.playerPrevY = oPlayer.y;
//}
#endregion