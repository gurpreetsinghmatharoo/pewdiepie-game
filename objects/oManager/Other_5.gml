/// @description 
//Save room state
if (!instance_exists(oPlayer) && !global.restarting){ //Player not existing means that the door was used
	instance_activate_all();
	roomSave();
	instance_deactivate_all(1);
	
	log("---------------------SAVED----------------------");
}