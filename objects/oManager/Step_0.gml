/// @description 
//show_debug_overlay(global.GUIEnabled);

#region Inventory
//Inventory info
var item1 = global.inv[0, item.type];
var item2 = global.inv[1, item.type];

//GUI
var guiW = display_get_gui_width();
var guiH = display_get_gui_height();

//Inventory
invW = cellSize*global.maxInv;
invH = cellSize;
invX = guiW/2 - invW/2;
invY = guiH - invH;

//Input
var mX = device_mouse_x_to_gui(0);
var mY = device_mouse_y_to_gui(0);
mCellX = (mX-invX) div cellSize;
mIn = point_in_rectangle(mX, mY, invX, invY, invX+invW, invY+invH);

//Inventory Loop
for(var i=0; i<global.maxInv; i++){
	//Get info
	var hp = global.inv[i, item.hp];
	var item = global.inv[i, item.type];
	var usecd = global.inv[i, item.usecd];
	var refillcd = global.inv[i, item.refillcd];
	
	//Refill
	if (hp<1 && !refillcd) global.inv[i, item.hp] += global.items[item, pr.refill];
	
	//Refill cooldown
	if (refillcd>0) global.inv[i, item.refillcd] -= 1;
	
	//Use countdown
	if (usecd>0) global.inv[i, item.usecd] -= 1;
}
#endregion

#region Scrap
//Select
/*var mCellClamp = clamp(mCellX, 0, global.maxInv-1);
//var itemMouse = global.inv[mCellClamp];
if (mIn && global.LMBPressed){
	global.equipped = mCellClamp;
}

//Select using key
if (global.Ikey){
	global.equipped = 0;
}
if (global.Okey){
	global.equipped = 1;
}*/
#endregion

#region Admin
if (global.shortcuts){
	//Distance test
	if (global.distTest){
		if (!lstate && mouse_check_button_pressed(mb_left)){
			lx1 = floor(mouse_x/16)*16;
			ly1 = floor(mouse_y/16)*16;
			lstate = 1;
		}
		
		if (lstate){
			lx2 = floor(mouse_x/16)*16;
			ly2 = floor(mouse_y/16)*16;
		}
		
		if (lstate && mouse_check_button_released(mb_left)){
			lstate = 0;
		}
	}
	
	//Context menu
	if (mouse_check_button_pressed(mb_right)){
		//Destroy old menu
		if (instance_exists(ctxMenu)) instance_destroy(ctxMenu);
		
		//Build array with options
		var arr = [];
		for(var i=0; i<CTX_OPTIONS; i++){
			arr[i] = i;
		}
		
		//Create menu
		ctxMenu = ctxCreate(mouse_x, mouse_y, ctxT.root, arr);
	}
	
	//GUI
	if (keyboard_combination(vk_control, ord("G"))) global.GUIEnabled = !global.GUIEnabled;
	
	//Restart
	if (keyboard_check_pressed(ord("R"))){
		//Restore stats
		//ds_map_copy(global.map, global.prevMap);
		//ds_map_destroy(global.prevMap);
		
		//Reset cutscene
		global.ctsPos = -1;
		
		//Restart
		global.restarting = true;
		room_restart();
	}
	
	//Room change
	var roomInput = keyboard_check_pressed(ord("0")) - keyboard_check_pressed(ord("9"));
	var newRoom = room + roomInput;
	var roomChanged = false;
	
	if (room_exists(newRoom) && abs(roomInput)){
		room_goto(newRoom);
		roomChanged = true;
	}
	
	//Room speed
	if (keyboard_combination(vk_control, ord("S"))){
		if (room_speed==60){
			room_speed = 2;
		}
		else{
			room_speed = 60;
		}
	}
	
	//Player speed
	if (keyboard_combination(vk_control, ord("P"))){
		if (oPlayer.moveSpeed==2){
			oPlayer.moveSpeed = 0.75;
			oPlayer.image_speed = 0.65;
			
			instance_destroy(oPlayer.follower);
		}
		else{
			oPlayer.moveSpeed = 2;
			oPlayer.image_speed = 1;
		}
	}
	
	//Skip cutscene pos
	if (keyboard_combination(vk_control, ord("C")) && global.ctsPos>=0){
		global.ctsPos++;
	}
	
	//Skip cutscene
	if ((keyboard_combination(vk_control, ord("C"), vk_shift) && global.ctsPos>=0) || roomChanged){
		cutsceneEndForce();
	}
	
	//Move player
	if (mouse_check_button_pressed(mb_middle) && instance_exists(oPlayer)){
		oPlayer.x = mouse_x;
		oPlayer.y = mouse_y;
	}
}
#endregion