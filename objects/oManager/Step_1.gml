/// @description 
getInput();

#region Main
//Time in room
global.roomTime = (global.roomTime+1) mod 100000;
#endregion

#region Cutscenes
if (global.ctsPos >= 0){
	//Previous
	global.ctsPosPrev = global.ctsPos;
	var firstFrame = global.ctsPosPrev != global.ctsPos;

	//Get animation
	var _anim = global.ctsAnim[global.ctsType, global.ctsPos];
	var _actor = _anim[adata.actor];
	var _type = _anim[adata.atype];
	var _data = _anim[adata.data];
	
	//Run code based on the type of animation
	switch (_type){
		//Move
		case atype.move:
			//Get data
			var targetX = _actor.x + _data[0];
			var targetY = _actor.y + _data[1];
		
			//Set speed (atm, hsp only)
			with (_actor){
				//state = st.idle;
				faceForced = false;
				hor = sign(_data[0]) * moveSpeed;
			}
			
			//Check for completion (atm, hsp only)
			if (abs(_actor.travelX) >= abs(_data[0])){
				global.ctsPos++;
				
				//Reset actor variables
				_actor.hor = 0;
				_actor.travelX = 0;
				_actor.travelY = 0;
			}
		break;
		
		//Move Async
		case atype.moveAsync:
			//Get data
			var targetX = _data[0];
			var targetY = _data[1];
		
			//Set speed (atm, hsp only)
			with (_actor){
				//state = st.idle;
				faceForced = false;
				moveAsync = true;
				
				hor = sign(targetX) * moveSpeed;
				targetXMove = targetX;
				targetYMove = targetY;
			}
			
			//Complete
			global.ctsPos++;
		break;
		
		//Say
		case atype.say:
			//Get data
			var msg = _data;
			
			//Make it talk
			_actor.msg = msg;
			_actor.talk = true;
			
			if (firstFrame){
				_actor.msgDone = false;
			}
			
			//Check for completion
			if (_actor.msgDone){
				global.ctsPos++;
				
				//Reset talking
				_actor.talk = false;
				_actor.lineCD = 0;
				_actor.msgCD = 0;
				_actor.msgDone = 0;
				with (_actor) msg_stop();
			}
		break;
		
		//Say Async
		case atype.sayAsync:
			//Get data
			var msg = _data;
			
			//Make it talk
			_actor.msg = msg;
			_actor.talk = true;
			_actor.sayAsync = true;
			
			//Complete
			global.ctsPos++;
		break;
		
		//Say stop
		case atype.sayStop:
			//Stop saying
			_actor.talk = false;
			_actor.sayAsync = false;
			_actor.lineCD = 0;
			_actor.msgCD = 0;
			with (_actor) msg_stop();
			
			//Completion
			global.ctsPos++;
		break;
		
		//Speed
		case atype.spd:
			//Get data
			var _hsp = _data[0];
			var _vsp = _data[1];
		
			//Change speeds
			_actor.hsp = _hsp;
			_actor.vsp = _vsp;
			
			//Completion
			global.ctsPos++;
		break;
		
		//Face
		case atype.face:
			//Get data
			var face = _data[0];
			
			//Stop movement
			_actor.hsp = 0;
		
			//Change scale
			_actor.image_xscale = face;
			
			//Completion
			global.ctsPos++;
		break;
		
		//Face forced
		case atype.faceForced:
			//Get data
			var face = _data[0];
		
			//Change scale
			_actor.faceForced = true;
			_actor.image_xscale = face;
			
			//Completion
			global.ctsPos++;
		break;
		
		//Wait
		case atype.wait:
			//Get data
			var waitTime = _data[0];
			
			//Wait
			global.ctsWait++;
			
			//Check for completion
			if (global.ctsWait > waitTime){
				global.ctsPos++;
				
				global.ctsWait = 0;
			}
		break;
		
		//Progress add
		case atype.globalProgressAdd:
			//Get data
			var add = _data[0];
			
			//Add
			global.map[? "progress"] += add;
			
			//Completion
			global.ctsPos++;
		break;
		
		//Room progress add
		case atype.roomProgressAdd:
			//Get data
			var add = _data[0];
			
			//Add
			progressAdd(room, add);
			
			//Completion
			global.ctsPos++;
		break;
		
		case atype.addActor:
			//Get data
			var X = _data[0];
			var Y = _data[1];
			var Layer = _data[2];
			var Obj = _data[3];
			
			//Create instance
			instance_create_layer(X, Y, Layer, Obj);
			
			//Add actor
			animStart(global.ctsType);
			addActors(Obj);
			animEnd();
			
			//Completion
			global.ctsPos++;
		break;
		
		case atype.camPos:
			//Get data
			var cX = _data[0];
			var cY = _data[1];
			
			//Set
			global.ctsCamX = cX;
			global.ctsCamY = cY;
			
			//Move
			global.ctsPos++;
		break;
		
		case atype.camPosRaw:
			//Get data
			var cX = _data[0];
			var cY = _data[1];
			
			//Set
			global.ctsCamX = cX;
			global.ctsCamY = cY;
			camera_set_view_pos(view_camera, cX, cY);
			
			//Move
			global.ctsPos++;
		break;
		
		case atype.camStop:
			//Get data
			var cX = _data[0];
			var cY = _data[1];
			
			//Vars
			var camX = camera_get_view_x(view_camera);
			var camY = camera_get_view_y(view_camera);
			
			//Set
			global.ctsCamX = camX + cX;
			global.ctsCamY = camY + cY;
			
			//Move
			global.ctsPos++;
		break;
		
		case atype.addInst:
			//Get data
			var X = _data[0];
			var Y = _data[1];
			var Layer = _data[2];
			var Obj = _data[3];
			
			//Create
			instance_create_layer(X, Y, Layer, Obj);
			
			//Move
			global.ctsPos++;
		break;
		
		case atype.destInst:
			//Get data
			var Obj = _data[0];
			
			//Destroy
			instance_destroy(Obj);
			
			//Move
			global.ctsPos++;
		break;
		
		case atype.setState:
			//Get data
			var State = _data[0];
			
			//Set
			with (_actor){
				state_set(State);
			}
			
			//Complete
			global.ctsPos++;
		break;
		
		case atype.camShake:
			//Get data
			var shX = _data[0];
			var shY = _data[1];
			var shF = _data[2];
			
			//Shake
			global.shakeX = shX;
			global.shakeY = shY;
			global.shakeFalloff = shF;
			
			//Sound
			audio_play_sound(sndShake, 1, 0);
			
			//Complete
			global.ctsPos++;
		break;
		
		case atype.setMoveSpeed:
			//Get data
			var Spd = _data[0];
			
			//Set
			_actor.moveSpeed = Spd;
			
			//Complete
			global.ctsPos++;
		break;
		
		case atype.setImageSpeed:
			//Get data
			var Spd = _data[0];
			
			//Set
			_actor.image_speed = Spd;
			
			//Complete
			global.ctsPos++;
		break;
	}
	
	//End cutscene
	if (global.ctsPos >= array_length_2d(global.ctsAnim, global.ctsType)){
		cutsceneEnd();
	}
}
#endregion