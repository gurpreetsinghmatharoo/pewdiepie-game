/// @description 
//Vars
var camX = camera_get_view_x(view_camera);
var camY = camera_get_view_y(view_camera);
var camW = camera_get_view_width(view_camera);
var camH = camera_get_view_height(view_camera);

//To follow
var toFollow = oPlayer;
if (!instance_exists(toFollow)) toFollow = oActorPoods;

//Shake values
var shX = random_range(-global.shakeX, global.shakeX);
var shY = random_range(-global.shakeY, global.shakeY);

if (global.ctsCamX != undefined && global.ctsCamY != undefined){
	cX = lerp(camX, global.ctsCamX + shX, global.camLerp);
	cY = lerp(camY, global.ctsCamY + shY, global.camLerp);
}
else if (instance_exists(toFollow)){
	//Lerp speed
	var lerpS = global.camLerp;
	if (abs(toFollow.hsp)){
		lerpS *= 4;
	}
	
	//Camera
	var cX, cY;
	cX = lerp(camX, (toFollow.x - camW/2) + shX, lerpS);
	cY = lerp(camY, (toFollow.y - camH/2) + shY, lerpS);
	cX = clamp(cX, 0, room_width-camW);
	cY = clamp(cY, 0, room_height-camH);
}else{
	var cX = camX;
	var cY = camY;
}

camera_set_view_pos(view_camera, cX, cY);

//Reduce shake
global.shakeX -= (global.shakeX > 0) * global.shakeFalloff;
global.shakeY -= (global.shakeY > 0) * global.shakeFalloff;

//Clouds
if (roomType==rmType.main){
	cloudBackHSP = lerp(cloudBackHSP, cloudBackSpeed * windX, 0.05);
	cloudFrontHSP = lerp(cloudFrontHSP, cloudFrontSpeed * windX, 0.05);

	cloudBackX += cloudBackHSP;
	cloudFrontX += cloudFrontHSP;
}

//Background
layer_x("Back_3", cX/1.1);
layer_x("Back_2", cloudBackX + cX/1.2);
layer_x("Back_1", cloudFrontX + cX/1.3);
layer_x("Back_0", cX/1.4);

var roomYPerc = cY/(room_height-camH);
var backY = roomYPerc * (backHeight-camH);

//Background y
//Exteriors only
if (roomPlace==rmType.exterior){
	if (roomType==rmType.main){
		layer_y("Back_3", cY - backY*0.7);
		layer_y("Back_2", cY - backY*0.8);
		layer_y("Back_1", cY - backY*0.9);
		layer_y("Back_0", cY - backY);
	}
	else{
		layer_y("Back_3", cY - backY);
		layer_y("Back_2", cY - backY);
		layer_y("Back_1", cY - backY);
		layer_y("Back_0", cY - backY);
	}
}

//Move interior hills up
if (roomPlace==rmType.interior && roomType==rmType.main){
	var newY = -48;
	layer_y("Back_3", newY);
	layer_y("Back_2", newY);
	layer_y("Back_1", newY);
	layer_y("Back_0", newY);
}