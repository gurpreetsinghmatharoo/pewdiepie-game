{
    "id": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oManager",
    "eventList": [
        {
            "id": "8ae23db8-b8a8-4325-a425-f7c66d039b2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        },
        {
            "id": "82f76038-9f7c-4ab1-8728-477e79cf9fc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        },
        {
            "id": "4e458af5-3ce9-409e-a421-67165ebf0d78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        },
        {
            "id": "6cd5b880-c60a-4dc7-a306-21083feb3b43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        },
        {
            "id": "20ea640b-0115-40d9-9798-3126e741aaf9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        },
        {
            "id": "e3eadd91-7b45-4ebf-85a4-c2939f5696ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        },
        {
            "id": "86561c52-1052-4776-8c34-a5f8d8f00723",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        },
        {
            "id": "acf58dec-c3e8-47d3-9f57-14e98908db35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        },
        {
            "id": "4d18405b-01d1-4300-b3df-e2cc6f6d4af7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        },
        {
            "id": "c05b50c1-18fe-4587-aebc-cc15d7dc0506",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        },
        {
            "id": "f12d77fc-c0f2-43b1-83da-b53f8f6bc7f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        },
        {
            "id": "b23e0e06-397b-4dce-9712-7d483fb942a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        },
        {
            "id": "6aa30cd3-6503-4755-9aeb-32962451920a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        },
        {
            "id": "dbb0e44a-2be7-4351-8460-46cfa4bc4b88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "3c6d5ff6-f3bc-4567-ab61-0c8756b20e77"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}