/// @description 
//Title
draw_set_font(ftMain);

var _x = room_width/2;
var _y = room_height*0.12 + sin(current_time/800)*4;

draw_set_color(c_black);
draw_text_center(_x+1, _y+1, "PewDiePie Game Demo");

draw_set_color(c_yellow);
draw_text_center(_x, _y, "PewDiePie Game Demo");
draw_set_color(c_white);

//Controls
var controls = "Controls:\n" +
"[WASD/Arrow keys] for movement\n" +
"[Space] to jump\n" +
"[E] to equip item to a slot\n" +
"[O/P] to select a slot or use it\n" +
"[ALT+ENTER] for fullscreen";

_y = room_height * 0.5;

draw_set_color(c_black);
draw_set_alpha(0.4);
draw_rectangle(0, room_height*0.27, room_width, room_height*0.74, 0);
draw_set_alpha(1);

draw_text_center(_x+1, _y+1, controls);

draw_set_color(c_white);
draw_text_center(_x, _y, controls);

//Hit enter
var text = "Hit [Enter] to play";

_y = room_height * 0.88;

draw_set_color(c_black);
draw_text_center(_x+1, _y+1, text);

draw_set_color(c_white);
draw_text_center(_x, _y, text);