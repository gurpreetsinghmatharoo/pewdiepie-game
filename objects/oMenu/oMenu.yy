{
    "id": "cd19c780-440c-4de1-b648-c9ed16c3cdb7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMenu",
    "eventList": [
        {
            "id": "efb77f45-cdd4-40c0-87d4-359d6a097817",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cd19c780-440c-4de1-b648-c9ed16c3cdb7"
        },
        {
            "id": "7afd5f97-a481-49f5-8569-2d3038dd79d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "cd19c780-440c-4de1-b648-c9ed16c3cdb7"
        },
        {
            "id": "2473cf0f-7136-45b4-a810-1253de6933ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cd19c780-440c-4de1-b648-c9ed16c3cdb7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}