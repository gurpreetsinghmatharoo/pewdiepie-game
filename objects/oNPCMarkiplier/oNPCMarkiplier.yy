{
    "id": "ff6846dd-00e6-4d28-b9e3-8b1976f2d870",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNPCMarkiplier",
    "eventList": [
        {
            "id": "3e1e086f-26a7-41cf-8399-f0d779f29d02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ff6846dd-00e6-4d28-b9e3-8b1976f2d870"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c8344a21-d229-42dc-bac0-6dbade58c92e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "d511615f-ed66-431b-816f-626802de6662",
    "visible": true
}