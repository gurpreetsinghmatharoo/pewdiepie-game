/// @description 
//Cutscene
moveAsync = false;
sayAsync = false;
faceForced = false;

//Props
moveSpeedOrg = 1;
moveSpeed = moveSpeedOrg;
jumpSpeed = 6;
gravSpeed = 1;
gravInterval = 2;

pushSpeed = 1;

image_speed = 0.75;

//Vars
hsp = 0;
vsp = 0;

hor = 0;
ver = 0;

boostX = 0;
boostY = 0;

gravCooldown = 0;

grounded = false;
groundedPrev = false;

travelX = 0;
travelY = 0;
targetXMove = 0;
targetYMove = 0;

chairPartCD = 0;

createdInCol = false;

//State
state = st.idle;

//Textbox
tbScaleX = 1;
tbScaleY = 0;

//Dialogue
talk = false;
talkEnd = false;

talkRad = 16;

lineWait = 60;
msgWait = 60;

lineCD = 0;
msgCD = 0;

msgDone = false;

msg = [];