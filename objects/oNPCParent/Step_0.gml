/// @description 
//Vars
groundedPrev = grounded;
grounded = collision(0, 1);

//States
switch(state){
	//Normal
	case st.idle: case st.move: case st.jump: case st.push: case st.walkMarzia:
		//Follow
		if (object_index==oActorEdgar && instance_exists(oActorPoods)){
			var diff = oActorPoods.x - x;
			
			if (abs(diff) > 32){
				hor = sign(diff);
			}
			else{
				hor = 0;
			}
		}
	
		//Movement
		hsp = lerp(hsp, hor, 0.1);
		
		//Jump
		var jDetect = 8;
		if (collision(hsp*jDetect, 0) && !collision(hsp*jDetect, -jumpSpeed * 8)){
			vsp = -jumpSpeed;
			
			scale_set(1, 1.2, 30);
		}
		
		//Functions
		playerGravity();
		playerCollisions();
		
		//Apply
		x += hsp;
		y += vsp;
		travelX += hsp;
		travelY += vsp;
		
		//Animation
		if (state!=st.walkMarzia){
			if (!grounded){
				state = st.jump;
				if (abs(hsp) && !faceForced) image_xscale = sign(hsp);
			}
			else if (abs(hsp)){
				//Get collision
				var col = collision(sign(hsp), 0);
			
				//Pushing
				if (col>1 && object_get_parent(col.object_index)==oPushableParent){
					state = st.push;
				}
				//Move
				else{
					state = st.move;
				}
			
				if (!faceForced) image_xscale = sign(hsp);
			}
			else{
				state = st.idle;
				if (facePlayer){
					image_xscale = sign(oPlayer.x - x);
					image_xscale = image_xscale==0 ? 1 : image_xscale;
				}
			}
		}
		else{
			//Flip
			if (abs(hsp)) image_xscale = sign(hsp);
		}
	break;
}

//Sprite
sprite_index = sprite[state];
		
//Move async
if (moveAsync && abs(travelX) >= abs(targetXMove)){
	hor = 0;
	travelX = 0;
	travelY = 0;
	targetXMove = 0;
	targetYMove = 0;
	moveAsync = false;
}

//Talking
if (collision_circle(x, y, talkRad, oPlayer, 0, 0) && array_length_1d(msg)>0){
	if (!talk && !talkEnd){
		talk = true;
	}
	
	image_xscale = sign(oPlayer.x - x);
	
	if (image_xscale==0) image_xscale = 1;
}
else{
	talk = false;
	talkEnd = false;
}

//Walk sound
soundWalk();