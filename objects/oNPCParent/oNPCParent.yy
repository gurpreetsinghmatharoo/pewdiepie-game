{
    "id": "c8344a21-d229-42dc-bac0-6dbade58c92e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNPCParent",
    "eventList": [
        {
            "id": "dfe14bb8-3acd-4717-ab0a-8132824f7d78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c8344a21-d229-42dc-bac0-6dbade58c92e"
        },
        {
            "id": "6247ee70-261c-4115-a336-6aa321f81cf4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c8344a21-d229-42dc-bac0-6dbade58c92e"
        },
        {
            "id": "4c4535b0-12fe-4a76-b0f4-e02e34a3ed49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c8344a21-d229-42dc-bac0-6dbade58c92e"
        },
        {
            "id": "478d6e53-92d5-4ad5-a89d-e12f0e927946",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "c8344a21-d229-42dc-bac0-6dbade58c92e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "21133b5e-cc44-4641-93a2-2c41b6488b40",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "29470465-f686-4c93-93a9-2f13bfc75bfc",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "facePlayer",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}