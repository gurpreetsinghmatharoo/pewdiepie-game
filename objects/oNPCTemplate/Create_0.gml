/// @description 
state = st.idle;

//Sprites
sprite[st.idle] = sDemon_Idle;
sprite[st.move] = sDemon_Move;

mask_index = sprite[st.idle];

//Props
moveSpeed = 2;
jumpSpeed = 6;
gravSpeed = 1;
gravInterval = 2;

//Vars
hp = 4;

hsp = 0;
vsp = 0;

moveDir = choose(1, -1);

gravCooldown = 0;

grounded = false;
groundedPrev = false;

//Boost
boostSpeed = 5;

boostX = 0;

//Scaling
scaleX = 1;
scaleY = 1;
scaleXRaw = 1;
scaleYRaw = 1;
scaleSpeed = 0.1;

//White
whiteAlpha = 0;