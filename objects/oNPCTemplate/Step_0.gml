/// @description 
//Item info
var itemSlot = -1;
if (global.Okey){
	itemSlot = 0;
}
else if (global.Pkey){
	itemSlot = 1;
}

var item = global.inv[max(0, itemSlot), item.type];
var itemHP = global.inv[max(0, itemSlot), item.hp];
var itemUse = 0;

if (item>=0){
	itemUse = global.items[item, pr.use];
}

//Vars
grounded = collision(0, 1);

//States
switch(state){
	case st.idle: case st.move: case st.jump:
		mask_index = sPlayer_Idle;
	
		//Movement
		hsp = moveDir * moveSpeed;
		hsp += boostX;
	
		//Gravity
		playerGravity();

		//Jump
		//if (global.jump){// && grounded){
		//    vsp = -jumpSpeed;
			
		//	//Scale
		//	scale_set(1, 1.2, 30);
		//}
		
		//Hit ground scale
		if (grounded && !groundedPrev){
			scale_set(1.4, 0.6, 5);
		}

		//Collisions
		enemyCollisions();

		//Apply
		x += hsp;
		y += vsp;
		
		//State
		//if (!grounded) state = st.jump;
		if (hsp!=0) state = st.move;
		else state = st.idle;
		
		//Scale
		image_xscale = state==st.move ? sign(hsp) : image_xscale;
		
		//Hurt
		var hurtInst = enemyHurt();
		
		if (hurtInst){
			hp--;
			whiteAlpha = 1;
			
			//Boost
			var dir = point_direction(hurtInst.x, hurtInst.y, x, y);
			boostX = lengthdir_x(boostSpeed, dir);
			vsp = lengthdir_y(boostSpeed, dir);
			
			//Delete
			if (hurtInst.object_index!=oPlayer){
				instance_destroy(hurtInst);
			}
		}
	break;
}

//Sprite
sprite_index = sprite[state];

//Scaling
scaleX = lerp(scaleX, scaleXRaw, scaleSpeed);
scaleY = lerp(scaleY, scaleYRaw, scaleSpeed);

//Boost
boostX = lerp(boostX, 0, 0.1);

//White
whiteAlpha -= (whiteAlpha>0)/10;

//Dead
if (hp<0){
	state_set(st.dead);
}