{
    "id": "222fe391-f947-443a-8110-f7af65aeffde",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNPCTemplate",
    "eventList": [
        {
            "id": "e055265d-c7d9-4b9f-9c61-9590a8e53b4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "222fe391-f947-443a-8110-f7af65aeffde"
        },
        {
            "id": "765428d3-88ef-4014-b32d-bf72dbcb5b14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "222fe391-f947-443a-8110-f7af65aeffde"
        },
        {
            "id": "2861a4bc-ec70-4bcc-aaaf-12f9f4e7a868",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "222fe391-f947-443a-8110-f7af65aeffde"
        },
        {
            "id": "8d783756-5d20-49c0-90ab-946be2df40db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "222fe391-f947-443a-8110-f7af65aeffde"
        },
        {
            "id": "e43ca651-4a1c-4e7c-b440-955b9ee5cd27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "222fe391-f947-443a-8110-f7af65aeffde"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}