{
    "id": "2e09ded0-b4a7-476b-b424-054de96d4c5e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oNote",
    "eventList": [
        {
            "id": "d37a9c29-2e2e-4563-b68e-1018d62373ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2e09ded0-b4a7-476b-b424-054de96d4c5e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4d9262cd-64be-44f7-902f-6ba6d74f3ffd",
    "visible": false
}