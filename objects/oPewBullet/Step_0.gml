/// @description 
//Out of bounds
if (x<-64 || x>room_width+64 || y<-64 || y>room_height+64 || collision(0, 0)){
	instance_destroy();
}

//Move
x += hsp;
y += vsp;

//Angle
image_angle = point_direction(0, 0, hsp, vsp);