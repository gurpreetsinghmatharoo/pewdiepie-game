{
    "id": "abb2da93-4641-4819-bb90-b3209efb6eb6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPewBullet",
    "eventList": [
        {
            "id": "e56e2572-583b-4f66-b2d6-67f4adf34e63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "abb2da93-4641-4819-bb90-b3209efb6eb6"
        },
        {
            "id": "d56a292a-5761-446a-bcd6-7b56730d1fc8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "abb2da93-4641-4819-bb90-b3209efb6eb6"
        },
        {
            "id": "a5302903-7864-4da3-9da8-b89cc8ddda9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "abb2da93-4641-4819-bb90-b3209efb6eb6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "421fde70-58d5-41a0-aa88-770f5634460e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "9b04aa01-44a6-407d-9442-a45ec1a6f49b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "$FF2300D3",
            "varName": "color[0]",
            "varType": 7
        },
        {
            "id": "0632eed5-ad89-4d82-b70c-28af5d39ac8f",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "$FFCE5323",
            "varName": "color[1]",
            "varType": 7
        }
    ],
    "solid": false,
    "spriteId": "38a12f03-932a-4db9-935d-861fe6fff187",
    "visible": true
}