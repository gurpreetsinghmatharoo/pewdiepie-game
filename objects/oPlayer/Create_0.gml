/// @description 
//Cutscenes
//actor = oActorPoods;

//States
state = st.idle;
stateType = stt.normal;
invincible = false;
follower = oEdgar;

//Sprites
playerSpriteInit();

//Props
moveSpeed = 2;
jumpSpeed = 6;
jumpSpeedHold = 0.2;
wallJumpHorSpeed = 8;
gravSpeed = 1;
gravInterval = 2;

pushSpeed = 1;

lerpSpeed = 0.2;

//Vars
hsp = 0;
vsp = 0;
jsp = jumpSpeedHold;

boostX = 0;
boostY = 0;

groundX = x;
groundY = y;

gravCooldown = 0;

grounded = false;
groundedPrev = false;

refillTimer = 0;

createdInCol = false;

//State vars
deadTime = 0;

//Scaling
scaleX = 1;
scaleY = 1;
scaleXRaw = 1;
scaleYRaw = 1;
scaleXPrev = 1;
scaleYPrev = 1;
scaleSpeed = 0.1;

//Item vars
pewType = st.pew;
pewShot = true; //Used for all gun type items

chairRot = 0;
chairRotSpeed = 12;

//Particles
chairPartCD = 0;

//Enemy interaction
hitCooldown = 0;

knockbackSpeedX = 8;
knockbackSpeedY = -5;

//White
whiteAlpha = 0;