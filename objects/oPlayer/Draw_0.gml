/// @description 
playerDraw();

//White
if (whiteAlpha>0){
	shader_set(shWhite);
	
	draw_sprite_ext(sprite_index, image_index, x, y,
		scaleX*image_xscale, scaleY, image_angle, image_blend, whiteAlpha);
		
	shader_reset();
}

//Item Selected
if (state==st.itemselect){
	var spr = global.items[global.itemSelected, pr.sprite];
	var scale = 1 + sin(current_time / 500)*0.1;
	
	draw_sprite_ext(spr, 0, x + bool(image_xscale), bbox_top - 12, scale, scale, 0, -1, 1);
}

//Debug
if (global.debug){
	//Scale
	//draw_text_center(x, y-32, "Scale: " + string(image_xscale) + ", " + string(image_yscale));

	//Mask
	gpu_set_blendmode_ext(bm_inv_dest_color, bm_zero);
	draw_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, 1);
	gpu_set_blendmode(bm_normal);
	
	//Vars
	draw_values(x, y, hsp, "hsp", vsp, "vsp", x, "x", y, "y");
}

#region Flex gun test
//var dir8 = ((global.axisDirOn div 45) * 45) mod 360; //8 directional
//var dir4 = dir8; //one sided
//if (dir8>90 && dir8<270) dir4 = (-global.axisDirOn + 180) mod 360;

//var x8 = x + lengthdir_x(32, dir8);
//var y8 = y + lengthdir_y(32, dir8);

//var x4 = x + lengthdir_x(32, dir4);
//var y4 = y + lengthdir_y(32, dir4);

//draw_circle(x8, y8, 8, 1);
//draw_circle(x4, y4, 9, 0);

//draw_text(x, y, global.inv[0, item.usecd]);
#endregion