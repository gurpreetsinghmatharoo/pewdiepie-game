/// @description 
//Reset invincibility
invincible = false;

//Item info
var itemSlot = -1;
if (global.Okey){
	itemSlot = 0;
}
else if (global.Pkey){
	itemSlot = 1;
}

var item = global.inv[max(0, itemSlot), item.type];
var itemHP = global.inv[max(0, itemSlot), item.hp];
var itemUse = 0;
var itemCD = global.inv[max(0, itemSlot), item.usecd];

if (item>=0){
	itemUse = global.items[item, pr.use];
}

//Vars
grounded = collision(0, 1);
slope = false;
var isWalling = (state==st.walltoright) - (state==st.walltoleft);

//Reset jump
if (grounded){
	jsp = jumpSpeedHold;
}

//Disable movement
if (isWalling!=0 && abs(global.horHoldTime)<10) global.hor = 0;
if (abs(boostX)) global.hor = 0;

//States
switch(state){
	case st.idle: case st.move: case st.jump: case st.walltoleft: case st.walltoright: case st.push:
		//Movement
		var _lerpSpeed = lerpSpeed;
		if (state==st.push) _lerpSpeed = 1;
		
		playerMovement(_lerpSpeed);
		//hsp = global.hor * moveSpeed;
		
		//Gravity
		playerGravity();

		//Jump
		if (global.jump && (grounded || state==st.walltoright || state==st.walltoleft)){
			boostX = wallJumpHorSpeed * -isWalling;
		    vsp = -jumpSpeed// / (1+abs(isWalling));
			
			//Scale
			scale_set(1, 1.2, 30);
			
			//Sound
			audio_play_sound(sndJump, 1, 0);
		}
		
		//Hit ground scale
		if (grounded && !groundedPrev){
			scale_set(1.4, 0.6, 5);
		}

		//Collisions
		playerCollisions();

		//Apply
		x += hsp;
		y += vsp;
		
		enemyInteraction();
		
		//Update grounded
		grounded = collision(0, 1);
		
		//State
		if (!grounded && !slope){
			//Wall to right
			if (collision(1, 0)){
				state = st.walltoright;
				image_xscale = -1;
				scale_set(1, 1.2, 2);
			}
			//Wall to left
			else if (collision(-1, 0)){
				state = st.walltoleft;
				image_xscale = 1;
				scale_set(1, 1.2, 2);
			}
			//Jump
			else{
				state = st.jump;
				if (abs(hsp)) image_xscale = sign(hsp);
			}
		}
		else if (abs(hsp)){
			//Get collision
			var col = collision(sign(hsp), 0);
			
			//Pushing
			if (col>1 && object_get_parent(col.object_index)==oPushableParent){
				state = st.push;
			}
			//Move
			else{
				state = st.move;
			}
			
			image_xscale = sign(hsp);
		}
		else state = st.idle;
		
		//Jump Image Index
		if (state==st.jump){
			var jumpPerc;
			if (vsp<0) 
			//jumpPerc = abs(vsp)/jumpSpeed;
			jumpPerc = smoothstep(0, jumpSpeed, abs(vsp));
			else if (vsp>0) 
			//jumpPerc = vsp/10;
			jumpPerc = smoothstep(0, 10, vsp);
			else jumpPerc = collision(0, 1) || collision(1, 0) || collision(-1, 0);
			
			var jumpFrames = sprite_get_number(sprite[st.jump]);
			image_index = clamp(jumpPerc * jumpFrames, 0, jumpFrames-1);
		}
		
		//Use
		if (itemSlot>=0 && item>=0 && itemHP>=itemUse && !itemCD){
			//Item action
			switch(item){
				//Pew gun
				case type.pew:
					//State, pew type
					if (grounded){
						state_set(pewType);
						pewShot = false;
						itemUseUp(type.pew);
					}
				break;
				
				//Chair
				case type.chair:
					//State, chair enter
					state_set(st.chairenter);
				break;
				
				//Flex gun
				case type.flexgun:
					if (grounded){
						state_set(st.flexgun_0);
						pewShot = false;
						itemUseUp(type.flexgun);
					}
				break;
			}
		}
	break;
	
	//Pews
	case st.pew: case st.pew2:
		//Shoot
		if (!pewShot && floor(image_index)==1){
			//Get bullet speed
			var bulletSpeed = global.items[type.pew, pr.spd];
			
			//Bullet position
			var bX = image_xscale>0 ? bbox_right+8 : bbox_left-8;
			var bullet = instance_create_layer(bX, y-2, "Instances_m1", oPewBullet);
			bullet.hsp = bulletSpeed*image_xscale;
			
			//Set towards nearest enemy
			var nearEnemy = instance_nearest(x, y, oEnemies);
			
			if (instance_exists(nearEnemy)){
				//Get properties
				var distRange = global.items[type.pew, pr.distrange];
				var dirRange = global.items[type.pew, pr.dirrange];
				
				//Get direction and distance
				var enemyDir = point_direction(bullet.x, bullet.y, nearEnemy.x, nearEnemy.y);
				var enemyDist = point_distance(bullet.x, bullet.y, nearEnemy.x, nearEnemy.y);
				
				var bulletDir = point_direction(0, 0, bullet.hsp, 0);
				
				//Set direction if in range
				if (abs(angle_difference(enemyDir, bulletDir)) < dirRange && enemyDist < distRange){
					bullet.hsp = lengthdir_x(bulletSpeed, enemyDir);
					bullet.vsp = lengthdir_y(bulletSpeed, enemyDir);
				}
			}
			
			pewShot = true;
			
			//Sound
			audio_play_sound(sndPewGun, 1, 0);
		}
	
		//Gravity
		playerGravity();

		//Collisions
		playerCollisions();
		
		//Apply
		//x += hsp;
		y += vsp;
		
		//End
		if (animation_end()){
			state_set(st.idle);
		}
		
		//Pew type
		if (pewType==st.pew){
			pewType = st.pew2;
		}
		else if (pewType==st.pew2){
			pewType = st.pew;
		}
	break;
	
	#region Chair
	
	//Chair enter
	case st.chairenter:
		if (animation_end()) state_set(st.chair);
	break;
	
	//Chair exit
	case st.chairexit:
		if (animation_end()){
			state_set(st.idle);
			image_speed = 1;
		}
	break;
	
	//Chair
	case st.chair: case st.chairdown: case st.chairup: case st.chairlie:
		//mask_index = sPlayer_Chair;
		
		//Movement
		playerMovement(lerpSpeed/2);
	
		//Gravity
		playerGravity();

		//Jump
		if (global.jump && grounded){
		    vsp = -jumpSpeed;
			
			//Scale
			scale_set(1, 1.2, 30);
			
			//Sound
			audio_play_sound(sndJump, 1, 0);
		}
		
		//Hit ground scale
		if (grounded && !groundedPrev){
			scale_set(1.4, 0.6, 5);
		}

		//Collisions
		enemyInteraction();
		playerCollisions();

		//Apply
		x += hsp;
		y += vsp;
		
		//State Move
		if ((state==st.chair || state==st.chairup) && abs(hsp)){
			state_set(st.chairdown);
		}
		else if (state==st.chairdown && animation_end()){
			state_set(st.chairlie);
		}
		
		//State Stop
		if ((state==st.chairlie || state==st.chairdown) && !abs(hsp)){
			state_set(st.chairup);
			image_index = sprite_get_number(sprite[st.chairup])-2;
			image_speed = -1;
		}
		else if (state==st.chairup && animation_end()){
			state_set(st.chair);
			image_speed = 1;
		}
		
		//Scale
		image_xscale = abs(hsp) ? sign(hsp) : image_xscale;
		
		//Get correct input
		var pressedTime = itemKey(type.chair, 2);
		
		//Leave
		if (pressedTime > 30){
			state_set(st.chairexit);
			image_index = sprite_get_number(sprite[st.chairexit])-0.1;
			image_speed = -1;
		}
		
		chairParticles();
		
		//Attack
		var pressedKey = itemKey(type.chair, 0);
		var pos = itemPos(type.chair);
		var chairHP = global.inv[pos, item.hp];
		var chairUse = global.items[type.chair, pr.use];
		var chairCD = global.inv[pos, item.usecd];
		
		if (pressedKey && (state==st.chairlie || state==st.chairdown || state==st.chairup) && chairHP >= chairUse && !chairCD){
			state_set(st.chairattack);
			
			//Use
			itemUseUp(type.chair);
		}
	break;
	
	//Chair attack
	case st.chairattack:
		//Movement
		playerMovement(lerpSpeed/4);
		playerGravity();
		playerCollisions();
		x+=hsp;
		y+=vsp;
	
		//Add
		chairRot += chairRotSpeed;
		
		//Scale
		scaleX = dcos(chairRot);
		
		//End
		if (chairRot>=360){
			chairRot = 0;
			state_set(st.chairlie);
		}
		
		chairParticles();
		
		//Attack particles
		if (chairRot<240){
			var xxNew = x - ((bbox_right-bbox_left)/2)*scaleX*image_xscale;
			var xxPrev = xprevious - ((bbox_right-bbox_left)/2)*scaleXPrev*image_xscale;
			var yy = y;
			
			for(var xx=min(xxNew, xxPrev); xx<=max(xxNew, xxPrev); xx++){
				fade_particle(xx, yy, sChairSpinPart, 8, 0, scaleX*image_xscale, 0.4, 1);
			}
		}
	break;
	
	#endregion
	
	//Flex Gun
	case st.flexgun_0: case st.flexgun_1:
	case st.flexgun_2: case st.flexgun_m1:
		//Get direction
		var dir8 = ((global.axisDirOn div 45) * 45) mod 360; //8 directional
		var dir4 = dir8; //one sided
		if (dir8>90 && dir8<270) dir4 = (-global.axisDirOn + 180) mod 360;
		
		//Down angle cannot be used
		if ((dir8 mod 360)==270) dir8 += image_xscale*45;
		
		//Set directional state
		if (dir4==0) state = st.flexgun_0;
		else if (dir4==45) state = st.flexgun_1;
		else if (dir4==90) state = st.flexgun_2;
		else state = st.flexgun_m1;
		
		//XScale
		var hx = lengthdir_x(1, global.axisDirOn);
		if (abs(hx)) image_xscale = sign(hx);
		
		//Properties
		var slot = itemPos(type.flexgun);
		//var cd = global.inv[slot, item.usecd];
		var spd = global.items[type.flexgun, pr.spd];
		
		//Shoot
		if (!pewShot){
			//Position offset
			var _x = -0.5;
			if (state==st.flexgun_2) _x += image_xscale * 4;
			
			//Create bullet
			var bullet = instance_create_layer(x + _x, y, "Instances_0", oFlexBall);
				
			bullet.hsp = lengthdir_x(spd, dir8);
			bullet.vsp = lengthdir_y(spd, dir8);
			
			//Cooldown
			global.inv[slot, item.usecd] = global.items[type.flexgun, pr.cooldown];
			
			//Shot
			pewShot = true;
			
			//Sound
			audio_play_sound(sndPewGun, 1, 0);
		}
		
		//Gravity
		playerGravity();

		//Collisions
		playerCollisions();
		
		//Apply
		//x += hsp;
		y += vsp;
		
		//End
		if (animation_end()){
			state_set(st.idle);
		}
	break;
	
	//Item select
	case st.itemselect:
		var slotToReplace = -1;
		if (global.OPressed) slotToReplace = 0;
		else if (global.PPressed) slotToReplace = 1;
		
		//Replace
		if (slotToReplace>=0){
			//Create oItem from already present item
			var item = global.inv[slotToReplace, item.type];
			if (item>=0){
				var itemInst = instance_create_layer(x, y, "Instances_1", oItemPickup);
				itemInst.itemType = item;
			}
			
			//Add
			global.inv[slotToReplace, item.type] = global.itemSelected;
			global.inv[slotToReplace, item.hp] = 1;
			
			//Key scale
			oManager.keyScale[slotToReplace] = global.keySelectScale;
			
			//State
			state_set(st.idle);
		}
	break;
	
	//Dead
	case st.dead:
		//Scale
		scaleXRaw = 0;
		
		//Timeout
		deadTime++;
		if (deadTime>room_speed){
			room_restart();
		}
	break;
}

//State types
if (state==st.pew || state==st.chairattack){
	stateType = stt.attack;
}
else{
	stateType = stt.normal;
}

//Sprite
sprite_index = sprite[state];

//Scaling
scaleX = lerp(scaleX, scaleXRaw, scaleSpeed);
scaleY = lerp(scaleY, scaleYRaw, scaleSpeed);

//Boost
boostX = lerp(boostX, 0, 0.1);
boostY = lerp(boostY, 0, 0.1);

//Cooldowns
refillTimer -= refillTimer > 0;
chairPartCD -= chairPartCD > 0;
hitCooldown -= hitCooldown > 0;
whiteAlpha -= (whiteAlpha>0)/10;

//Hurt
var enemyInst = instance_place(x, y, oEnemies);
var isHurtable = false;

if (!instance_exists(enemyInst)){
	enemyInst = instance_place(x, y, oHurtableParent);
	isHurtable = true;
}

if (enemyInst && !hitCooldown && stateType==stt.normal && !invincible && (isHurtable || enemyInst.hp>0)){
	//Reduce health
	global.playerHP -= enemyInst.attack;
			
	//Sound
	audio_play_sound(sndPoodHit, 1, 0);
	
	//Knockback
	boostX = sign(x-enemyInst.x)*knockbackSpeedX;
	vsp = knockbackSpeedY;
	
	//White
	whiteAlpha = 1;
	
	//Cooldown
	hitCooldown = room_speed/2;
	
	//Lose coins
	var coins = global.collectVar[coll.coin];
	
	var lost = min(coins, enemyInst.coinLoss);
	global.collectVar[coll.coin] -= lost;
	
	//Spawn coins
	repeat(lost){
		var coinInst = instance_create_layer(x, y, "Instances_1", oCoin);
		
		//Get speeds
		coinInst.hsp = random_range(-coinInst.maxHsp, coinInst.maxHsp);
		coinInst.vsp = random_range(-10, -6);
		
		//Set timer
		coinInst.timer = room_speed*3;
	}
	
	//Destroy hurtable
	if (isHurtable){
		instance_destroy(enemyInst);
	}
}

//Die
if (global.playerHP<=0 && state!=st.dead){
	state_set(st.dead);
			
	//Sound
	audio_play_sound(sndDie, 1, 0);
}

//Record grounded position
if (grounded){
	groundX = x;
	groundY = y;
}

//Fall below the room
if (y > room_height + 128){
	x = groundX;
	y = groundY;
	
	global.playerHP -= 20;
}

//Collectibles
getCollectibles();

//Walk sound
soundWalk();