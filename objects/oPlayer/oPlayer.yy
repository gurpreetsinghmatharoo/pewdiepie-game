{
    "id": "0144099e-f68d-4f34-93e6-919a9637e68c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "6cd9cdd1-0766-44d3-bfe5-4b1acfd17410",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0144099e-f68d-4f34-93e6-919a9637e68c"
        },
        {
            "id": "3229b3c0-b8b1-426c-9aaa-e7f3d00a643d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0144099e-f68d-4f34-93e6-919a9637e68c"
        },
        {
            "id": "578754ba-1e97-4b46-9498-bec0ec636c54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0144099e-f68d-4f34-93e6-919a9637e68c"
        },
        {
            "id": "d24556a6-daf5-4d8c-9318-550a347d5ae2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "0144099e-f68d-4f34-93e6-919a9637e68c"
        },
        {
            "id": "44304e2e-f433-4ef8-bd36-746776bc9364",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0144099e-f68d-4f34-93e6-919a9637e68c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "21133b5e-cc44-4641-93a2-2c41b6488b40",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "827bcbb0-eef8-42ce-bb33-74455ecbc986",
    "visible": true
}