/// @description Open chest
if (created < count){
	//Create instance
	var inst = instance_create_layer(x+8, y+8, "Instances_0", object);
	inst.scaleX = 0.5;
	inst.scaleY = 0.5;
	inst.hsp = random_range(-3, 3);
	inst.createdInCol = true;
	
	//Push out
	//with (object){
	//	var yAdd = 0;
	//	while (collision(0, yAdd)){
	//		yAdd++;
	//	}
		
	//	y += yAdd;
	//}
	
	//Enable collectibles' timer
	if (object_get_parent(inst.object_index) == oCollectibleParent){
		inst.timer = room_speed*10;
	}
	
	//Set next alarm
	created++;
	alarm[0] = 20;
}