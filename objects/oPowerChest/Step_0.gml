/// @description 
//States
switch(state){
	//Locked
	case doorSt.locked:
		if (PL_get_active(x, y) && global.ctsCamX==undefined){
			unlockTimer++;
			
			if (unlockTimer > 10){
				state = doorSt.unlocked;
			
				//Set alarm
				alarm[0] = 30;
			}
		}
	break;
	
	//Unlocked
	case doorSt.unlocked:
		//Animate until at last frame
		if (image_index < image_number-1){
			image_speed = 1;
		}
		else{
			image_speed = 0;
			image_index = image_number-1;
		}
	break;
}