{
    "id": "cbb31207-5d2b-4d48-814b-e4fc1e393aa1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPowerChest",
    "eventList": [
        {
            "id": "496f3da7-93c4-4650-a998-866229332283",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cbb31207-5d2b-4d48-814b-e4fc1e393aa1"
        },
        {
            "id": "57fc6ae2-4db3-4e31-84d9-2767fc8b2d78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cbb31207-5d2b-4d48-814b-e4fc1e393aa1"
        },
        {
            "id": "34337c7d-97d2-42dd-a857-fda003c5e16d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cbb31207-5d2b-4d48-814b-e4fc1e393aa1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7b24afab-ef8d-4e33-8535-851c001e52dd",
    "visible": true
}