/// @description 
//Vars
camX = camera_get_view_x(view_camera);
camY = camera_get_view_y(view_camera);
camW = camera_get_view_width(view_camera);
camH = camera_get_view_height(view_camera);
marg = PL_CAM_MARGIN;

//Run
if (CD==0){
	//Loop and empty
	if (!global.clearedThisStep){
		for(var xx = 0; xx <= room_width; xx += PL_SIZE){
			for(var yy = 0; yy <= room_height; yy += PL_SIZE){
				var act = PL_get_active(xx, yy);
				if (act){
					PL_set_active(xx, yy, false);
				}
			}
		}
		
		global.clearedThisStep = true;
	}
	
	//Spread
	PL_spread(x, y);
	
	//Set cooldown
	//CD = Repeat;
	CD = -1;
}
else{
	//CD -= CD > 0;
}

//Spread
