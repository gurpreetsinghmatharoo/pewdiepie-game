{
    "id": "2ea30f6c-648e-4f86-9022-d0ab29f28a0f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPowerSource",
    "eventList": [
        {
            "id": "8574360d-1661-4880-8fa5-51e9f32401fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ea30f6c-648e-4f86-9022-d0ab29f28a0f"
        },
        {
            "id": "3273d1f1-9722-4555-a8ea-40240c2f2da4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2ea30f6c-648e-4f86-9022-d0ab29f28a0f"
        },
        {
            "id": "ede10055-68c5-418e-8e3b-0dc480e01d12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "2ea30f6c-648e-4f86-9022-d0ab29f28a0f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "481c5ad6-4f2c-4c6f-aef6-b564823fe581",
    "visible": true
}