{
    "id": "a8628f7e-3f6a-4bb1-aeb2-7ab4b9a24ca8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPurpleGate",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "d93d1c17-8081-4cc0-9a81-00cb90ded859",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "dd046f0c-63c4-4461-843e-c46894593510",
            "propertyId": "f3546706-1d60-421c-aec0-d1b5652f4f78",
            "value": "90"
        }
    ],
    "parentObjectId": "dd046f0c-63c4-4461-843e-c46894593510",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d7c9ea15-e620-4cab-848c-e3bdb9d08ff3",
    "visible": true
}