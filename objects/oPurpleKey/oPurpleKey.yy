{
    "id": "c48b17f0-9406-4538-85a5-b725728baeff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPurpleKey",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "b0f1ae4f-42c7-4300-8d0b-7479c356b19c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "c5abf91f-4c5b-4945-a820-a1a125231383",
            "propertyId": "d26a146e-c453-48e6-924d-7576c363d1c6",
            "value": "oPurpleGate"
        }
    ],
    "parentObjectId": "c5abf91f-4c5b-4945-a820-a1a125231383",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "be5be2fd-ff51-431d-97ff-40e3a007a3e7",
    "visible": true
}