{
    "id": "94e1431c-16ce-4e98-9fe5-9a284f64a9fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPushableParent",
    "eventList": [
        {
            "id": "b35a2411-68fc-4414-ace8-a66d8c288f4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "94e1431c-16ce-4e98-9fe5-9a284f64a9fd"
        },
        {
            "id": "1919ec0e-9ba3-430c-98e0-be55460ff8dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "94e1431c-16ce-4e98-9fe5-9a284f64a9fd"
        },
        {
            "id": "c6618db3-768d-4916-84d8-f29553a75013",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "94e1431c-16ce-4e98-9fe5-9a284f64a9fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "eaebb985-ad2f-4530-a1fb-c1c3b11e2748",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}