/// @description 
//Camera
var cX = camera_get_view_x(view_camera);
var cY = camera_get_view_y(view_camera);

//Surface
surface_set_target(shadowSurf);
draw_clear_alpha(0, 0)

//Draw shadows
with (oCastShadow){
	draw_sprite_ext(sprite_index, image_index, ((x + global.shadowDistX) - cX) * CAM.SCALE, ((y + global.shadowDistY) - cY) * CAM.SCALE, image_xscale * CAM.SCALE, image_yscale * CAM.SCALE, 0, -1, 1);
}

surface_reset_target();

//Draw surface
draw_surface_ext(shadowSurf, cX, cY,
	1/CAM.SCALE, 1/CAM.SCALE, 0, 1, 0.4);