{
    "id": "2d288ccf-c54e-4e70-9aa5-deb52fbf6037",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oShadows",
    "eventList": [
        {
            "id": "14a2f0d2-2630-4d02-a39f-0af83239d1e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2d288ccf-c54e-4e70-9aa5-deb52fbf6037"
        },
        {
            "id": "0d78797f-7f30-4692-93a4-f29bd8e63981",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 8,
            "m_owner": "2d288ccf-c54e-4e70-9aa5-deb52fbf6037"
        },
        {
            "id": "d704d37f-311b-40d5-8944-51db8c88d735",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2d288ccf-c54e-4e70-9aa5-deb52fbf6037"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}