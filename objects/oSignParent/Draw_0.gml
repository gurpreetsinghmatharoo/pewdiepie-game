/// @description 
//Self
draw_self();

//Textbox scale
if (playerCol){
	tbScaleY = lerp(tbScaleY, 1, 0.1);
}
else{
	tbScaleY = lerp(tbScaleY, 0, 0.2);
}

//Draw textbox
if (tbScaleY > 0.05){
	msg_dynamic(msg, global.textButton, spr_msg_grid, spr_msg_arrow, tbScaleY);
}
else{
	msg_stop();
}