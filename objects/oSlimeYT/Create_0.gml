/// @description 
//Parent
event_inherited();

//State
state = st.idle;

//Sprites
sprite[st.idle] = sSlimeYT_Idle;
sprite[st.move] = sSlimeYT_Move;
sprite[st.dead] = sSlimeYT_Idle;

mask_index = sprite[st.idle];

//Props
moveSpeed = 2;
jumpSpeed = 6;
gravSpeed = 1;
gravInterval = 2;

//Vars
hp = 5;
attack = 8;
coinLoss = 10;