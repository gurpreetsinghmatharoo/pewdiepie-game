/// @description 
//Set player speeds
if (image_alpha==0.5){
	if (spawnHsp != 0){
		oPlayer.hsp = spawnHsp;
		oPlayer.follower.hsp = spawnHsp;
		spawnHsp = 0;
	}
	if (spawnVsp != 0){
		oPlayer.vsp = spawnVsp;
		oPlayer.follower.vsp = spawnVsp;
		spawnVsp = 0;
	}
	
	image_alpha = 1;
}