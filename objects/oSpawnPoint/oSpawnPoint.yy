{
    "id": "c03c408d-b414-4dce-9799-b8a6e91b726b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpawnPoint",
    "eventList": [
        {
            "id": "fca07917-6b47-4ecf-a699-b54cc0fdeee6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c03c408d-b414-4dce-9799-b8a6e91b726b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "59b70524-0abd-490b-a2cc-3e856f6174e3",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": true,
            "rangeMax": 10,
            "rangeMin": -10,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "spawnHsp",
            "varType": 0
        },
        {
            "id": "9cb505b3-0fe8-47d9-b63f-d73b50c2406b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": true,
            "rangeMax": 10,
            "rangeMin": -10,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "spawnVsp",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "cc31db95-e21e-4eaf-88c1-31670f164a40",
    "visible": false
}