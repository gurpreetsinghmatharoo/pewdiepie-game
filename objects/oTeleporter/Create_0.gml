/// @description 
//Can it be entered?
enterable = false;

//If it's a door, set the sprite and mask and make it visible
if (isDoor){
	var spr = sDoor;
	
	//Big door
	if (image_xscale==2) spr = sDoorBig;
	
	//Set sprite and mask
	sprite_index = spr;
	mask_index = spr;
	
	//Set door frame
	if (global.roomPlace[room]==rmType.interior) image_index = 1;
	
	//Make visible
	visible = true;
}