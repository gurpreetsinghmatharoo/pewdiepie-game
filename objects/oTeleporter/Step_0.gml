/// @description 
//Player collision
var playerCol = place_meeting(x, y, oPlayer);

//Enable entering
if (!playerCol && !enterable){
	enterable = true;
}

//Enter door
if (playerCol && (!isDoor || global.up) && enterable){
	//Ini info save
	ini_open(global.FNLevelChange);
	
	//Main
	ini_write_real("main", "flagCreatePlayer", 1);
	//ini_write_real("main", "xOff", oPlayer.x-x);
	//ini_write_real("main", "yOff", oPlayer.y-y);
	
	//Target
	ini_write_real("target", "room", targetRoom);
	ini_write_real("target", "inst", targetInst);
	
	//Player
	ini_write_real("player", "hsp", oPlayer.hsp);
	ini_write_real("player", "vsp", oPlayer.vsp);
	ini_write_real("player", "boostX", oPlayer.boostX);
	ini_write_real("player", "boostY", oPlayer.boostY);
	ini_write_real("player", "state", oPlayer.state);
	ini_write_real("player", "hp", global.playerHP);
	
	//Follower
	ini_write_real("follower", "obj", oPlayer.follower.object_index);
	ini_write_real("follower", "hp", oPlayer.follower.hp);
	
	//Inventory
	for(var i=0; i<global.maxInv; i++){
		for(var j=0; j<array_length_2d(global.inv, i); j++){
			ini_write_real("inv", string(i)+string(j), global.inv[i, j]);
		}
	}
	
	ini_close();
	
	//Surface
	global.fadeSurf = surface_create(surface_get_width(application_surface),
									surface_get_height(application_surface));
									
	surface_copy(global.fadeSurf, 0, 0, application_surface);
	
	//Set alarm and deactivate
	oManager.alarm[1] = FADETIME;
	global.Alarm1Room = targetRoom;
	global.fadeState = fdSt.finish;
	instance_deactivate_all(0);
	instance_activate_object(oManager);
	
	//Sound
	audio_play_sound(sndPoodHit, 1, 0);
}