{
    "id": "4598317f-e25b-4d1e-b470-607d9b73e100",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTeleporter",
    "eventList": [
        {
            "id": "d205d815-c7c5-4105-be06-6a66d968f32a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4598317f-e25b-4d1e-b470-607d9b73e100"
        },
        {
            "id": "4c3d23be-9e8f-44f1-a292-3ce157470ba2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4598317f-e25b-4d1e-b470-607d9b73e100"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "86ccbe31-6a4c-48d5-9126-9f9276d191be",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "targetInst",
            "varType": 4
        },
        {
            "id": "c86fd738-685b-49de-8fa0-34029f53eb6b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "targetRoom",
            "varType": 5
        },
        {
            "id": "9c6e9be8-aa3e-47d7-a3a2-79533d3cd6f7",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "isDoor",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "77318fa1-e283-46bd-811b-e65fa3d4fc25",
    "visible": false
}