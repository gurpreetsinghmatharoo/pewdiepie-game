/// @description 
event_inherited();

//Check for power
var angle = image_angle + 90;
var dist = PL_SIZE/2;

var xx = x + lengthdir_x(dist, angle);
var yy = y + lengthdir_y(dist, angle);
var act = PL_get_active(xx, yy);

if (act && state==doorSt.locked){
	state = doorSt.unlocked;
	
	//Camera Focus
	if (global.roomTime > room_speed){
		global.ctsCamX = (x+lengthdir_x(sprite_height/2, image_angle-90)) - camera_get_view_width(view_camera)/2;
		global.ctsCamY = (y+lengthdir_y(sprite_height/2, image_angle-90)) - camera_get_view_height(view_camera)/2;
	
		global.ctsCamX = clamp(global.ctsCamX, 0, room_width - camera_get_view_width(view_camera));
		global.ctsCamY = clamp(global.ctsCamY, 0, room_height - camera_get_view_height(view_camera));
		
		//Reset
		alarm[0] = room_speed*2;
	}
}
else if (!act && state==doorSt.unlocked){
	state = doorSt.locked;
}