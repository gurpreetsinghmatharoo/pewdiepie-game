{
    "id": "4335443d-b91b-4309-b509-5d1a3c5599e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oYellowGate",
    "eventList": [
        {
            "id": "06557049-f9b7-4f80-9632-57928d214253",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4335443d-b91b-4309-b509-5d1a3c5599e2"
        },
        {
            "id": "d89a8d5f-ea8f-461f-b7c9-319bb16760f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4335443d-b91b-4309-b509-5d1a3c5599e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "78349e84-bd45-4a53-8fa7-9e8c93bef39d",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "dd046f0c-63c4-4461-843e-c46894593510",
            "propertyId": "f3546706-1d60-421c-aec0-d1b5652f4f78",
            "value": "90"
        }
    ],
    "parentObjectId": "dd046f0c-63c4-4461-843e-c46894593510",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "78092670-6569-43c2-a1fa-40086801d85f",
    "visible": true
}