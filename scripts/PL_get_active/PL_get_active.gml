/// @desc Returns whether a power line tile is active or not
/// @arg x
/// @arg y

//Args
var _x = argument[0];
var _y = argument[1];

//Check
var _td = tilemap_get_at_pixel(global.Tiles_Lines, _x, _y);

//Empty
if (_td<=0){
	return -1;
}

//Return active
return _td > PL_DIV// || oFlexGrid.grid[# _x div PL_SIZE, _y div PL_SIZE];