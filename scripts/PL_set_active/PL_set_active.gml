/// @desc Turns a power line on or off
/// @arg x
/// @arg y
/// @arg bool

//Args
var _x = argument[0];
var _y = argument[1];
var _bool = argument[2];

//Check
var _td = tilemap_get_at_pixel(global.Tiles_Lines, _x, _y);

//Enable
if (_td >= PL_DIV && !_bool){
	_td -= PL_DIFF;
}
//Disable
else if (_td < PL_DIV && _bool){
	_td += PL_DIFF;
}

//Set
tilemap_set_at_pixel(global.Tiles_Lines, _td, _x, _y);