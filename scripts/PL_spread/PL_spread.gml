/// @arg x
/// @arg y

//Args
var _x = argument[0];
var _y = argument[1];

//Function
var act;

//Right
var xx = _x + PL_SIZE;
var yy = _y;
act = PL_get_active(xx, yy); //Get active

if (!act && act>=0){ //If it is not active
	PL_set_active(xx, yy, true); //Activate it
	PL_spread(xx, yy); //Spread it from there too
}

//Left
var xx = _x - PL_SIZE;
var yy = _y;
act = PL_get_active(xx, yy); //Get active

if (!act && act>=0){ //If it is not active
	PL_set_active(xx, yy, true); //Activate it
	PL_spread(xx, yy); //Spread it from there too
}

//Up
var xx = _x;
var yy = _y - PL_SIZE;
act = PL_get_active(xx, yy); //Get active

if (!act && act>=0){ //If it is not active
	PL_set_active(xx, yy, true); //Activate it
	PL_spread(xx, yy); //Spread it from there too
}

//Down
var xx = _x;
var yy = _y + PL_SIZE;
act = PL_get_active(xx, yy); //Get active

if (!act && act>=0){ //If it is not active
	PL_set_active(xx, yy, true); //Activate it
	PL_spread(xx, yy); //Spread it from there too
}