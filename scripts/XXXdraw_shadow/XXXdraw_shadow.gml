/// @arg sprite
/// @arg subimg
/// @arg x
/// @arg y
/// @arg xscale
/// @arg yscale
/// @arg angle

if (instance_exists(oShadows)){
	surface_set_target(oShadows.shadowSurf);
	draw_sprite_ext(argument0, argument1, argument2 - camera_get_view_x(view_camera), argument3 - camera_get_view_y(view_camera), argument4, argument5, argument6, -1, 1);
	surface_reset_target();
}