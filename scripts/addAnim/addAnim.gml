/// @arg actor
/// @arg atype
/// @arg [data]

//Args
var _actor = argument[0];
var _atype = argument[1];
var _data = argument[2];

//Vars
var _cts = animCurrent;

//Create array
var _temparr;
_temparr[adata.actor] = _actor;
_temparr[adata.atype] = _atype;
_temparr[adata.data] = _data;

//Add to queue array
var queueSize = array_length_2d(global.ctsAnim, _cts);
global.ctsAnim[_cts, queueSize] = _temparr;