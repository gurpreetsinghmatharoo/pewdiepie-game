/// @function collision
/// @arg xAdd
/// @arg yAdd
/// @arg <oneway(true)>

//Args
var _xAdd = argument[0];
var _yAdd = argument[1];
var _oneway = argument_count > 2 ? argument[2] : true;

//Object Collision
var objCol = instance_place(x + _xAdd, y + _yAdd, oCollision);

//Check for NPCs too for specific objects
if (object_get_parent(object_index) == oPushableParent){
	objCol = objCol || place_meeting(x + _xAdd, y + _yAdd, oPushableParent)
					|| place_meeting(x + _xAdd, y + _yAdd, oFollowerParent)
					|| place_meeting(x + _xAdd, y + _yAdd, oEnemies);
}

//Tile collision
var x1 = bbox_left+_xAdd;
var x2 = bbox_right+_xAdd;
var y1 = bbox_top+_yAdd;
var y2 = bbox_bottom+_yAdd;

if (_xAdd>0){
	x1 = ceil(x1);
	x2 = ceil(x2);
}
if (_yAdd>0){
	y = ceil(y);
	
	y1 = ceil(y1);
	y2 = ceil(y2);
}

var tileCol = tilemap_collision(global.Tiles_Main, x1, y1) or 
			  tilemap_collision(global.Tiles_Main, x2, y1) or 
			  tilemap_collision(global.Tiles_Main, x2, y2) or 
			  tilemap_collision(global.Tiles_Main, x1, y2) or
			  tilemap_collision(global.Tiles_Main, x+_xAdd, y1) or
			  tilemap_collision(global.Tiles_Main, x+_xAdd, y2) or
			  tilemap_collision(global.Tiles_Main, x1, y+_yAdd) or
			  tilemap_collision(global.Tiles_Main, x2, y+_yAdd);

//One way collision
var onewayCol = false;

if (_yAdd > 0 && _oneway){
	for(var _y = y2 - _yAdd; _y <= y2; _y++){
		onewayCol = tilemap_collision(global.Tiles_Oneway, x2, _y) or 
					tilemap_collision(global.Tiles_Oneway, x1, _y);
		
		if (onewayCol) break;
	}
	
	//Make sure that the player's y2 is at top of a tile
	onewayCol = onewayCol && (_y mod global.Tiles_Oneway_CellSize)<=1;
}

//Out of bounds
var outOfBounds = x + _xAdd < 0 || x + _xAdd > room_width;

//Return
if (objCol) 
return objCol;
else
return tileCol || outOfBounds || onewayCol;