/// @arg x
/// @arg y
/// @arg type
/// @arg [options]

//Arguments
var _x = argument[0];
var _y = argument[1];
var _type = argument[2];
var _opt = argument[3];

//Create
var _ctx = instance_create_layer(_x, _y, "Instances_m1", oContextMenu);
_ctx.type = _type;
_ctx.options = _opt;
_ctx.optionCount = array_length_1d(_opt);
_ctx.height = _ctx.listH * _ctx.optionCount;

return _ctx;