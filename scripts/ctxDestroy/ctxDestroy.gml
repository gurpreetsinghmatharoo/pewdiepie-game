/// Goes through the parents to destroy them
if (instance_exists(parent)){
	with (parent) ctxDestroy();
}
	
instance_destroy();