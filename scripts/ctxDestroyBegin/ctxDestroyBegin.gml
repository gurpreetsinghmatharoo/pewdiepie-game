/// Goes through the children to find the last
if (instance_exists(child)){
	with (child) ctxDestroyBegin();
}
else ctxDestroy();