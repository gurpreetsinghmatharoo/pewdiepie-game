/// @arg cts
/// @arg progress
/// @arg room

//Arguments
var _cts = argument[0];
var _progress = argument[1];
var _room = argument[2];

//Function
if (global.ctsPos < 0 && room==_room && progressGet(room)==_progress){
	cutsceneStart(_cts);
}