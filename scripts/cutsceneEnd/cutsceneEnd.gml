
//Vars
var _cts = global.ctsType;

//Loop through actors
for(var i=0; i<array_length_2d(global.ctsActors, _cts); i++){
	//Get actor
	var _actor = global.ctsActors[_cts, i];
	
	//Actor pos
	//var xx = 0;
	//var yy = 0;
	
	//Get entity
	var _entity = getEntity(_actor);
	
	if (object_exists(_entity) && instance_exists(_actor)){
		//Activate entity
		instance_activate_object(_entity);
		
		//Activated? If not, create if
		if (!instance_exists(_entity)){
			instance_create_layer(0, 0, "Instances_0", _entity);
		}
		
		//If entity exists
		if (instance_exists(_entity)){
			//Pass properties
			_entity.x = _actor.x;
			_entity.y = _actor.y;
			_entity.state = _actor.state;
			_entity.image_xscale = _actor.image_xscale;
		}
	}
	
	//Destroy actor
	if (instance_exists(_actor)) instance_destroy(_actor);
}

//Cutscene variables
global.ctsPos = -1;
global.ctsCamX = undefined;
global.ctsCamY = undefined;

//Create follower
if (instance_exists(oPlayer) && !instance_exists(oPlayer.follower)){
	instance_create_layer(oPlayer.x, oPlayer.y, "Instances_0", oPlayer.follower);
}