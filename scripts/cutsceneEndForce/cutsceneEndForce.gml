/// @desc Force ends a cutscene, for debugging purposes only
//Add player
if (!instance_exists(oActorPoods)){
	var camX = camera_get_view_x(view_camera);
	var camY = camera_get_view_y(view_camera);
			
	animStart(global.ctsType);
	addAnim(noone, atype.addActor, [camX + CAM.WIDTH/2, camY + CAM.HEIGHT/2, "Instances_0", oActorPoods]);
	animEnd();
}
		
//End cutscene
global.ctsPos = array_length_2d(global.ctsAnim, global.ctsType)-2;