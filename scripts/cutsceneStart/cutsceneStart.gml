/// @arg cts

//Arguments
var _cts = argument[0];

//Loop through actors
for(var i=0; i<array_length_2d(global.ctsActors, _cts); i++){
	//Get actor
	var _actor = global.ctsActors[_cts, i];
	
	//Actor pos
	var xx = 0;
	var yy = 0;
	
	//Get entity
	var _entity = getEntity(_actor);
	
	if (instance_exists(_entity)){
		//Get its position
		var xx = _entity.x;
		var yy = _entity.y;
		
		//Deactivate it
		instance_deactivate_object(_entity);
	}
	
	//Create actor
	instance_create_layer(xx, yy, "Instances_0", _actor);
}

//Cutscene variables
global.ctsType = _cts;
global.ctsPos = 0;
global.ctsWait = 0;