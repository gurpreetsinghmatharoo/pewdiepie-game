/// @arg x
/// @arg y
/// @arg value
/// @arg name
/// @arg [...]

//Args
var _x = argument[0];
var _y = argument[1];

//Function
draw_set_halign(fa_center);
draw_set_valign(fa_middle);

var tHeight = 0;

for(var i=argument_count-2; i>=2; i-=2){
	var text = argument[i];
	var name = argument[i+1];
	
	var str = name + ": " + string(text);
	
	tHeight += string_height(str);
	
	draw_text(_x, _y - tHeight, str);
}

draw_set_halign(fa_left);
draw_set_valign(fa_top);