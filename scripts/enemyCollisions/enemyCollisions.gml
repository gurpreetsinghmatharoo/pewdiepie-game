//Move out if stuck
collisionSafe();

//Collisions
if (collision(hsp, 0)){
	while(!collision(sign(hsp), 0)){
		x += sign(hsp);	
	}

	hsp = 0;
	moveDir *= -1;
}

if (collision(0, vsp+flap)){
	while(!collision(0, sign(vsp+flap))){
		y += sign(vsp+flap);
	}
	
	vsp = 0;
	flap = 0;
}

if (collision(hsp, vsp+flap)){
	if (abs(hsp)){
		hsp = 0;
		moveDir *= -1;
	}
	else{
		vsp = 0;
		flap = 0;
	}
}

//Ledge
if (!collision(hsp*4, 0) && !collision(hsp*4, 16) && object_index != oFlyingEnemy){
	hsp = 0;
	moveDir *= -1;
}