/// @function enemyHurt

//Projectiles
var pewCol = instance_place(x, y, oProjectileParent);
if (!pewCol) pewCol = mask_rect_collision(0, 0, oPushableParent, 1, 1); //Pushable thrown on top

if (pewCol) return pewCol;

//Flex Ball
var flexCol = noone;

with (oFlexBall){
	if (collision_circle(x, y, radius, other, false, false)){
		flexCol = id;
	}
}

if (flexCol) return flexCol;

//Chair
if (instance_exists(oPlayer) && oPlayer.state==st.chairattack){
	var colX = oPlayer.x + (oPlayer.image_xscale * oPlayer.scaleX * 8);
	var colY = oPlayer.y;
	
	var col = collision_rectangle(colX-8, colY-4, colX+8, colY+4, id, 0, 0);
	
	if (col){
		return oPlayer;
	}
}

//Noone
return noone;