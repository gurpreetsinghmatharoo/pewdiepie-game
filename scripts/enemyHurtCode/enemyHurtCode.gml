var hurtInst = enemyHurt();

if (hurtInst){
	//Get info
	var hurtType = enemyHurtType(hurtInst);
	var hurtAmount;
	if (hurtType>=0) hurtAmount = global.items[hurtType, pr.attack];
	else hurtAmount = abs(hurtType);
			
	hp -= hurtAmount;
	whiteAlpha = 1;
			
	//Boost
	var dir = point_direction(hurtInst.x, hurtInst.y, x, y);
	boostX = lengthdir_x(boostSpeed, dir);
	if (!canFly) vsp = lengthdir_y(boostSpeed, dir);
	else boostY = lengthdir_y(boostSpeed, dir);
			
	//Delete
	if (!in(hurtInst.object_index, oPlayer) && !in(object_get_parent(hurtInst.object_index), oPushableParent)){
		instance_destroy(hurtInst);
	}
			
	//Sound
	audio_play_sound(sndEnemyHurt, 1, 0);
}