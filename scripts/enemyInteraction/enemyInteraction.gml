//Enemy ver
var enemyCol = instance_place(x, y+vsp, oEnemies);

if (enemyCol && !grounded && vsp>0){
	//Move
	while(!place_meeting(x, y+1, enemyCol)){
		y++;
	}
	y+=4;
	
	//Jump
	vsp = -jumpSpeed;
	scale_set(1, 1.2, 30);
	
	with (enemyCol){
		scale_set(1.4, 0.6, 10);
	}
	
	//Invincible
	invincible = true;
}