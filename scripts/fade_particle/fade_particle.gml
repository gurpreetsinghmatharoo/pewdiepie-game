/// @arg x
/// @arg y
/// @arg sprite
/// @arg life
/// @arg life_range
/// @arg xscale
/// @arg yscale
/// @arg back

//Args
var _x = argument[0];
var _y = argument[1];
var _sprite = argument[2];
var _life = argument[3];
var _lifeRange = argument[4];
var _xscale = argument[5];
var _yscale = argument[6];
var _back = argument[7];

//Function
part_type_sprite(global.fadePart, _sprite, 0, 0, 0);
part_type_life(global.fadePart, _life - _lifeRange/2, _life + _lifeRange/2);
part_type_scale(global.fadePart, _xscale, _yscale);
part_particles_create(_back ? global.partSysBack : global.partSys, _x, _y, global.fadePart, 1);