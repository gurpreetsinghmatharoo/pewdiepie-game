gml_pragma("global", "gameInit()");

#region Physics
global.pushSpeed = 1;
global.slopeHeight = 8;
#endregion

#region Input
//Input variables that required an initial value
global.axisDirOn = 0;
#endregion

#region Saving
//Files names
global.FNLevelChange = "levelchangetemp";
global.FNTempSave = "tempsave";
#endregion

#region Vars
//Game info
global.GUIEnabled = true;
global.teleportersEnabled = true;

//Input
global.horHoldTime = 0;
global.downHoldTime = 0;
global.OPressed = 0;
global.PPressed = 0;

//Collision
//global.onewayCollision = true;

//Power
global.clearedThisStep = false;
#endregion

#region Graphics
//Level end fade
#macro FADETIME 30

//Camera
enum CAM{
	WIDTH = 320,
	HEIGHT = 180,
	SCALE = 4
}

#macro CAMLERP 0.05

global.camLerp = CAMLERP;

//Shadow
global.shadowDistX = 2;
global.shadowDistY = 3;
#endregion

#region Particles
//Particles
global.partSys = part_system_create(); //Main particles
global.partSysBack = part_system_create(); //Particles at the back
global.partInvisible = part_system_create(); //Invisible particles
part_system_depth(global.partSysBack, 350);

//White poof
global.whitePoof = part_type_create();
part_type_life(global.whitePoof, 40, 50);
part_type_sprite(global.whitePoof, sWhitePoof, 0, 0, 0);
part_type_speed(global.whitePoof, 0.1, 0.4, 0, 0);
part_type_direction(global.whitePoof, 90, 90, 0, 0);
part_type_size(global.whitePoof, 0, 0.1, 0.03, 0);
part_type_alpha3(global.whitePoof, 1, 1, 0);

//Ded poof
global.poofPart = part_type_create();
part_type_life(global.poofPart, 40, 50);
part_type_speed(global.poofPart, 0.1, 0.2, 0, 0);
part_type_direction(global.poofPart, 0, 360, 0, 0);
part_type_size(global.poofPart, 0, 0.1, 0.03, 0);
part_type_alpha3(global.poofPart, 1, 1, 0);

//Fade particle
global.fadePart = part_type_create();
part_type_alpha2(global.fadePart, 1, 0);

//Generic particle type
global.genPart = part_type_create();
#endregion

#region Constants
//Functions
#macro log show_debug_message

//Power
#macro PL_DIV 80
#macro PL_DIFF 112
#macro PL_SIZE 16
#macro PL_CAM_MARGIN 64

//Item types
enum type{
	pew,
	chair,
	flexgun
}

//Item data (constant)
enum pr{
	sprite, //Sprite
	name, //Name
	attack, //Attack, optional
	spd, //Speed, optional
	use, //How much to use up once
	refill, //Amount to refill with
	cooldown, //Usage cooldown
	distrange, //Distance range
	dirrange //Direction range
}

//Item stats (variable)
enum item{
	type, //Item type
	hp, //Item HP
	usecd, //Usage countdown
	refillcd //Refilling countdown
}

//States
enum st{
	//Basic
	idle,
	move,
	jump,
	walltoright,
	walltoleft,
	hurt, //Currently only for actors
	dead,
	push,
	
	//Edgar only
	pickup,
	
	//Pew
	pew,
	pew2,
	
	//Chair
	chair,
	chairenter,
	chairexit,
	chairdown,
	chairup,
	chairlie,
	chairattack,
	
	//Flex Gun
	flexgun_0,
	flexgun_1,
	flexgun_2,
	flexgun_m1,
	
	//Misc
	itemselect,
	alert,
	pickMarzia,
	walkMarzia,
	
	//Enemy/NPC specific
	flap
}

//State types
enum stt{
	normal,
	attack
}

//Key states
enum keySt{
	idle,
	picked,
	used
}

//Door states
enum doorSt{
	locked,
	unlocked
}

//Room types
enum rmType{
	main,
	underground,
	
	interior,
	exterior
}

//Fade states
enum fdSt{
	none,
	start,
	finish
}
#endregion

#region Context menu
//Context menu types
enum ctxT{
	root,
	objs,
	rooms
}

//Context menu options
enum ctx{
	addInst,
	removeInst,
	changeRoom,
	ctsEnd,
	stopCam,
	close
}

#macro CTX_OPTIONS 6

//Context menu names
global.ctxName[ctx.addInst] = "Add Instance";
global.ctxName[ctx.removeInst] = "Remove Instance";
global.ctxName[ctx.changeRoom] = "Change Room";
global.ctxName[ctx.ctsEnd] = "End Cutscene";
global.ctxName[ctx.stopCam] = "Stop Camera";
global.ctxName[ctx.close] = "Close";
#endregion

#region Collectibles
//Collectible types
enum coll{
	coin
}

//Collectible properties
enum cp{
	sprite
}

//Collectible data (constant)
global.collectData[coll.coin, cp.sprite] = sCoin;

//Collectible amount (variable)
global.collectVar[coll.coin] = 0;
#endregion

#region Global Data Structures
//Room types
global.roomTypes[rmHomeInside] = rmType.main;
global.roomTypes[rmHome] = rmType.main;
global.roomTypes[rmUnderground1] = rmType.underground;
global.roomTypes[rmCity] = rmType.main;
{
	global.roomTypes[rmCityHouse0] = rmType.main;
}
global.roomTypes[rmW1_1] = rmType.main;
global.roomTypes[rmW1_2] = rmType.main;

//Interior or Exterior
global.roomPlace[rmHomeInside] = rmType.interior;
global.roomPlace[rmHome] = rmType.exterior;
global.roomPlace[rmUnderground1] = rmType.exterior;
global.roomPlace[rmCity] = rmType.exterior;
{
	global.roomPlace[rmCityHouse0] = rmType.interior;
}
global.roomPlace[rmW1_1] = rmType.exterior;
global.roomPlace[rmW1_2] = rmType.exterior;

//Room backgrounds
global.roomBack[rmType.main, 0] = sBackHills;
global.roomBack[rmType.main, 1] = sBackCloudFront;
global.roomBack[rmType.main, 2] = sBackCloudBack;
global.roomBack[rmType.main, 3] = sBackSky;

global.roomBack[rmType.underground, 0] = sBackUnderground0;
global.roomBack[rmType.underground, 1] = sBackUnderground1;
global.roomBack[rmType.underground, 2] = sBackUnderground2;
global.roomBack[rmType.underground, 3] = sBackUnderground3;

//Items
global.items[type.pew, pr.sprite] = sItemPew;
global.items[type.pew, pr.name] = "Pew Guns";
global.items[type.pew, pr.attack] = 1;
global.items[type.pew, pr.spd] = 5;
global.items[type.pew, pr.use] = 0.1;
global.items[type.pew, pr.refill] = 0.01;
global.items[type.pew, pr.cooldown] = 0;
global.items[type.pew, pr.distrange] = 160;
global.items[type.pew, pr.dirrange] = 45;

global.items[type.chair, pr.sprite] = sItemChair;
global.items[type.chair, pr.name] = "Costly Chair";
global.items[type.chair, pr.attack] = 1;
global.items[type.chair, pr.use] = 0.1;
global.items[type.chair, pr.refill] = 0.01;
global.items[type.chair, pr.cooldown] = 0;

global.items[type.flexgun, pr.sprite] = sItemFlexGun;
global.items[type.flexgun, pr.name] = "Flex Gun";
global.items[type.flexgun, pr.attack] = 1;
global.items[type.flexgun, pr.spd] = 5;
global.items[type.flexgun, pr.use] = 0.2;
global.items[type.flexgun, pr.refill] = 0.01;
global.items[type.flexgun, pr.cooldown] = 30;
#endregion

#region Cutscenes
global.ctsEnabled = true;

//Cutscenes
enum cts{
	test,
	
	opening,
	outside0,
	demoEnd,
	
	martiplier0
}

//Action types
enum atype{
	//Actions
	move, //Move relatively
	say, //Say a message
	spd, //Set speeds
	face, //Face in a direction
	faceForced, //Force facing in a direction regardless of the movement direction
	
	//Anti-actions
	sayStop, //Stop talking
	
	//Asynchronous actions
	moveAsync, //Move asynchronously
	sayAsync, //Talk asynchronously
	
	//System actions
	wait,
	globalProgressAdd, //Add to the global progress
	roomProgressAdd, //Add to the room's progress
	addActor, //Add a new actor
	
	//Camera
	camPos, //Set camera position (lerp)
	camPosRaw, //Set camera position (instant)
	camShake, //Camera shake
	camStop, //Stop camera there
	
	//Instances
	addInst, //Add a new instance
	destInst, //Destroy an instance
	setState, //Set actor's state
	setMoveSpeed, //Set movement speed
	setImageSpeed //Set image speed
}

//Action data types
enum adata{
	atype,
	actor,
	data
}

//Arrays
global.ctsActors[0, 0] = 0; //Actors
//global.ctsAnim[0, 0] = 0; //Animations/Actions
global.ctsAnim = -1;

//Variables
global.ctsType = -1; //The type of cutscene being played
global.ctsPos = -1; //Current cutscene's progress. -1 means it isn't playing.
global.ctsPosPrev = -1; //Previous pos

//Actor/entity relations
global.entity[oActorPoods] = oPlayer;
global.entity[oActorEdgar] = oEdgar;
global.entity[oActorMarzia] = MarziaPH0;
global.entity[oActorBrad1] = oNPCBrad1;
global.entity[oActorGurpreet] = oNPCGurpreet;

//Build animations
animCurrent = -1;

#region Test
animStart(cts.test);
addActors(oActorPoods, oActorEdgar);
addAnim(noone, atype.wait, [60]);
animEnd();
//addAnim(cts.test, oActorPoods, atype.move, [64, 0]);
//addAnim(cts.test, oActorPoods, atype.say, ["Hey Edgar..."]);
//addAnim(cts.test, oActorEdgar, atype.face, [-1]);
//addAnim(cts.test, oActorEdgar, atype.say, ["Yes, poods?"]);
//addAnim(cts.test, oActorPoods, atype.say, ["You suck."]);
//addAnim(cts.test, oActorEdgar, atype.move, [42, 0]);
//addAnim(cts.test, oActorEdgar, atype.face, [-1]);
//addAnim(cts.test, oActorEdgar, atype.say, ["Well, you suck too!"]);
//addAnim(cts.test, oActorEdgar, atype.move, [128, 0]);
#endregion

#region Opening
//Set up
animStart(cts.opening);
addActors(oActorMarzia);
addAnim(noone, atype.camPosRaw, [480 - CAM.WIDTH, 0]);
addAnim(noone, atype.wait, [30]);

///Scene
//Marzia part
addAnim(oActorMarzia, atype.moveAsync, [184, 0]);
addAnim(noone, atype.wait, [60]);
addAnim(oActorMarzia, atype.sayAsync, ["Hmm... Where's the lasagna?"]);
addAnim(noone, atype.wait, [160]);
addAnim(oActorMarzia, atype.face, [-1]);
addAnim(noone, atype.wait, [50]);

addAnim(oActorMarzia, atype.move, [-24, 0]);
addAnim(oActorMarzia, atype.say, ["I think I kept it here somewhere..."]);
addAnim(noone, atype.camShake, [5, 25, 0.2]);
addAnim(noone, atype.wait, [10]);
addAnim(oActorMarzia, atype.setState, [st.alert]);
addAnim(noone, atype.wait, [120]);

addAnim(oActorMarzia, atype.sayAsync, ["What was that?!"]);
addAnim(noone, atype.wait, [60]);

//Shadow dude part
addAnim(noone, atype.addInst, [432, 112, "Instances_0", oActorShadowDude]);
addAnim(oActorShadowDude, atype.face, [-1, 0]);
addAnim(oActorShadowDude, atype.moveAsync, [-120, 0]);
addAnim(noone, atype.wait, [180]);
addAnim(oActorMarzia, atype.face, [1, 0]);
addAnim(oActorMarzia, atype.sayAsync, ["!?"]);
addAnim(noone, atype.wait, [30]);

//Kidnap
addAnim(oActorShadowDude, atype.setState, [st.pickMarzia]);
//addAnim(noone, atype.destInst, [oActorMarzia]);
addAnim(oActorShadowDude, atype.setMoveSpeed, [1]);
addAnim(oActorShadowDude, atype.setImageSpeed, [0.75]);
addAnim(oActorShadowDude, atype.moveAsync, [120, 0]);
addAnim(noone, atype.wait, [40]);
addAnim(oActorShadowDude, atype.sayAsync, ["Felix! Felix!!!!"]);
addAnim(noone, atype.wait, [105]);
addAnim(noone, atype.destInst, [oActorShadowDude]);

//Pood
addAnim(noone, atype.addActor, [64, 112, "Instances_0", oActorPoods]);
addAnim(noone, atype.addActor, [64, 112, "Instances_0", oActorEdgar]);
addAnim(oActorPoods, atype.moveAsync, [200, 0]);
addAnim(noone, atype.wait, [40]);
addAnim(noone, atype.wait, [100]);
addAnim(oActorPoods, atype.say, ["Where's my lasagna, Marzia?"]);
addAnim(noone, atype.wait, [5]);
addAnim(oActorPoods, atype.sayAsync, ["Hmm, where's Marzia?"]);
addAnim(noone, atype.wait, [10]);
addAnim(oActorPoods, atype.face, [-1]);
addAnim(noone, atype.wait, [30]);
addAnim(oActorPoods, atype.face, [1]);
addAnim(noone, atype.wait, [30]);
addAnim(oActorPoods, atype.face, [-1]);
addAnim(noone, atype.wait, [30]);
addAnim(oActorPoods, atype.face, [1]);
addAnim(noone, atype.wait, [30]);

//End
addAnim(noone, atype.roomProgressAdd, [1]); //Progress
animEnd();
#endregion

#region Outside 0
animStart(cts.outside0);
addActors(oActorPoods, oActorEdgar, oActorBrad1);
addAnim(oActorBrad1, atype.setState, [st.hurt]);
addAnim(oActorBrad1, atype.face, [-1]);

addAnim(oActorPoods, atype.moveAsync, [84, 0]);
addAnim(noone, atype.wait, [40]);
addAnim(noone, atype.wait, [40]);
addAnim(oActorBrad1, atype.say, ["Oh hey Felix."]);
addAnim(oActorPoods, atype.say, ["Hey Brad, what happened to you?"]);
addAnim(oActorBrad1, atype.say, ["You won't believe it, Felix...", "I saw this shadow dude who kidnapped Marzia for some reason.", "I got hurt trying to free her..."]);
addAnim(oActorPoods, atype.sayAsync, ["Oh man... How are you feeling?", "Come into the hous-"]);
addAnim(noone, atype.wait, [220]);
addAnim(oActorPoods, atype.sayStop, []);
addAnim(oActorBrad1, atype.say, ["No, I'll be fine. You go to the town and try to get help to save Marzia."]);
addAnim(oActorPoods, atype.say, ["Okay. Take care Brad."]);

//addAnim(oActorPoods, atype.moveAsync, [64, 0]);
//addAnim(noone, atype.wait, [25]);
//addAnim(oActorBrad1, atype.face, [1]);
//addAnim(noone, atype.wait, [30]);
//addAnim(oActorPoods, atype.setState, [st.chairenter]);
//addAnim(oActorPoods, atype.moveAsync, [400, 0]);
//addAnim(noone, atype.wait, [60]);
//addAnim(oActorPoods, atype.sayAsync, ["I'm coming, Marzia!"]);
//addAnim(noone, atype.wait, [60]);
//addAnim(noone, atype.camStop, [0, 0]);
//addAnim(noone, atype.wait, [400]);

addAnim(noone, atype.roomProgressAdd, [1]); //Progress
animEnd();
#endregion

#region Martiplier
animStart(cts.outside0) {
	
} animEnd();
#endregion

#region Demo End
animStart(cts.demoEnd);
addActors(oActorPoods, oActorEdgar, oActorGurpreet);

addAnim(oActorPoods, atype.move, [256, 0]);
addAnim(oActorGurpreet, atype.face, [-1]);
addAnim(oActorGurpreet, atype.say, ["Hey, you're finally here."]);
addAnim(oActorPoods, atype.say, ["Who're you?"]);
addAnim(oActorGurpreet, atype.say, ["I'm the developer of this game, Gurpreet Singh Matharoo.",
									"I thought I'd show up to thank you for playing the game.",
									"It really means a lot to me. I wish I could take this project further and actually make a game out of it...",
									"But sadly, I have to focus on work and things that take a higher priority.",
									"Anyway, hope you enjoyed the prototype.",
									"I'd really love it if you could let me know what you thought of it, in the comments of the game page.",
									"Oh and..."]);

addAnim(oActorGurpreet, atype.face, [1, 0]);

addAnim(oActorGurpreet, atype.say, ["Here's the chair. Don't worry, I won't ask for $399.",
									"Take it, and fall into the pit.",
									"You'll be teleported back to the start of the game so that you could play with the chair.",
									"Have fun, and again, thanks for playing!"]);

addAnim(noone, atype.roomProgressAdd, [1]);
animEnd();
#endregion

#endregion

#region Main
//Debug
global.debug = 0; //1 = player/system, 2 = npcs/actors, 3 = doors
global.shortcuts = 1;
global.colTest = 0;
global.distTest = 1; //Distance test

//Data
global.map = ds_map_create();

//Collectibles
ds_map_add_list(global.map, "collectibles", array_to_list(global.collectVar));

//Progress
global.map[? "progress"] = 0;

//Object info
ds_map_add_map(global.map, "objInfo", ds_map_create());

//Room progress
var r = 0;
while (room_exists(r)){
	global.map[? string(r) + "_progress"] = 0;
	r++;
}
#endregion
