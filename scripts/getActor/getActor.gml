/// @arg entity

//Arguments
var _entity = argument[0];

//Loop
for(var i=0; i<array_length_1d(global.entity); i++){
	var val = global.entity[i];
	
	if (val==_entity){
		return i;
	}
}

//Not found
return noone;