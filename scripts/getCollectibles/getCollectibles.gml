//Check for collectibles
var collInst = instance_place(x, y, oCollectibleParent);

if (collInst && !collInst.collected && !collInst.hsp && !abs(collInst.vsp)){
	//Add
	global.collectVar[ collInst.type ] ++;
	
	//Collected
	collInst.collected = true;
	
	//Particles
	with (collInst) poof_particles_spread(sCoinPart, 3, 40);
	
	//Sound
	switch(collInst.object_index){
		case oCoin:
			//Sound
			audio_play_sound(sndCoinGet, 1, 0);
		break;
	}
}