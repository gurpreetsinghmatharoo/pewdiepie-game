/// @arg actor

//Arguments
var _actor = argument[0];

//Loop
for(var i=0; i<array_length_1d(global.entity); i++){
	var val = global.entity[i];
	
	if (i==_actor && val>0){
		return val;
	}
}

//Not found
return noone;