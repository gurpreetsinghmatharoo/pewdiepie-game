////Input
///Keyboard
//Movement
global.hor = (keyboard_check(ord("D")) || keyboard_check(vk_right))-(keyboard_check(ord("A")) || keyboard_check(vk_left));
global.ver = (keyboard_check(ord("S")) || keyboard_check(vk_down))-(keyboard_check(ord("W")) || keyboard_check(vk_up));

global.jump = keyboard_check_pressed(vk_space);
global.jumpHold = keyboard_check(vk_space);

global.up = keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up);
global.upHold = keyboard_check(ord("W")) || keyboard_check(vk_up);
global.down = keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down);
global.downHold = keyboard_check(ord("S")) || keyboard_check(vk_down);

//Items
global.Ikey = keyboard_check_pressed(ord("I"));
global.Okey = keyboard_check_pressed(ord("O"));
global.Okeyhold = keyboard_check(ord("O"));
global.Pkey = keyboard_check_pressed(ord("P"));
global.Pkeyhold = keyboard_check(ord("P"));
global.Ekey = keyboard_check_pressed(ord("E"));

//NPCs
global.textButton = ord("E");
global.textKeyHold = keyboard_check(global.textButton);

///Gamepad controls go here


///Based on the input
global.axisDir = point_direction(0, 0, global.hor, global.ver);
global.axisDirOn = (abs(global.hor) || abs(global.ver)) ? global.axisDir : global.axisDirOn; //Only when the axis is being used

///Key hold timings
//OP Pressed (Hold time)
if (global.Pkeyhold){
	global.PPressed++;
}
else{
	global.PPressed = 0;
}

if (global.Okeyhold){
	global.OPressed++;
}
else{
	global.OPressed = 0;
}

//Down hold time
if (global.downHold){
	global.downHoldTime++;
}
else{
	global.downHoldTime = 0;
}

//Hor hold time
if (global.hor & 1){
	global.horHoldTime += global.hor;
}
else{
	global.horHoldTime = 0;
}

//Mouse... unused
global.LMB = mouse_check_button(mb_left);
global.LMBPressed = mouse_check_button_pressed(mb_left);