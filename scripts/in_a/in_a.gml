/// @arg check
/// @arg [vals]

//Args
var _check = argument[0];
var _arr = argument[1];

//Function
for(var i=0; i<array_length_1d(_arr); i++){
	if (_check==_arr[i]) return true;
}

return false;