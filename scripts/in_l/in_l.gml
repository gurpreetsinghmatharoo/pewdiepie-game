/// @arg check
/// @arg [| vals]

//Args
var _check = argument[0];
var _list = argument[1];

//Function
for(var i=0; i<ds_list_size(_list); i++){
	if (_check==_list[| i]) return true;
}

return false;