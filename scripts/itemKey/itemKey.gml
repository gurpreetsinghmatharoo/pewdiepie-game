/// @arg itemType
/// @arg inputType
// 0 - Hit
// 1 - Hold
// 2 - Pressed (Time)

var pos = itemPos(argument0);

switch (argument1){
	case 0:
		if (pos==0) return global.Okey;
		if (pos==1) return global.Pkey;
		return 0;
	break;
	
	case 1:
		if (pos==0) return global.Okeyhold;
		if (pos==1) return global.Pkeyhold;
		return 0;
	break;
	
	case 2:
		if (pos==0) return global.OPressed;
		if (pos==1) return global.PPressed;
		return 0;
	break;
}