/// @arg type

//Args
var _item = argument[0];

//Vars
var pos = itemPos(_item);
var itemUse = global.items[_item, pr.use];
var itemCooldown = global.items[_item, pr.cooldown];

//Apply
global.inv[pos, item.hp] -= itemUse;
global.inv[pos, item.usecd] = itemCooldown;
global.inv[pos, item.refillcd] = 60;