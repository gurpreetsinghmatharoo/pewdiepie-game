/// @arg modifier
/// @arg key
/// @arg [extra_modifier]

//Args
var extMod = argument_count > 2 ? keyboard_check(argument[2]) : 1;

//Function
return keyboard_check(argument[0]) && keyboard_check_pressed(argument[1]) && extMod;