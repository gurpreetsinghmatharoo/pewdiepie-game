//Draw the flex particles on the layer
if (event_type==ev_draw && event_number==0 && instance_exists(oFlexGrid) && !oFlexGrid.isEmpty){
	//Surface
	global.layerSurf = surface_create(room_width, room_height);
	surface_set_target(global.layerSurf);
	draw_clear_alpha(0, 0);
}