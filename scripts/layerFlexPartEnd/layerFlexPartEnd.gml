//Draw the flex particles on the layer
if (event_type==ev_draw && event_number==0 && instance_exists(oFlexGrid) && !oFlexGrid.isEmpty){
	//Flex Part
	if (surface_exists(oFlexGrid.surf)){
		gpu_set_blendmode_ext(bm_dest_alpha, bm_inv_src_alpha);
		draw_surface(oFlexGrid.surf, 0, 0);
		gpu_set_blendmode(bm_normal);
	}
	
	//Surface
	surface_reset_target();
	draw_surface(global.layerSurf, 0, 0);
	
	//Free
	surface_free(global.layerSurf);
}