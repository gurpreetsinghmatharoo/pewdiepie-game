/// @arg x1
/// @arg y1
/// @arg x2
/// @arg y2
/// @arg <precision>

//Args
var x1 = argument[0];
var y1 = argument[1];
var x2 = argument[2];
var y2 = argument[3];
var prec = argument_count > 4 ? argument[4] : 4;

//Function
var dist = point_distance(x1, y1, x2, y2);
var dir = point_direction(x1, y1, x2, y2);

for(var i=1; i<=dist; i+=prec){
	//Get point
	var px = x1 + lengthdir_x(i, dir);
	var py = y1 + lengthdir_y(i, dir);
	
	//Check collision
	var col = tilemap_collision(global.Tiles_Main, px, py);
	
	if (col) return false;
}

//Path Clear
return true;