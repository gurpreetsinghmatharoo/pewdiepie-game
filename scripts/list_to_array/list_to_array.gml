/// @arg list

//Args
var _list = argument[0];

//Function
var arr = [];

for(var i=0; i<ds_list_size(_list); i++){
	arr[i] = _list[| i];
}

return arr;