/// @arg xAdd
/// @arg yAdd
/// @arg obj
/// @arg xmargin
/// @arg ymargin

//Args
var _xAdd = argument[0];
var _yAdd = argument[1];
var _obj = argument[2];
var _xMargin = argument[3];
var _yMargin = argument[4];

//Function
return collision_rectangle((bbox_left-_xMargin) + _xAdd,
						   (bbox_top-_yMargin) + _yAdd,
						   (bbox_right+_xMargin) + _xAdd,
						   (bbox_bottom+_yMargin) + _yAdd,
						   _obj, 0, 1);
						   