/// @param message_string
/// @param speed_key_optional
/// @param grid_sprite_optional
/// @param arrow_sprite_optional
/// @param scale_y_optional

//Scale y
var _scale_y = argument_count > 4 ? argument[4] : 1;
var _alpha = _scale_y * 2;

var text_scale = 0.5;

//Sets drawing offset to the middle top of the sprite
var xm = (sprite_width/2) - sprite_xoffset;
var ym = bbox_top - y;

//Move the textbox up for actors
if (object_get_parent(object_index)!=oSignParent) ym -= 4;

//Default sprites used for the textbox
var grid_sprite = spr_msg_grid;
var arrow_sprite = spr_msg_arrow;

//Set sprites from the arguments, if they have been given
if (argument_count>2) grid_sprite = argument[2];
if (argument_count>3) arrow_sprite = argument[3];

if (!variable_instance_exists(id, "char")) char = 0;
//This is the speed at which the text is drawn. You may change it:
var text_speed = 0.25;
if (argument_count>1){
	if (keyboard_check(argument[1])==true){
		//this is the boosted text speed
		text_speed = 0.6;
	}
}

var msg_org = argument[0];
var msg = string_copy(msg_org, 1, min(floor(char + text_speed), string_length(msg_org)));

var max_width_org = 96;
var drawn_str = get_drawn_string(0, 0, msg_org, -1, max_width_org, -1, text_scale, text_scale * _scale_y);
var max_width = min(string_width(drawn_str) * text_scale, max_width_org);
var max_height = string_height(drawn_str) * text_scale * _scale_y;

//get cell size
var cell_size = sprite_get_width(grid_sprite)/3;

//draw textbox
//textbox height (above player sprite)
var height = sprite_get_height(arrow_sprite) - sprite_get_yoffset(arrow_sprite);

//surface
//surface_set_target(global.tbSurf);
//xm -= camera_get_view_x(view_camera);
//ym -= camera_get_view_y(view_camera);

//get position
var x_loc = x + xm;
var y_loc = y + ym - (ym * (1-_scale_y));

//get arrow offset
var arrow_xm = sprite_get_width(arrow_sprite)/2 - sprite_get_xoffset(arrow_sprite);
//top left corner
draw_sprite_part_ext(grid_sprite, 0, 0, 0, cell_size, cell_size, x_loc - ((max_width/2)+cell_size), y_loc - (max_height + cell_size*2), 1, 1, -1, _alpha);
//top right corner
draw_sprite_part_ext(grid_sprite, 0, cell_size*2, 0, cell_size, cell_size, x_loc + (max_width/2), y_loc - (max_height + cell_size*2), 1, 1, -1, _alpha);
//top part
draw_sprite_part_ext(grid_sprite, 0, cell_size, 0, cell_size, cell_size, x_loc - (max_width/2), y_loc - (max_height + cell_size*2), max_width/cell_size, 1, c_white, _alpha);
//bottom left corner
draw_sprite_part_ext(grid_sprite, 0, 0, cell_size*2, cell_size, cell_size, x_loc - ((max_width/2)+cell_size), y_loc - cell_size, 1, 1, -1, _alpha);
//left part
draw_sprite_part_ext(grid_sprite, 0, 0, cell_size, cell_size, cell_size, x_loc - ((max_width/2)+cell_size), y_loc - (max_height + cell_size), 1, max_height/cell_size, c_white, _alpha);
//bottom right corner
draw_sprite_part_ext(grid_sprite, 0, cell_size*2, cell_size*2, cell_size, cell_size, x_loc + (max_width/2), y_loc - cell_size, 1, 1, -1, _alpha);
//right part
draw_sprite_part_ext(grid_sprite, 0, cell_size*2, cell_size, cell_size, cell_size, x_loc + (max_width/2), y_loc - (max_height + cell_size), 1, max_height/cell_size, c_white, _alpha);
//bottom part
draw_sprite_part_ext(grid_sprite, 0, cell_size, cell_size*2, cell_size, cell_size, x_loc - (max_width/2), y_loc - (cell_size), max_width/cell_size, 1, c_white, _alpha);
//center part
draw_sprite_part_ext(grid_sprite, 0, cell_size, cell_size, cell_size, cell_size, x_loc - (max_width/2), y_loc - (max_height + cell_size), max_width/cell_size, max_height/cell_size, c_white, _alpha);

//draw_9slice(x - ((max_width/2)+cell_size), bbox_top - (max_height + cell_size*2), 18, 13, grid_sprite, 0, -1, 1, 1);
//draw_9slice(x, y, 64, 64, grid_sprite, 0);
//arrow
draw_sprite_ext(arrow_sprite, 0, x_loc - arrow_xm, y + ym, 1, _scale_y, 0, -1, _alpha);
//text
draw_string(x_loc - (max_width/2), y_loc - (max_height + cell_size), msg_org, -1, max_width_org, char, text_scale, text_scale * _scale_y, 0, -1, _alpha);
//other
char += text_speed;

//surface
//surface_reset_target();

//Debug
//draw_values(x, y, x_loc, "x loc", y_loc, "y loc", max_width, "max width", max_height, "max height");
//draw_text(x, y-64, drawn_str);

//Return
return char >= string_length(msg_org);