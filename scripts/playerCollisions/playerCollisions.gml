//Move out if stuck
collisionSafe();

//Horizontal Collisions
var colHsp = collision(hsp, 0);
if (colHsp){
	//Push
	var pushed = false;
	
	//Player push
	if (object_index==oPlayer && colHsp>1 && object_get_parent(colHsp.object_index)==oPushableParent){
		//Floor position
		x = floor(x);
		
		//Move closer
		var _sign = min(abs(hsp), abs(sign(hsp))) * sign(hsp);
		
		while(!collision(_sign, 0)){
			x += _sign;
		}
		
		//Set speeds
		hsp = global.pushSpeed * sign(hsp);
		colHsp.hsp = hsp;
		
		//Check collisions for the box
		with (colHsp){
			playerCollisions();
			
			x += hsp;
		}
		
		//Pushed
		if (abs(colHsp.hsp)){
			pushed = true;
		}
	}
	
	//Pushable push
	if (object_get_parent(object_index)==oPushableParent){
		//Check pushable collisions
		var pushCol = instance_place(x + hsp, y, oPushableParent);
		
		//Push
		if (pushCol && (object_get_parent(pushCol.object_index)==oPushableParent)){
			//Move closer
			while(!collision(sign(hsp), 0)){
				x += sign(hsp);
			}
		
			//Set speeds
			hsp = global.pushSpeed * sign(hsp);
			pushCol.hsp = hsp;
		
			//Check collisions for the box
			with (pushCol){
				playerCollisions();
			
				x += hsp;
			}
		
			//Pushed
			if (abs(pushCol.hsp)){
				pushed = true;
			}
		}
	}
	
	//Normal collision
	if (!pushed){
		var _sign = sign(hsp);
		
		//Slope
		//var slopeUp = !collision(hsp, -global.slopeHeight);
		slope = false; //Slops disabled
		
		if (slope){
			var upY = -1;
			
			while(collision(hsp, upY)){
				upY--;
			}
		
			vsp = upY;
		}
		//Stop
		else if (!createdInCol){
			while(!collision(_sign, 0)){
				x += _sign;
			}

			hsp = 0;
		}
	}
}

//Slope going down
//slope = abs(hsp) && !collision(0, 1) && collision(0, global.slopeHeight+1) && collision(-hsp, 1);

//if (slope){
//	var downY = 1;
	
//	while (!collision(hsp, downY)){
//		downY++;
//	}
	
//	vsp = downY - 1;
//}

//Vertical Collisions
if (collision(0, vsp) && !createdInCol){
	while(!collision(0, sign(vsp))){
		y += sign(vsp);	
	}
	
	//Bounce
	if (object_get_parent(object_index)==oCollectibleParent){
		vsp = -vsp*0.6;
	}
	else{
		vsp = 0;
	}
}

//Diagonal Collisions
if (collision(hsp, vsp) && !createdInCol){
	if (abs(hsp)) hsp = 0;
	else vsp = 0;
}