//Gravity
if (vsp < 10 && !gravCooldown){
	vsp += gravSpeed;
	gravCooldown = gravInterval;
	
	//Jump more by reducing gravity
	var forPlayer = object_index==oPlayer && global.jumpHold && vsp<0;
	forPlayer = forPlayer || state==st.walltoright || state==st.walltoleft;
	var forEdgar = object_index==oEdgar && oPlayer.bbox_bottom < bbox_top;
	
	if (forPlayer || forEdgar){
		gravCooldown *= 2;
	}
}

//Cooldown
gravCooldown -= gravCooldown > 0;