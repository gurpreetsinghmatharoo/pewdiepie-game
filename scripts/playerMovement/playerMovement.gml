/// @arg lerpSpeed

//HSP
hsp = lerp(hsp, global.hor * moveSpeed + boostX, argument0);

//Go below one way tile
var onewayBelow = tilemap_collision(global.Tiles_Oneway, x, bbox_bottom+1);
var floorBelow = collision(0, 1, false);

if (object_index==oPlayer && onewayBelow && !floorBelow && global.downHoldTime > 8){
	y += gravSpeed*4;
	//global.onewayCollision = false;
	
	follower.alarm[0] = room_speed;
}