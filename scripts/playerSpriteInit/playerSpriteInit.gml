//Sprites
sprite[st.idle] = sPlayer_Idle;
sprite[st.move] = sPlayer_Move;
sprite[st.jump] = sPlayer_Jump;
sprite[st.dead] = sPlayer_Idle;
sprite[st.walltoright] = sPlayer_Walljump;
sprite[st.walltoleft] = sPlayer_Walljump;

sprite[st.push] = sPlayer_Push;

sprite[st.pew] = sPlayer_Pew;
sprite[st.pew2] = sPlayer_Pew2;

//Chair
sprite[st.chair] = sPlayer_Chair;
sprite[st.chairenter] = sPlayer_ChairEnter;
sprite[st.chairexit] = sPlayer_ChairEnter;
sprite[st.chairdown] = sPlayer_ChairAnim;
sprite[st.chairup] = sPlayer_ChairAnim;
sprite[st.chairlie] = sPlayer_ChairLie;
sprite[st.chairattack] = sPlayer_ChairLie;

sprite[st.itemselect] = sPlayer_ItemGet;

//Flex Gun
sprite[st.flexgun_0] = sPlayer_FlexGun_0;
sprite[st.flexgun_1] = sPlayer_FlexGun_1;
sprite[st.flexgun_2] = sPlayer_FlexGun_2;
sprite[st.flexgun_m1] = sPlayer_FlexGun_m1;

defaultMask = sprite[st.idle];
mask_index = defaultMask;