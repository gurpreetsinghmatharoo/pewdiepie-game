/// @arg x
/// @arg y
/// @arg sprite
/// @arg dir
/// @arg life
/// @arg [color]

//Args
var _x = argument[0];
var _y = argument[1];
var _sprite = argument[2];
var _dir = argument[3];
var _life = argument[4];
var _color = argument_count > 5 ? argument[5] : -1;

//Function
part_type_sprite(global.poofPart, _sprite, 0, 0, 0);
part_type_direction(global.poofPart, _dir, _dir, 0, 0);
part_type_life(global.poofPart, _life - 5, _life + 5);

//Color
if (_color >= 0) part_type_color1(global.poofPart, _color);

part_particles_create(global.partSys, _x, _y, global.poofPart, 1);