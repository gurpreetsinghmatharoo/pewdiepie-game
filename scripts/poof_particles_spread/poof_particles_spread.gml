/// @arg sprite
/// @arg count
/// @arg life

//Arguments
var _spr = argument[0];
var _count = argument[1];
var _life = argument[2];

//Function
repeat(_count){
	var xx = irandom_range(bbox_left, bbox_right);
	var yy = irandom_range(bbox_top, bbox_bottom);
	var dir = point_direction(x, y, xx, yy);
	poof_particle(xx, yy, _spr, dir, _life);
}