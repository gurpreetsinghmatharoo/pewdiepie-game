/// @args restarted?

if (!file_exists(global.FNTempSave)) return;

//Args
var restarted = argument[0];

//Vars
var sMap = ds_map_secure_load(global.FNTempSave);
var mapD = sMap[? "objInfo"];
var map = mapD[? room_get_name(room)];

	if (map==undefined) return; //No save info for this room

var savedObjs = map[? "savedObjs"];
var savedParents = map[? "savedParents"];

	if (ds_list_size(savedObjs)<=0) show_error("Empty list loaded", 1); //Should not be empty

//Copy map
ds_map_copy(global.map, sMap);

//Loop
var count = map[? "count"];

for(var i=0; i<count; i++){
	//Vars
	var _id = map[? string(i)];
	var obj = map[? string(i)+"_obj"]
	var type = map[? string(i)+"_type"];
	
	//Player skip if loading through teleporter
	if ((obj==oPlayer || object_get_parent(obj)==oFollowerParent) && !restarted){
		continue;
	}
	
	//Create
	if (!instance_exists(_id)){
		//Player and follower
		if (obj==oPlayer || object_get_parent(obj)==oFollowerParent){
			//Instantiate
			_id = instance_create_layer(0, 0, "Instances_0", obj);
			
			//Add to map
			map[? string(i)] = _id;
			map[? string(_id)] = i;
		}
	}
	
	//Load variables
	if (instance_exists(_id)){
		switch (type){
			case 0: //Characters
				//Load
				_id.x =			map[? string(i)+"_x"];
				_id.y =			map[? string(i)+"_y"];
				_id.state =		map[? string(i)+"_state"];
				_id.hsp =		map[? string(i)+"_hsp"];
				_id.vsp =		map[? string(i)+"_vsp"];
				_id.boostX =	map[? string(i)+"_boostX"];
				_id.boostY =	map[? string(i)+"_boostY"];
			break;
			
			case 1: //Item pickups
				_id.x =			map[? string(i)+"_x"];
				_id.y =			map[? string(i)+"_y"];
				_id.itemType =		map[? string(i)+"_itemType"];
			break;
		}
		
		//Log
		log(object_get_name(obj) + " loaded!");
	}
}

//Delete instances that are not present in the save
//with (all){
//	var _id = map[? string(id)];
	
//	if (_id == undefined && (in_l(object_index, savedObjs) || in_l(object_get_parent(object_index), savedParents))){
//		instance_destroy(id, 0);
//	}
//}

//To vars
global.collectVar = list_to_array(sMap[? "collectibles"]);
global.playerHP = sMap[? "playerHP"];

//Inventory
var w = global.map[? "inv_w"];
var h = global.map[? "inv_h"];

for(var i=0; i<h; i++){
	for(var j=0; j<w; j++){
		global.inv[i, j] = global.map[? string(i) + string(j)];
	}
}