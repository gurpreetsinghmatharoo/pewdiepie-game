//Vars
var mapD = global.map[? "objInfo"];
var map = mapD[? room_get_name(room)];

if (map==undefined){
	map = ds_map_create();
	ds_map_add_map(mapD, room_get_name(room), map);
}

ds_map_clear(map);

var count = 0;

//Object info
var savedObjs = ds_list_create();
ds_list_add(savedObjs, oPlayer, oItemPickup);
ds_map_add_list(map, "savedObjs", savedObjs);

var savedParents = ds_list_create();
ds_list_add(savedParents, oFollowerParent, oNPCParent);
ds_map_add_list(map, "savedParents", savedParents);

with (all){
	//Characters : 0
	if (in(object_index, oPlayer)
	 || in(object_get_parent(object_index), oFollowerParent, oNPCParent)){
			//Main
			map[? string(id)] = count;
			map[? string(count)] = id;
			map[? string(count)+"_obj"] = object_index;
			map[? string(count)+"_type"] = 0;
			
			//Basic
			map[? string(count)+"_x"]  = x;
			map[? string(count)+"_y"]  = y;
			map[? string(count)+"_state"]  = state;
			
			//Physics
			map[? string(count)+"_hsp"]  = hsp;
			map[? string(count)+"_vsp"]  = vsp;
			map[? string(count)+"_boostX"]  = boostX;
			map[? string(count)+"_boostY"]  = boostY;
			
			//Count
			count++;
	
			log(object_get_name(object_index) + " saved!");
	}
	
	//Item pickups : 1
	else if (object_index==oItemPickup){
			//Main
			map[? string(id)] = count;
			map[? string(count)] = id;
			map[? string(count)+"_obj"] = object_index;
			map[? string(count)+"_type"] = 1;
			
			//Basic
			map[? string(count)+"_x"]  = x;
			map[? string(count)+"_y"]  = y;
			map[? string(count)+"_itemType"]  = itemType;
			
			//Count
			count++;
	
			log(object_get_name(object_index) + " saved!");
	}
	
	//Adding more? Don't forget to update savedObjs
}
	
map[? "count"] = count;

//Main
global.map[? "playerHP"] = global.playerHP;
ds_map_replace_list(global.map, "collectibles", array_to_list(global.collectVar));

//Inventory
global.map[? "inv_h"] = array_height_2d(global.inv);
global.map[? "inv_w"] = array_length_2d(global.inv, 0);

for(var i=0; i<array_height_2d(global.inv); i++){
	for(var j=0; j<array_length_2d(global.inv, i); j++){
		global.map[? string(i) + string(j)] = global.inv[i, j];
	}
}

//Save
ds_map_secure_save(global.map, global.FNTempSave);

//Test
//var file = file_text_open_write(global.FNTempSave);
//file_text_write_string(file, json_encode(global.map));
//file_text_close(file);