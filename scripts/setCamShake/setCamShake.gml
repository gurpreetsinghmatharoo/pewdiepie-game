/// @arg shakeX
/// @arg shakeY
/// @arg falloff

global.shakeX = argument0;
global.shakeY = argument1;
global.shakeFalloff = argument2;