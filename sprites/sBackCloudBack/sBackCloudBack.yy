{
    "id": "666030a9-191b-453d-854c-f1358e061306",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackCloudBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 100,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8291208-0ca3-4b1f-b460-719af1cc376a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "666030a9-191b-453d-854c-f1358e061306",
            "compositeImage": {
                "id": "7c10de64-28bf-47b4-84fc-162b1b579fb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8291208-0ca3-4b1f-b460-719af1cc376a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4092615e-09b7-4f50-9f6d-aaa01f5d5c55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8291208-0ca3-4b1f-b460-719af1cc376a",
                    "LayerId": "9dfcfd83-d2c6-4e2c-856a-6f2fdc322492"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "9dfcfd83-d2c6-4e2c-856a-6f2fdc322492",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "666030a9-191b-453d-854c-f1358e061306",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}