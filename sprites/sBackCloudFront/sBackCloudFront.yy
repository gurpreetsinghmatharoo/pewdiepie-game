{
    "id": "b9731c4a-9146-4584-834f-20ac4bbdb383",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackCloudFront",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 95,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb8097d4-eadb-4d0a-9d4c-eb15ceef804d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9731c4a-9146-4584-834f-20ac4bbdb383",
            "compositeImage": {
                "id": "ac8499d3-fee3-4880-8832-8303f4ecdf37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb8097d4-eadb-4d0a-9d4c-eb15ceef804d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc0fc68f-725d-43dd-b3c0-a72ea6507641",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb8097d4-eadb-4d0a-9d4c-eb15ceef804d",
                    "LayerId": "b511cd7b-93bb-4a78-af2e-dc9f2f398a57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "b511cd7b-93bb-4a78-af2e-dc9f2f398a57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9731c4a-9146-4584-834f-20ac4bbdb383",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}