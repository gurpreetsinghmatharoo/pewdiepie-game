{
    "id": "1ff2202c-d432-420e-a8c8-7866380be72e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackHills",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 100,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a039ffdc-823c-4662-9726-57910642f83e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ff2202c-d432-420e-a8c8-7866380be72e",
            "compositeImage": {
                "id": "c7930611-a95c-4650-a966-3ed457b9d6ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a039ffdc-823c-4662-9726-57910642f83e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "037b491d-c409-418e-a4d1-89a834c923cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a039ffdc-823c-4662-9726-57910642f83e",
                    "LayerId": "af696956-8b12-42e4-9d9d-64f761f36a55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "af696956-8b12-42e4-9d9d-64f761f36a55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ff2202c-d432-420e-a8c8-7866380be72e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}