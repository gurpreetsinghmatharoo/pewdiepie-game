{
    "id": "1c54dcfb-ccab-4692-8159-af164107e51b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackMain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15d82405-82f9-46b3-9f7d-a92c5638f484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c54dcfb-ccab-4692-8159-af164107e51b",
            "compositeImage": {
                "id": "6ade4757-6081-4de1-a963-1f01cf66eec1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15d82405-82f9-46b3-9f7d-a92c5638f484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a71a7739-12cf-47a3-987c-c11dcd19e0e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15d82405-82f9-46b3-9f7d-a92c5638f484",
                    "LayerId": "b970cb0f-58ec-4306-bca8-761f8cb07404"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "b970cb0f-58ec-4306-bca8-761f8cb07404",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c54dcfb-ccab-4692-8159-af164107e51b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}