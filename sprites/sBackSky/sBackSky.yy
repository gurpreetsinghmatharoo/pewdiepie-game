{
    "id": "966c396d-92c4-4ec1-a3fc-294f9c3e426f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackSky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4151f883-ae67-4281-8af9-ce966d1f5338",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "966c396d-92c4-4ec1-a3fc-294f9c3e426f",
            "compositeImage": {
                "id": "0164f486-729b-4bd9-a90c-01a4ffa717cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4151f883-ae67-4281-8af9-ce966d1f5338",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dac2725-2bfe-44ee-b9bd-4db8c47505c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4151f883-ae67-4281-8af9-ce966d1f5338",
                    "LayerId": "44fbd884-80ca-47e9-848e-378fae2af60d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "44fbd884-80ca-47e9-848e-378fae2af60d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "966c396d-92c4-4ec1-a3fc-294f9c3e426f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}