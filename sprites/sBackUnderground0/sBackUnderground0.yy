{
    "id": "f0da1348-ccc4-483c-97c3-9ba11be3c15a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackUnderground0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4c2e458-fe97-4ae2-ab0b-ccd2fe275d83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0da1348-ccc4-483c-97c3-9ba11be3c15a",
            "compositeImage": {
                "id": "a511cc7d-4ada-4d42-a45b-0fe7db1499e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4c2e458-fe97-4ae2-ab0b-ccd2fe275d83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3694b5f9-d203-46b7-8271-e2c240faa017",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4c2e458-fe97-4ae2-ab0b-ccd2fe275d83",
                    "LayerId": "887a893f-8d59-4f60-ad65-a337517e2e7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "887a893f-8d59-4f60-ad65-a337517e2e7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0da1348-ccc4-483c-97c3-9ba11be3c15a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}