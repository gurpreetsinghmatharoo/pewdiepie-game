{
    "id": "27e7672c-b2cf-4857-b91a-619dd438cb80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackUnderground1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e9e4775-45d1-46f6-adc3-de5c1c5d6246",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27e7672c-b2cf-4857-b91a-619dd438cb80",
            "compositeImage": {
                "id": "3bb986ee-caf0-43c7-ae38-d65bca69c046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e9e4775-45d1-46f6-adc3-de5c1c5d6246",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a43ed3f7-41dc-4649-abed-68775c348359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e9e4775-45d1-46f6-adc3-de5c1c5d6246",
                    "LayerId": "acb4518c-90d0-4dde-978d-8010ef2e164e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "acb4518c-90d0-4dde-978d-8010ef2e164e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27e7672c-b2cf-4857-b91a-619dd438cb80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}