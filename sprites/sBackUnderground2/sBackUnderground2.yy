{
    "id": "92c650d3-f6b7-491b-bd8e-f453d108b9d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackUnderground2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8480af87-1cd5-49e6-a2e6-5a0f38cc7b8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92c650d3-f6b7-491b-bd8e-f453d108b9d9",
            "compositeImage": {
                "id": "3a862d3f-0d84-4b33-ba0a-14a45baf683a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8480af87-1cd5-49e6-a2e6-5a0f38cc7b8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9f50653-9a91-45b1-ab41-478f52e6f164",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8480af87-1cd5-49e6-a2e6-5a0f38cc7b8a",
                    "LayerId": "e6452999-83be-41d3-81f5-13c7e3d4c1da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "e6452999-83be-41d3-81f5-13c7e3d4c1da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92c650d3-f6b7-491b-bd8e-f453d108b9d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}