{
    "id": "c23e6d84-6edf-4cdc-958a-e857d4cae486",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackUnderground3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58cacb6f-f8bb-4c07-a4da-e97c5d86b3f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c23e6d84-6edf-4cdc-958a-e857d4cae486",
            "compositeImage": {
                "id": "d6e398a4-32fa-4ed8-aeb9-6b8d9e7f79d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58cacb6f-f8bb-4c07-a4da-e97c5d86b3f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e3d16d9-52f1-4543-b060-6a2c7fb109b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58cacb6f-f8bb-4c07-a4da-e97c5d86b3f2",
                    "LayerId": "9ed2b475-b039-4bd4-8e67-7b43a5919302"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "9ed2b475-b039-4bd4-8e67-7b43a5919302",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c23e6d84-6edf-4cdc-958a-e857d4cae486",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}