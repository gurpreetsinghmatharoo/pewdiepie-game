{
    "id": "c5d6abf1-ffc9-4516-b088-cc41b863c103",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "916b8c54-faec-4770-8ede-74a9146dfa8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d6abf1-ffc9-4516-b088-cc41b863c103",
            "compositeImage": {
                "id": "159995bb-0fb8-47f1-9e16-fece4566580d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "916b8c54-faec-4770-8ede-74a9146dfa8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87bcd6db-e73f-4e66-ae1f-1da8cfd36334",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "916b8c54-faec-4770-8ede-74a9146dfa8b",
                    "LayerId": "8b529d9c-cb69-4d90-8820-b08c4469dfe3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8b529d9c-cb69-4d90-8820-b08c4469dfe3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5d6abf1-ffc9-4516-b088-cc41b863c103",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}