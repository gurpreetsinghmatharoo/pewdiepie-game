{
    "id": "124e4812-e4d2-4d8e-a2af-14e5163f9390",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBoxBig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48ce2a7d-a4f1-4169-9c8d-6016ef99536d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124e4812-e4d2-4d8e-a2af-14e5163f9390",
            "compositeImage": {
                "id": "c6a8338b-7c61-4ce0-9433-14cadb830089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48ce2a7d-a4f1-4169-9c8d-6016ef99536d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc9fbbb9-e773-4c7d-b4b3-0c56359b7fac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48ce2a7d-a4f1-4169-9c8d-6016ef99536d",
                    "LayerId": "f690cd1f-fc59-4021-be75-a8a00b045d98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f690cd1f-fc59-4021-be75-a8a00b045d98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "124e4812-e4d2-4d8e-a2af-14e5163f9390",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}