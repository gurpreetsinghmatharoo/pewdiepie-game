{
    "id": "c4926a55-2a52-4e41-b544-2403a985a0d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBrad1_Hurt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 23,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e238b75-c766-42ef-bf9a-6c906f5524b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4926a55-2a52-4e41-b544-2403a985a0d7",
            "compositeImage": {
                "id": "0c296aaa-9d1c-40d6-ad75-af78f2849fb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e238b75-c766-42ef-bf9a-6c906f5524b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "988f8782-d5fb-4cfa-841e-4ac9896f75f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e238b75-c766-42ef-bf9a-6c906f5524b8",
                    "LayerId": "d1ac0321-d5d8-4b7c-ae77-ed55c666015c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d1ac0321-d5d8-4b7c-ae77-ed55c666015c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4926a55-2a52-4e41-b544-2403a985a0d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}