{
    "id": "0bccdfe8-a07b-46b0-bb2c-ebc0b96e2fa0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBrad1_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 20,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc97a407-b1dc-42dc-8749-90796095d779",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bccdfe8-a07b-46b0-bb2c-ebc0b96e2fa0",
            "compositeImage": {
                "id": "c804a1fd-3d5e-42ea-ae1f-47df71aebc75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc97a407-b1dc-42dc-8749-90796095d779",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "155dd5c1-594e-4cf4-9842-c96399779cee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc97a407-b1dc-42dc-8749-90796095d779",
                    "LayerId": "449aab7d-f7ce-4174-954b-578f8d29ff8b"
                }
            ]
        },
        {
            "id": "8b24767d-a690-4f80-bbe3-df94a4a2d83d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bccdfe8-a07b-46b0-bb2c-ebc0b96e2fa0",
            "compositeImage": {
                "id": "276ad2cd-d6ab-4b69-8b2e-19368d4fa844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b24767d-a690-4f80-bbe3-df94a4a2d83d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "452b59ca-2886-4ab5-bf48-4391791c8830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b24767d-a690-4f80-bbe3-df94a4a2d83d",
                    "LayerId": "449aab7d-f7ce-4174-954b-578f8d29ff8b"
                }
            ]
        },
        {
            "id": "80d0b39a-2735-40b7-bd48-4c2ae44a5dee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bccdfe8-a07b-46b0-bb2c-ebc0b96e2fa0",
            "compositeImage": {
                "id": "d6b50212-c084-4002-a9a5-50d4c0415b2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80d0b39a-2735-40b7-bd48-4c2ae44a5dee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec6f4002-3b47-42b3-a719-1ea401443ff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80d0b39a-2735-40b7-bd48-4c2ae44a5dee",
                    "LayerId": "449aab7d-f7ce-4174-954b-578f8d29ff8b"
                }
            ]
        },
        {
            "id": "bdb6ea9f-bd88-44dd-afb0-3bf77fc2f810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bccdfe8-a07b-46b0-bb2c-ebc0b96e2fa0",
            "compositeImage": {
                "id": "8f814c2a-57dc-4930-aa46-6b4c9e151d7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdb6ea9f-bd88-44dd-afb0-3bf77fc2f810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06ff332f-d95a-42e3-b7c1-086e995769b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdb6ea9f-bd88-44dd-afb0-3bf77fc2f810",
                    "LayerId": "449aab7d-f7ce-4174-954b-578f8d29ff8b"
                }
            ]
        },
        {
            "id": "df568b14-0b6c-4b43-9e04-94843d8cca73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bccdfe8-a07b-46b0-bb2c-ebc0b96e2fa0",
            "compositeImage": {
                "id": "9776568e-5880-44f6-84ad-ce006ca4114f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df568b14-0b6c-4b43-9e04-94843d8cca73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68273a7f-3959-48ae-b404-c490d0ebdb5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df568b14-0b6c-4b43-9e04-94843d8cca73",
                    "LayerId": "449aab7d-f7ce-4174-954b-578f8d29ff8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "449aab7d-f7ce-4174-954b-578f8d29ff8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0bccdfe8-a07b-46b0-bb2c-ebc0b96e2fa0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}