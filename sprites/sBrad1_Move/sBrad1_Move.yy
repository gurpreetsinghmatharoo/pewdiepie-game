{
    "id": "7d9acb26-df1d-49f7-a4e5-5172044a3efb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBrad1_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 23,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87ec8596-54de-434a-b122-88d79f8c4004",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9acb26-df1d-49f7-a4e5-5172044a3efb",
            "compositeImage": {
                "id": "e60524e5-0af8-4cdb-96a1-f543b4ce0b4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87ec8596-54de-434a-b122-88d79f8c4004",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88d77aa4-7292-4c63-a3b5-4e43c6f1e2ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87ec8596-54de-434a-b122-88d79f8c4004",
                    "LayerId": "a472f2f4-aaaa-4104-8cd4-5c15e45a8925"
                }
            ]
        },
        {
            "id": "66e69f9b-444e-4625-999b-1ab6340f9c94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9acb26-df1d-49f7-a4e5-5172044a3efb",
            "compositeImage": {
                "id": "4b4e561f-1d91-4812-8b3e-ddbe4cbd4464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66e69f9b-444e-4625-999b-1ab6340f9c94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f6e1f99-935e-4b95-accd-26eea3d458e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66e69f9b-444e-4625-999b-1ab6340f9c94",
                    "LayerId": "a472f2f4-aaaa-4104-8cd4-5c15e45a8925"
                }
            ]
        },
        {
            "id": "c6f0c67f-5ed1-41fa-9c33-7905cadd7a8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9acb26-df1d-49f7-a4e5-5172044a3efb",
            "compositeImage": {
                "id": "6ee6ea3c-1079-4469-aa66-e45c5270c91b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6f0c67f-5ed1-41fa-9c33-7905cadd7a8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28b00d5f-c064-41ec-a063-d448e71e2669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6f0c67f-5ed1-41fa-9c33-7905cadd7a8a",
                    "LayerId": "a472f2f4-aaaa-4104-8cd4-5c15e45a8925"
                }
            ]
        },
        {
            "id": "d7c801ec-4395-4219-b69c-47f3f8a987ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9acb26-df1d-49f7-a4e5-5172044a3efb",
            "compositeImage": {
                "id": "b20ad4e5-663f-43de-bca2-86cb44e00e74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7c801ec-4395-4219-b69c-47f3f8a987ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "291b3eb6-a42c-4c5d-b24a-d19c8574395f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7c801ec-4395-4219-b69c-47f3f8a987ca",
                    "LayerId": "a472f2f4-aaaa-4104-8cd4-5c15e45a8925"
                }
            ]
        },
        {
            "id": "379ba947-47a1-4e0d-a28a-88285551291b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9acb26-df1d-49f7-a4e5-5172044a3efb",
            "compositeImage": {
                "id": "9e1a4cb7-955c-41e6-907d-bf2ecb00a8e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "379ba947-47a1-4e0d-a28a-88285551291b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7906c2e3-e092-4faa-99ad-e93984f672b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "379ba947-47a1-4e0d-a28a-88285551291b",
                    "LayerId": "a472f2f4-aaaa-4104-8cd4-5c15e45a8925"
                }
            ]
        },
        {
            "id": "b30396d6-5a9f-4648-a316-406ccb03b16d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9acb26-df1d-49f7-a4e5-5172044a3efb",
            "compositeImage": {
                "id": "0e495b65-112f-4314-a6cc-8fbe8edae2a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b30396d6-5a9f-4648-a316-406ccb03b16d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fc734a7-fa9c-403f-9bd7-69a47a7d995b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b30396d6-5a9f-4648-a316-406ccb03b16d",
                    "LayerId": "a472f2f4-aaaa-4104-8cd4-5c15e45a8925"
                }
            ]
        },
        {
            "id": "270bc8a6-3cae-460d-8c23-802fd4edfb87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9acb26-df1d-49f7-a4e5-5172044a3efb",
            "compositeImage": {
                "id": "2ed7822e-f528-4a01-9772-64e391cc6d84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "270bc8a6-3cae-460d-8c23-802fd4edfb87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fff6d915-aa3e-46e7-814d-dee28ab21be9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "270bc8a6-3cae-460d-8c23-802fd4edfb87",
                    "LayerId": "a472f2f4-aaaa-4104-8cd4-5c15e45a8925"
                }
            ]
        },
        {
            "id": "fb8b1240-339a-442c-a948-c6eb587244d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9acb26-df1d-49f7-a4e5-5172044a3efb",
            "compositeImage": {
                "id": "fcfbf8b6-6daf-45b3-bf3f-a0d212e2b151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb8b1240-339a-442c-a948-c6eb587244d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6313744-7c1a-4b0e-99c6-2829c3481f77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb8b1240-339a-442c-a948-c6eb587244d5",
                    "LayerId": "a472f2f4-aaaa-4104-8cd4-5c15e45a8925"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a472f2f4-aaaa-4104-8cd4-5c15e45a8925",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d9acb26-df1d-49f7-a4e5-5172044a3efb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}