{
    "id": "5bea8837-1e83-4972-bf2d-6a73ff7803d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChairSpinPart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4546ac5b-6aa1-4772-822c-77f24ad8146f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bea8837-1e83-4972-bf2d-6a73ff7803d6",
            "compositeImage": {
                "id": "d511e9a1-5cf1-417d-af36-34bf217fff6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4546ac5b-6aa1-4772-822c-77f24ad8146f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf3dd864-9b20-419f-9de0-a94f25382730",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4546ac5b-6aa1-4772-822c-77f24ad8146f",
                    "LayerId": "d8396f67-205e-42c3-a1f6-78ff5253c249"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d8396f67-205e-42c3-a1f6-78ff5253c249",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bea8837-1e83-4972-bf2d-6a73ff7803d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 12
}