{
    "id": "d3ffc403-89b7-47eb-9ad2-4d5fbb93cbcf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCoin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef4e88c5-0a9e-4243-b83b-d090fdfda297",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3ffc403-89b7-47eb-9ad2-4d5fbb93cbcf",
            "compositeImage": {
                "id": "713910ad-4427-4a17-97fa-1a341285ead3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef4e88c5-0a9e-4243-b83b-d090fdfda297",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a12a8cc-b12e-4d37-a830-86e55030d3dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef4e88c5-0a9e-4243-b83b-d090fdfda297",
                    "LayerId": "884dfb44-f262-4c5c-8b0a-731ba0cdbc1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "884dfb44-f262-4c5c-8b0a-731ba0cdbc1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3ffc403-89b7-47eb-9ad2-4d5fbb93cbcf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}