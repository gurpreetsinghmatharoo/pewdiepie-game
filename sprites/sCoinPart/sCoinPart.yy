{
    "id": "5d123093-cd94-431f-82fd-f97b001c2559",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCoinPart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1ac139a-c677-4daf-8a9e-d3f331a7ad1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d123093-cd94-431f-82fd-f97b001c2559",
            "compositeImage": {
                "id": "c6e431c5-b771-4324-84a9-ccdfd5065aab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1ac139a-c677-4daf-8a9e-d3f331a7ad1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91197078-c0cb-4586-84ef-aa0c7f492958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1ac139a-c677-4daf-8a9e-d3f331a7ad1c",
                    "LayerId": "02044f8a-d571-4814-99c3-4e4e93ed99ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "02044f8a-d571-4814-99c3-4e4e93ed99ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d123093-cd94-431f-82fd-f97b001c2559",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}