{
    "id": "ac0583b9-ddf7-40a9-bbab-379ecdb89d32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbd43a43-f3bc-48f6-939c-89f70a146583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac0583b9-ddf7-40a9-bbab-379ecdb89d32",
            "compositeImage": {
                "id": "ee384ca2-fe81-425f-94e3-09db6027e83b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbd43a43-f3bc-48f6-939c-89f70a146583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be7374a1-f5ca-497c-8dc1-e1a55fc3b594",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbd43a43-f3bc-48f6-939c-89f70a146583",
                    "LayerId": "2d82a92e-dd15-442a-83c3-fa66d74153e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2d82a92e-dd15-442a-83c3-fa66d74153e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac0583b9-ddf7-40a9-bbab-379ecdb89d32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}