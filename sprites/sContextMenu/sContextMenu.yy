{
    "id": "6d1be065-723e-47c3-a7db-08d08b2c2193",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sContextMenu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91eb76d2-992a-4ecd-b209-198acc1032dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d1be065-723e-47c3-a7db-08d08b2c2193",
            "compositeImage": {
                "id": "f84d24ff-90dd-4262-b7bd-3adecc4b001c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91eb76d2-992a-4ecd-b209-198acc1032dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "877f7317-6b97-4ced-be77-3f3bfcadcda1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91eb76d2-992a-4ecd-b209-198acc1032dd",
                    "LayerId": "a92b0ce0-35ac-4679-bc65-48bbaf6c033e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "a92b0ce0-35ac-4679-bc65-48bbaf6c033e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d1be065-723e-47c3-a7db-08d08b2c2193",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 0,
    "yorig": 0
}