{
    "id": "bf62d98d-3bdb-41d1-881a-83a0342701c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDemonPoof",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1510756f-681f-41c7-bd8c-542a60924a22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf62d98d-3bdb-41d1-881a-83a0342701c7",
            "compositeImage": {
                "id": "405cdd1d-6a83-4cf3-b9b9-70d7255c37ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1510756f-681f-41c7-bd8c-542a60924a22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1041e41b-9862-4eba-8c61-19ca7d932152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1510756f-681f-41c7-bd8c-542a60924a22",
                    "LayerId": "2a8fad80-7f7a-4bb6-92b5-775bf6e12a79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "2a8fad80-7f7a-4bb6-92b5-775bf6e12a79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf62d98d-3bdb-41d1-881a-83a0342701c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}