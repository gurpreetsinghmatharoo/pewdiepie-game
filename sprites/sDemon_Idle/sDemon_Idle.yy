{
    "id": "92c4c860-632e-4d45-ae1d-8e826bdbd6ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDemon_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31d47716-5cc9-40b2-b15f-9e1da9d49ab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92c4c860-632e-4d45-ae1d-8e826bdbd6ad",
            "compositeImage": {
                "id": "28637f17-61eb-48ad-af41-6dc43c8774ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31d47716-5cc9-40b2-b15f-9e1da9d49ab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb724d56-d40e-4961-b7fe-88862d0d30e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31d47716-5cc9-40b2-b15f-9e1da9d49ab5",
                    "LayerId": "59e48de1-b8cc-446a-a912-27c39a2950ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "59e48de1-b8cc-446a-a912-27c39a2950ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92c4c860-632e-4d45-ae1d-8e826bdbd6ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}