{
    "id": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDemon_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30c19c09-cb92-4469-b873-2cec5285997c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
            "compositeImage": {
                "id": "eed2eb05-3f25-4e44-9cda-ef682390b309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30c19c09-cb92-4469-b873-2cec5285997c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b91c5d54-3953-444f-8100-e50860fc7830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30c19c09-cb92-4469-b873-2cec5285997c",
                    "LayerId": "94a75b86-dfef-4f88-8188-ca0f53018126"
                }
            ]
        },
        {
            "id": "b5edf718-a310-44a6-9dd9-94346118b3e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
            "compositeImage": {
                "id": "70d422ee-f57a-4e6a-b80a-b6711adea360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5edf718-a310-44a6-9dd9-94346118b3e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cd68cfc-b245-4ecd-b006-37394e705971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5edf718-a310-44a6-9dd9-94346118b3e4",
                    "LayerId": "94a75b86-dfef-4f88-8188-ca0f53018126"
                }
            ]
        },
        {
            "id": "e5727b86-b6f4-459b-9b42-5066642dd44c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
            "compositeImage": {
                "id": "343d779d-3bbc-45ca-b92e-f1ebd53d5b18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5727b86-b6f4-459b-9b42-5066642dd44c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a525b4ee-6052-4fb1-8d4b-e863a4779612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5727b86-b6f4-459b-9b42-5066642dd44c",
                    "LayerId": "94a75b86-dfef-4f88-8188-ca0f53018126"
                }
            ]
        },
        {
            "id": "290322a3-c76f-4285-9e97-c6e8f5912a4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
            "compositeImage": {
                "id": "d0b22d0d-02ac-4d5a-91e7-339331a733a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "290322a3-c76f-4285-9e97-c6e8f5912a4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc873279-76a3-4382-8290-fbae29e308fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "290322a3-c76f-4285-9e97-c6e8f5912a4f",
                    "LayerId": "94a75b86-dfef-4f88-8188-ca0f53018126"
                }
            ]
        },
        {
            "id": "4ba36c8b-0423-441f-a57f-8c8a8fadc86f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
            "compositeImage": {
                "id": "12b0149a-c3be-4307-8de3-ba5ba1a6e0b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ba36c8b-0423-441f-a57f-8c8a8fadc86f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6ecef8a-9a93-4a51-8328-0bc95ca1f980",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ba36c8b-0423-441f-a57f-8c8a8fadc86f",
                    "LayerId": "94a75b86-dfef-4f88-8188-ca0f53018126"
                }
            ]
        },
        {
            "id": "e8e674ad-8184-45f5-8a7d-ce1959187a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
            "compositeImage": {
                "id": "a8e5cd51-1cfb-4424-96d2-a42d74cd45dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8e674ad-8184-45f5-8a7d-ce1959187a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60d47ac0-7c76-4d2a-bc3f-27167484be0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8e674ad-8184-45f5-8a7d-ce1959187a05",
                    "LayerId": "94a75b86-dfef-4f88-8188-ca0f53018126"
                }
            ]
        },
        {
            "id": "5e75df6b-6747-4ead-a2e0-7b3aa22ab374",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
            "compositeImage": {
                "id": "abdfc660-f112-42eb-9938-937963271260",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e75df6b-6747-4ead-a2e0-7b3aa22ab374",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8141807d-f054-420b-b828-dcb48911c3e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e75df6b-6747-4ead-a2e0-7b3aa22ab374",
                    "LayerId": "94a75b86-dfef-4f88-8188-ca0f53018126"
                }
            ]
        },
        {
            "id": "6dbff760-3663-4b93-bb50-2b61a1f69c2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
            "compositeImage": {
                "id": "a153e61f-ab83-4077-b905-cf9cb992165d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dbff760-3663-4b93-bb50-2b61a1f69c2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38f5d653-caa1-4391-aca4-fd31b9231ebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dbff760-3663-4b93-bb50-2b61a1f69c2f",
                    "LayerId": "94a75b86-dfef-4f88-8188-ca0f53018126"
                }
            ]
        },
        {
            "id": "b3707152-13ba-4240-a090-3175d657e934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
            "compositeImage": {
                "id": "01064f25-fd6e-466a-be65-d3614cec22b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3707152-13ba-4240-a090-3175d657e934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4e95006-9b29-461c-b3a6-0d520982f1ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3707152-13ba-4240-a090-3175d657e934",
                    "LayerId": "94a75b86-dfef-4f88-8188-ca0f53018126"
                }
            ]
        },
        {
            "id": "b14fb15a-368a-467f-bbbd-d44fd8b45512",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
            "compositeImage": {
                "id": "6e535d18-3296-4e75-895d-c4068504c9f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b14fb15a-368a-467f-bbbd-d44fd8b45512",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c125f17-f403-4389-b275-eae7586870a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b14fb15a-368a-467f-bbbd-d44fd8b45512",
                    "LayerId": "94a75b86-dfef-4f88-8188-ca0f53018126"
                }
            ]
        },
        {
            "id": "8aaf566b-e6d7-4272-bc59-1a87a9b1fedc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
            "compositeImage": {
                "id": "02bb08c3-1668-4675-8d78-c26b54363d4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aaf566b-e6d7-4272-bc59-1a87a9b1fedc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa9b4487-3bcf-43a2-adfd-889a05d84b8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aaf566b-e6d7-4272-bc59-1a87a9b1fedc",
                    "LayerId": "94a75b86-dfef-4f88-8188-ca0f53018126"
                }
            ]
        },
        {
            "id": "1b59ff60-a00b-4278-bbb0-d7620a6cbd62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
            "compositeImage": {
                "id": "b8703482-59b5-4336-8aae-0c97a8921037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b59ff60-a00b-4278-bbb0-d7620a6cbd62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7067d047-14a9-410b-b09f-54d54d09340b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b59ff60-a00b-4278-bbb0-d7620a6cbd62",
                    "LayerId": "94a75b86-dfef-4f88-8188-ca0f53018126"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "94a75b86-dfef-4f88-8188-ca0f53018126",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e886cf18-b4de-423d-9c08-62aa3a75fe0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}