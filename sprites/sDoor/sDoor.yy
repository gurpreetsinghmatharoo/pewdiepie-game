{
    "id": "003cc678-389e-4eb4-b3a4-0ba20a5ff993",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0b76d17-d235-45e7-aac2-ce56bcc0bbd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "003cc678-389e-4eb4-b3a4-0ba20a5ff993",
            "compositeImage": {
                "id": "b896a1ca-bd9d-4b06-851a-1e9fa831ce70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0b76d17-d235-45e7-aac2-ce56bcc0bbd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f26ade2b-7d8c-4955-9dda-3262f2cf861f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0b76d17-d235-45e7-aac2-ce56bcc0bbd6",
                    "LayerId": "24fd93d0-2719-4786-b20c-f78bc9df5034"
                }
            ]
        },
        {
            "id": "576a21ac-783d-4f3a-82a9-4802701892d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "003cc678-389e-4eb4-b3a4-0ba20a5ff993",
            "compositeImage": {
                "id": "cba588ab-239a-4ff7-862e-1e5949bf66b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "576a21ac-783d-4f3a-82a9-4802701892d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d62939d-ac7e-4724-90dd-a4c6cbe33864",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "576a21ac-783d-4f3a-82a9-4802701892d2",
                    "LayerId": "24fd93d0-2719-4786-b20c-f78bc9df5034"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "24fd93d0-2719-4786-b20c-f78bc9df5034",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "003cc678-389e-4eb4-b3a4-0ba20a5ff993",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}