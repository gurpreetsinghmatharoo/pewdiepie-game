{
    "id": "b4f8d19f-dbb0-441c-9367-e8b167f92c8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoorBig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 16,
    "bbox_right": 47,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2370881-1461-48db-ae60-354945e0da5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4f8d19f-dbb0-441c-9367-e8b167f92c8a",
            "compositeImage": {
                "id": "1f41f124-210f-4b57-9390-a4dd4246b684",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2370881-1461-48db-ae60-354945e0da5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b3a1824-077e-4dca-9a7f-8aaeb0cf3674",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2370881-1461-48db-ae60-354945e0da5e",
                    "LayerId": "83230289-a1a6-462b-a72e-2a0e6085bb35"
                }
            ]
        },
        {
            "id": "08d86c6e-eebd-4ad0-b125-6a4c19743231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4f8d19f-dbb0-441c-9367-e8b167f92c8a",
            "compositeImage": {
                "id": "ee2fbb15-1a49-43db-bb2b-f4db97cdc743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08d86c6e-eebd-4ad0-b125-6a4c19743231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e9a8848-fce7-4284-8a5d-963d7d47d813",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08d86c6e-eebd-4ad0-b125-6a4c19743231",
                    "LayerId": "83230289-a1a6-462b-a72e-2a0e6085bb35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "83230289-a1a6-462b-a72e-2a0e6085bb35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4f8d19f-dbb0-441c-9367-e8b167f92c8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}