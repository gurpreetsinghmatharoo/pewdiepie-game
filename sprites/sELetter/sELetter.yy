{
    "id": "6a2e3165-2ae7-4611-8745-5896a2dee010",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sELetter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 2,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a7c6c7c-3cba-4abe-ae83-59137a256247",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a2e3165-2ae7-4611-8745-5896a2dee010",
            "compositeImage": {
                "id": "dccbc77f-b632-4801-81c5-d26313866538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a7c6c7c-3cba-4abe-ae83-59137a256247",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f05ef5f6-2560-4a78-bc2e-f7232685d5e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a7c6c7c-3cba-4abe-ae83-59137a256247",
                    "LayerId": "2194edde-aac5-4268-a010-e21566ab7b12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "2194edde-aac5-4268-a010-e21566ab7b12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a2e3165-2ae7-4611-8745-5896a2dee010",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}