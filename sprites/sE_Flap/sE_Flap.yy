{
    "id": "6afd8cf7-9cc1-4bbb-b2e1-3f2daed2bc8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sE_Flap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 5,
    "bbox_right": 25,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cb140f8-d98f-49c4-b83c-68d6515e4ed3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6afd8cf7-9cc1-4bbb-b2e1-3f2daed2bc8c",
            "compositeImage": {
                "id": "1ad5065f-631d-4fc5-8a28-47734a90fc5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cb140f8-d98f-49c4-b83c-68d6515e4ed3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c8aa2fa-2c15-4f30-bffc-dea801b45e96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cb140f8-d98f-49c4-b83c-68d6515e4ed3",
                    "LayerId": "83233b42-51b8-48b3-b2a9-5c2ac26d454b"
                }
            ]
        },
        {
            "id": "ce9cee7d-b66e-4f14-ba1b-40b2ae3b6f11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6afd8cf7-9cc1-4bbb-b2e1-3f2daed2bc8c",
            "compositeImage": {
                "id": "d8afa81b-3c64-49f1-8faf-51a80258d46b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce9cee7d-b66e-4f14-ba1b-40b2ae3b6f11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c56d166a-a400-48d3-b619-0548a12e6a72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce9cee7d-b66e-4f14-ba1b-40b2ae3b6f11",
                    "LayerId": "83233b42-51b8-48b3-b2a9-5c2ac26d454b"
                }
            ]
        },
        {
            "id": "f723fe6d-b88d-4e49-bafa-b0ba36ff484c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6afd8cf7-9cc1-4bbb-b2e1-3f2daed2bc8c",
            "compositeImage": {
                "id": "8026d7eb-1c1f-42f3-bdda-172715287a66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f723fe6d-b88d-4e49-bafa-b0ba36ff484c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2b189ef-888b-40da-a4f2-e84694631434",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f723fe6d-b88d-4e49-bafa-b0ba36ff484c",
                    "LayerId": "83233b42-51b8-48b3-b2a9-5c2ac26d454b"
                }
            ]
        },
        {
            "id": "d5ffa546-1995-47aa-9b51-e33c52f9c9cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6afd8cf7-9cc1-4bbb-b2e1-3f2daed2bc8c",
            "compositeImage": {
                "id": "7d858825-867e-4a1f-9b0d-dc355b9cb9c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5ffa546-1995-47aa-9b51-e33c52f9c9cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce2f210-ee3d-493b-9d35-01bad7c65a33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5ffa546-1995-47aa-9b51-e33c52f9c9cf",
                    "LayerId": "83233b42-51b8-48b3-b2a9-5c2ac26d454b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "83233b42-51b8-48b3-b2a9-5c2ac26d454b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6afd8cf7-9cc1-4bbb-b2e1-3f2daed2bc8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}