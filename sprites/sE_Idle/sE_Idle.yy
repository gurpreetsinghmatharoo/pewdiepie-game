{
    "id": "835badd2-3333-4e06-8450-7f567317a0e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sE_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 8,
    "bbox_right": 21,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fef7d37f-672e-4941-b26c-378fe8517131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "835badd2-3333-4e06-8450-7f567317a0e5",
            "compositeImage": {
                "id": "af2fd148-85dc-4a71-9d50-39d03f9907b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fef7d37f-672e-4941-b26c-378fe8517131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a699602-93ab-4fb2-b03d-3540c2ec71e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fef7d37f-672e-4941-b26c-378fe8517131",
                    "LayerId": "fc7cafe0-ef32-4042-84d0-bb4dd332c476"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fc7cafe0-ef32-4042-84d0-bb4dd332c476",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "835badd2-3333-4e06-8450-7f567317a0e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}