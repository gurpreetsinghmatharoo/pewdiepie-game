{
    "id": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEdgar_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 4,
    "bbox_right": 20,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3bfc120c-ef55-4155-b599-253c89cac5cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "e74e0c30-96e2-44d6-953c-2184171f19bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bfc120c-ef55-4155-b599-253c89cac5cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6bcc2e6-f964-4057-a01e-689fadcf1b62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bfc120c-ef55-4155-b599-253c89cac5cc",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "a8ad3fb8-f77d-454f-8b5b-640f10649935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "f0a98795-4e71-4137-8136-99eb97fe93b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8ad3fb8-f77d-454f-8b5b-640f10649935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27e92989-90f1-4e86-b0b9-1897040d9a2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8ad3fb8-f77d-454f-8b5b-640f10649935",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "98b09954-e840-4ae6-8a94-f345776d8d26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "d7493bd3-6b18-444d-9607-eed794964882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98b09954-e840-4ae6-8a94-f345776d8d26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e26896e3-d6cb-4259-a517-863885f92e1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98b09954-e840-4ae6-8a94-f345776d8d26",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "3e590d58-1750-44d6-9688-da905f281faa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "7cfeb5b2-b16f-4b48-9b3f-fc957c731258",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e590d58-1750-44d6-9688-da905f281faa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b391e58-6ea8-468b-9693-beeb9ed502b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e590d58-1750-44d6-9688-da905f281faa",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "c018e668-cebb-4ba2-b0df-3082b77f753b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "4ff38227-65c2-47da-8376-6cc52acc8aa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c018e668-cebb-4ba2-b0df-3082b77f753b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a105ecdd-163f-4ad2-b4f4-cf2d05d8c48f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c018e668-cebb-4ba2-b0df-3082b77f753b",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "a730b1f7-6289-4ebe-b6a4-f58fdc245ab2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "a3212954-0d38-473e-8ff1-932daa447cfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a730b1f7-6289-4ebe-b6a4-f58fdc245ab2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6231c4d-5311-4cbd-a314-47fdded3e073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a730b1f7-6289-4ebe-b6a4-f58fdc245ab2",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "6519ffd3-08b1-44c0-922d-faf143f0ef31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "bad62346-7321-47ad-886d-4259b9744a1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6519ffd3-08b1-44c0-922d-faf143f0ef31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbcfb310-05ad-476d-ace3-1e9ec7e913a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6519ffd3-08b1-44c0-922d-faf143f0ef31",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "2cedcbc4-f94c-4326-adab-9053b877e956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "563d0c38-5069-481e-b55d-19c023bf2ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cedcbc4-f94c-4326-adab-9053b877e956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e8e6ef9-676f-4ecf-9c7c-4a687b6d20b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cedcbc4-f94c-4326-adab-9053b877e956",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "c08a4fd7-6aca-4b35-85c9-c4310f4e80e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "8b357562-31a6-4be4-877b-c879a0e6c8f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c08a4fd7-6aca-4b35-85c9-c4310f4e80e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d376844b-38e9-4ce8-b38b-3c9d05a73ed7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c08a4fd7-6aca-4b35-85c9-c4310f4e80e5",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "232a0db4-dea9-4fbd-9a0f-17542a3a81df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "f96d5cc7-5977-47bc-94a8-fa092b987c5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "232a0db4-dea9-4fbd-9a0f-17542a3a81df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22d548ed-eb65-40e8-8d6e-5c7318a0c742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "232a0db4-dea9-4fbd-9a0f-17542a3a81df",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "ee7b3589-4b19-441f-958e-6363d6c0e43a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "caae56c4-7fd6-4a00-8a2d-c78a109c139d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee7b3589-4b19-441f-958e-6363d6c0e43a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a57502c0-1bb6-4495-8ec1-18ee89a8e956",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee7b3589-4b19-441f-958e-6363d6c0e43a",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "e5a5f1f0-627f-48e9-84f0-1fc42bc5362d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "f0ba77ba-9088-4989-9071-7112d047085e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5a5f1f0-627f-48e9-84f0-1fc42bc5362d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "710cf506-5d51-4800-92ab-3ef6c56b44e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5a5f1f0-627f-48e9-84f0-1fc42bc5362d",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "054e25c0-9bf5-479c-a931-df5680800463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "4bd1a880-4a24-42e9-aa93-13dc0b622191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "054e25c0-9bf5-479c-a931-df5680800463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b4c3fdc-255d-4b2b-b565-8b049b01a8c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "054e25c0-9bf5-479c-a931-df5680800463",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "ebf0d303-4bd5-4a63-bee1-e8f04a78c530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "1bbb756f-e857-457c-9104-8b0ad348c103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebf0d303-4bd5-4a63-bee1-e8f04a78c530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce3c226-059c-4c5b-861a-d5225efa1426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebf0d303-4bd5-4a63-bee1-e8f04a78c530",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "a2acec94-33bd-4faa-849e-ba9da76aadf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "2210c0af-fbd5-44f5-9fe9-8509f6c2edda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2acec94-33bd-4faa-849e-ba9da76aadf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "985e58c0-3fb8-4bca-b36d-a30899a46a18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2acec94-33bd-4faa-849e-ba9da76aadf0",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        },
        {
            "id": "d6bddc90-32f3-4a77-880c-be6736d48033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "compositeImage": {
                "id": "72e08742-5f0e-4d49-9942-47fe637eeb31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6bddc90-32f3-4a77-880c-be6736d48033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "098ca918-e834-4631-8b43-ee5b1ce612c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6bddc90-32f3-4a77-880c-be6736d48033",
                    "LayerId": "987a2ebb-41ac-400d-be49-990d07d4dbca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "987a2ebb-41ac-400d-be49-990d07d4dbca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e653041d-a70b-4c7a-9d7c-6a18228e8250",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}