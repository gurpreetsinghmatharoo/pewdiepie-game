{
    "id": "6553693d-83a3-47ad-9ed8-3132e8839009",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEdgar_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 20,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00fef481-de00-40e1-aaec-ff782420ad77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553693d-83a3-47ad-9ed8-3132e8839009",
            "compositeImage": {
                "id": "309dc2ca-ed45-4acd-9d6a-21a0f37193de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00fef481-de00-40e1-aaec-ff782420ad77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "688b27d6-beb4-43b8-b69f-eae521d3f5ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00fef481-de00-40e1-aaec-ff782420ad77",
                    "LayerId": "6034506f-5c2e-47fc-8a18-910d82b8a4f5"
                }
            ]
        },
        {
            "id": "ae71a535-ca44-40f7-8181-71343f84fec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553693d-83a3-47ad-9ed8-3132e8839009",
            "compositeImage": {
                "id": "6ccb6383-395e-4ae1-b1c8-7119db966841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae71a535-ca44-40f7-8181-71343f84fec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23fddeb8-ca19-437e-bdf6-621ce91051bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae71a535-ca44-40f7-8181-71343f84fec6",
                    "LayerId": "6034506f-5c2e-47fc-8a18-910d82b8a4f5"
                }
            ]
        },
        {
            "id": "8923025c-48f6-4baa-b64c-26f65186d6ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553693d-83a3-47ad-9ed8-3132e8839009",
            "compositeImage": {
                "id": "67fca1e6-c5d0-40ee-aae7-36fb8508717e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8923025c-48f6-4baa-b64c-26f65186d6ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7a5b46f-17ae-4e4d-99e2-9600cd7dae42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8923025c-48f6-4baa-b64c-26f65186d6ab",
                    "LayerId": "6034506f-5c2e-47fc-8a18-910d82b8a4f5"
                }
            ]
        },
        {
            "id": "af85bdfa-1728-4f2d-80d9-3ca9fe4de6ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553693d-83a3-47ad-9ed8-3132e8839009",
            "compositeImage": {
                "id": "e0871193-d954-460c-933b-852edd2f66b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af85bdfa-1728-4f2d-80d9-3ca9fe4de6ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9a866d0-c4f2-4124-b71d-544ff37aa4b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af85bdfa-1728-4f2d-80d9-3ca9fe4de6ca",
                    "LayerId": "6034506f-5c2e-47fc-8a18-910d82b8a4f5"
                }
            ]
        },
        {
            "id": "bcbd3b27-c40b-4dc9-8892-5ed285754d11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553693d-83a3-47ad-9ed8-3132e8839009",
            "compositeImage": {
                "id": "a8ef0f6c-4b07-46c5-b9cf-45044aae14ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcbd3b27-c40b-4dc9-8892-5ed285754d11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8af90b78-31ad-4b24-bd65-b757a8316260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcbd3b27-c40b-4dc9-8892-5ed285754d11",
                    "LayerId": "6034506f-5c2e-47fc-8a18-910d82b8a4f5"
                }
            ]
        },
        {
            "id": "58f3b530-dafd-4623-b172-58fe22968dd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553693d-83a3-47ad-9ed8-3132e8839009",
            "compositeImage": {
                "id": "718bec37-8988-422c-912c-7d849ed59b0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58f3b530-dafd-4623-b172-58fe22968dd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0610f2c2-2ac3-4205-b812-18177f611ae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58f3b530-dafd-4623-b172-58fe22968dd0",
                    "LayerId": "6034506f-5c2e-47fc-8a18-910d82b8a4f5"
                }
            ]
        },
        {
            "id": "448bc4f9-729f-44e0-8af0-664e2b7bf74a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553693d-83a3-47ad-9ed8-3132e8839009",
            "compositeImage": {
                "id": "b9401d23-78e4-4ae2-8cb6-4e0bd4810197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "448bc4f9-729f-44e0-8af0-664e2b7bf74a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8484ad0-63e5-4b7c-8a1b-320db67f834b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "448bc4f9-729f-44e0-8af0-664e2b7bf74a",
                    "LayerId": "6034506f-5c2e-47fc-8a18-910d82b8a4f5"
                }
            ]
        },
        {
            "id": "275709ee-d73a-48e8-b26e-22cacc8c7f82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6553693d-83a3-47ad-9ed8-3132e8839009",
            "compositeImage": {
                "id": "5aa47a5a-e12f-477b-be11-a66b1fdbdc00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "275709ee-d73a-48e8-b26e-22cacc8c7f82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af22fd08-7ce6-43ef-a505-72403be63feb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "275709ee-d73a-48e8-b26e-22cacc8c7f82",
                    "LayerId": "6034506f-5c2e-47fc-8a18-910d82b8a4f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "6034506f-5c2e-47fc-8a18-910d82b8a4f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6553693d-83a3-47ad-9ed8-3132e8839009",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}