{
    "id": "e54eb1a3-9c5d-47f0-a299-e0876da9bda9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEdgar_Pickup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 4,
    "bbox_right": 20,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34084c80-b9f0-4954-9f16-e96a492d9d11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e54eb1a3-9c5d-47f0-a299-e0876da9bda9",
            "compositeImage": {
                "id": "1d38afaf-738f-4ac7-a9f5-2e088ac04a5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34084c80-b9f0-4954-9f16-e96a492d9d11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32bf8cfd-4e59-4400-89cf-bcb2004487ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34084c80-b9f0-4954-9f16-e96a492d9d11",
                    "LayerId": "3c71366e-d1d5-47fd-abf7-8a74832eeec1"
                }
            ]
        },
        {
            "id": "69fbd42b-9ee9-4b90-9287-56453fb92b0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e54eb1a3-9c5d-47f0-a299-e0876da9bda9",
            "compositeImage": {
                "id": "78c878bd-abb9-4f2a-9ebe-58cc5c5e51ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69fbd42b-9ee9-4b90-9287-56453fb92b0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46f0f56f-0d27-473a-b014-b288ed246e7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69fbd42b-9ee9-4b90-9287-56453fb92b0f",
                    "LayerId": "3c71366e-d1d5-47fd-abf7-8a74832eeec1"
                }
            ]
        },
        {
            "id": "b4ebfabb-3147-45cb-a883-0030f342b88b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e54eb1a3-9c5d-47f0-a299-e0876da9bda9",
            "compositeImage": {
                "id": "08227e44-b929-4b09-a6b6-4300b06ec644",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4ebfabb-3147-45cb-a883-0030f342b88b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84be0585-ca53-40b8-8438-eb8d592fedfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4ebfabb-3147-45cb-a883-0030f342b88b",
                    "LayerId": "3c71366e-d1d5-47fd-abf7-8a74832eeec1"
                }
            ]
        },
        {
            "id": "e913ae22-f766-445c-87ce-cf47d0017518",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e54eb1a3-9c5d-47f0-a299-e0876da9bda9",
            "compositeImage": {
                "id": "41f5e8d0-06c7-4eab-baa5-0f5b9c14bd63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e913ae22-f766-445c-87ce-cf47d0017518",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df087e7d-969e-4ff4-ad5b-75f6710044b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e913ae22-f766-445c-87ce-cf47d0017518",
                    "LayerId": "3c71366e-d1d5-47fd-abf7-8a74832eeec1"
                }
            ]
        },
        {
            "id": "4609354f-275b-45ad-adc2-e4057363e464",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e54eb1a3-9c5d-47f0-a299-e0876da9bda9",
            "compositeImage": {
                "id": "d54842ca-ad20-4cf5-a209-81a5ac8b6fa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4609354f-275b-45ad-adc2-e4057363e464",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8217e446-9c1a-479c-bc15-63ce6af98e84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4609354f-275b-45ad-adc2-e4057363e464",
                    "LayerId": "3c71366e-d1d5-47fd-abf7-8a74832eeec1"
                }
            ]
        },
        {
            "id": "eb22ac34-0eba-4259-944e-db2c93e462b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e54eb1a3-9c5d-47f0-a299-e0876da9bda9",
            "compositeImage": {
                "id": "7145d54c-72db-432f-8165-ab24963d9e98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb22ac34-0eba-4259-944e-db2c93e462b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13439692-9714-4d56-a106-6f1c17e699ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb22ac34-0eba-4259-944e-db2c93e462b7",
                    "LayerId": "3c71366e-d1d5-47fd-abf7-8a74832eeec1"
                }
            ]
        },
        {
            "id": "4d426ff1-0221-463e-920b-9463d2fef5e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e54eb1a3-9c5d-47f0-a299-e0876da9bda9",
            "compositeImage": {
                "id": "8b87a00b-d076-4fde-b06d-b8d0527eb51e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d426ff1-0221-463e-920b-9463d2fef5e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4efe5d85-9fe5-4811-8f3a-50613936d8c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d426ff1-0221-463e-920b-9463d2fef5e4",
                    "LayerId": "3c71366e-d1d5-47fd-abf7-8a74832eeec1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "3c71366e-d1d5-47fd-abf7-8a74832eeec1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e54eb1a3-9c5d-47f0-a299-e0876da9bda9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}