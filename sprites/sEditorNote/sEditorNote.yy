{
    "id": "4d9262cd-64be-44f7-902f-6ba6d74f3ffd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEditorNote",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 8,
    "bbox_right": 56,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "782db390-6b64-4824-8c86-690d3734b967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d9262cd-64be-44f7-902f-6ba6d74f3ffd",
            "compositeImage": {
                "id": "417945fb-2c4a-4526-bf5d-e52df6bb8558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "782db390-6b64-4824-8c86-690d3734b967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0d9359c-9d31-4391-974d-f7dd206a0b57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "782db390-6b64-4824-8c86-690d3734b967",
                    "LayerId": "c90c4cd8-edc9-4874-9aea-af9fc407b364"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c90c4cd8-edc9-4874-9aea-af9fc407b364",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d9262cd-64be-44f7-902f-6ba6d74f3ffd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}