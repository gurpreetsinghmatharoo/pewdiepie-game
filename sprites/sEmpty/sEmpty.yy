{
    "id": "dead01ef-0c4a-446f-ba42-588d0705a949",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEmpty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e5b4d52-d845-4741-a288-5b830a5b4dc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dead01ef-0c4a-446f-ba42-588d0705a949",
            "compositeImage": {
                "id": "5f0c8e6e-9f91-4051-a65a-e556f064ab55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e5b4d52-d845-4741-a288-5b830a5b4dc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d15064eb-f69f-4a62-9a6b-c81f0f4f4a9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e5b4d52-d845-4741-a288-5b830a5b4dc2",
                    "LayerId": "6296be82-0b9c-49d1-bc57-29a4f9202d18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6296be82-0b9c-49d1-bc57-29a4f9202d18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dead01ef-0c4a-446f-ba42-588d0705a949",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}