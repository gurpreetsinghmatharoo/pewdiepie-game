{
    "id": "a20cbc97-987a-404f-a82c-4fb416222972",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlexPart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3deb092-736c-4772-b323-48e8130ac246",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a20cbc97-987a-404f-a82c-4fb416222972",
            "compositeImage": {
                "id": "475b3994-7ffb-4988-831a-5949ef291358",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3deb092-736c-4772-b323-48e8130ac246",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe907de5-13e0-4643-aed5-11c97474ae63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3deb092-736c-4772-b323-48e8130ac246",
                    "LayerId": "ddb74110-1acc-44ff-9bfe-819ca4790162"
                }
            ]
        },
        {
            "id": "ce16f649-95bb-4a48-b02c-63d2cb1b3557",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a20cbc97-987a-404f-a82c-4fb416222972",
            "compositeImage": {
                "id": "fe35d9e9-07a6-414b-bb3e-90afb8f35a75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce16f649-95bb-4a48-b02c-63d2cb1b3557",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bcc5864-3445-4aff-a8b8-30bdc20d793f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce16f649-95bb-4a48-b02c-63d2cb1b3557",
                    "LayerId": "ddb74110-1acc-44ff-9bfe-819ca4790162"
                }
            ]
        },
        {
            "id": "51e9402c-3da7-4a36-8399-09d1fd7f7987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a20cbc97-987a-404f-a82c-4fb416222972",
            "compositeImage": {
                "id": "f5c963b1-9fbb-451f-9a27-d1bf0958ed8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51e9402c-3da7-4a36-8399-09d1fd7f7987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dff3cc2-683e-46de-bba5-349ecedc0f19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51e9402c-3da7-4a36-8399-09d1fd7f7987",
                    "LayerId": "ddb74110-1acc-44ff-9bfe-819ca4790162"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ddb74110-1acc-44ff-9bfe-819ca4790162",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a20cbc97-987a-404f-a82c-4fb416222972",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}