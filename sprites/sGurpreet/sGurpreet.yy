{
    "id": "8a50779b-4d52-42fb-a5dd-914e3599b3d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGurpreet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 20,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98b40280-4a0d-412a-835d-7d981d3452c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a50779b-4d52-42fb-a5dd-914e3599b3d3",
            "compositeImage": {
                "id": "fb9ad899-d2e5-4415-aa95-5fa2023f7084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98b40280-4a0d-412a-835d-7d981d3452c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a935c8a-ed52-4a9f-9202-d9d2143613de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98b40280-4a0d-412a-835d-7d981d3452c7",
                    "LayerId": "377fa2ab-f988-4352-bf82-7968ec6ed717"
                }
            ]
        },
        {
            "id": "080a306c-92cd-47a7-938c-f22d9f751357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a50779b-4d52-42fb-a5dd-914e3599b3d3",
            "compositeImage": {
                "id": "a8f9a241-a0c0-4388-a6fe-100c752dec31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "080a306c-92cd-47a7-938c-f22d9f751357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de0ed887-d36f-48bc-9078-dd462ca7a13c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "080a306c-92cd-47a7-938c-f22d9f751357",
                    "LayerId": "377fa2ab-f988-4352-bf82-7968ec6ed717"
                }
            ]
        },
        {
            "id": "b65242bc-951e-4669-867a-bead6ba0c03a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a50779b-4d52-42fb-a5dd-914e3599b3d3",
            "compositeImage": {
                "id": "5911f88d-c784-460e-bb05-470af4639d84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b65242bc-951e-4669-867a-bead6ba0c03a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9edc67c-7fa0-452e-b282-001dfea6407c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b65242bc-951e-4669-867a-bead6ba0c03a",
                    "LayerId": "377fa2ab-f988-4352-bf82-7968ec6ed717"
                }
            ]
        },
        {
            "id": "5ccbff84-980e-43c9-9360-7ee09142de92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a50779b-4d52-42fb-a5dd-914e3599b3d3",
            "compositeImage": {
                "id": "e8916543-354b-439b-bb55-7078b0b83a87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ccbff84-980e-43c9-9360-7ee09142de92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba742ffa-9f14-4766-b15e-fbc504041998",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ccbff84-980e-43c9-9360-7ee09142de92",
                    "LayerId": "377fa2ab-f988-4352-bf82-7968ec6ed717"
                }
            ]
        },
        {
            "id": "f1a48516-99dc-40f6-9183-237c0fe73c5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a50779b-4d52-42fb-a5dd-914e3599b3d3",
            "compositeImage": {
                "id": "1e7bf29a-2c01-4329-ba7f-fbc3e596c53c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1a48516-99dc-40f6-9183-237c0fe73c5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3b07cf1-e1a9-41e2-a9f8-4171c017bda8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1a48516-99dc-40f6-9183-237c0fe73c5b",
                    "LayerId": "377fa2ab-f988-4352-bf82-7968ec6ed717"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "377fa2ab-f988-4352-bf82-7968ec6ed717",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a50779b-4d52-42fb-a5dd-914e3599b3d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}