{
    "id": "064bc1b9-afe2-4cef-9dc3-0c59bb9c2db3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHealthbar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55007256-ee9e-4727-9baa-2dcdc8bd38c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "064bc1b9-afe2-4cef-9dc3-0c59bb9c2db3",
            "compositeImage": {
                "id": "9cf659d5-c0d7-44f5-acb9-92142fa72be0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55007256-ee9e-4727-9baa-2dcdc8bd38c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a2c554f-3eb1-4756-9f26-38191f8a0032",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55007256-ee9e-4727-9baa-2dcdc8bd38c7",
                    "LayerId": "74342c72-0861-4c08-bd62-a410971c708e"
                }
            ]
        },
        {
            "id": "8a51a918-8519-4c07-852c-25cafa4c1a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "064bc1b9-afe2-4cef-9dc3-0c59bb9c2db3",
            "compositeImage": {
                "id": "f133f462-f1dc-4fe5-b4b5-16da3de99f26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a51a918-8519-4c07-852c-25cafa4c1a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fdca175-2057-482d-91e2-deb4f785404a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a51a918-8519-4c07-852c-25cafa4c1a05",
                    "LayerId": "74342c72-0861-4c08-bd62-a410971c708e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "74342c72-0861-4c08-bd62-a410971c708e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "064bc1b9-afe2-4cef-9dc3-0c59bb9c2db3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}