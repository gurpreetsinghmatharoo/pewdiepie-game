{
    "id": "dbac63d0-ec8c-4a60-9d07-c6f5a5af239e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHouse1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 3,
    "bbox_right": 159,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ec47d62-cde6-4978-8b0f-78482b7a9e7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbac63d0-ec8c-4a60-9d07-c6f5a5af239e",
            "compositeImage": {
                "id": "2ba103d3-2941-45a8-9230-d0b5bd39cbe2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ec47d62-cde6-4978-8b0f-78482b7a9e7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76bf35d2-fc83-4a3c-b99a-6d0c2593f39c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ec47d62-cde6-4978-8b0f-78482b7a9e7a",
                    "LayerId": "34232969-b355-4e8d-90ea-0790e99784b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "34232969-b355-4e8d-90ea-0790e99784b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbac63d0-ec8c-4a60-9d07-c6f5a5af239e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}