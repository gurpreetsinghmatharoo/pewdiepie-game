{
    "id": "0dae48d5-53f1-41c0-9b20-385fa881ec38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHouse2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 3,
    "bbox_right": 159,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b090970c-c186-455f-9a9e-e92fa67cc675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dae48d5-53f1-41c0-9b20-385fa881ec38",
            "compositeImage": {
                "id": "efcfe439-08b8-47af-9db5-cf62a17e31e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b090970c-c186-455f-9a9e-e92fa67cc675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d2cfc20-cd0a-4e1c-baa9-ee9379602e1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b090970c-c186-455f-9a9e-e92fa67cc675",
                    "LayerId": "cd0a224e-7f99-449a-b344-b5ca88d60155"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "cd0a224e-7f99-449a-b344-b5ca88d60155",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0dae48d5-53f1-41c0-9b20-385fa881ec38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}