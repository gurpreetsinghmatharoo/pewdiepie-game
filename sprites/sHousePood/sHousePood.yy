{
    "id": "ff70da5d-16fe-4fc2-a9bc-cee81e0053ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHousePood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 3,
    "bbox_right": 159,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84b0ca27-9b11-4293-b239-5221543de025",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff70da5d-16fe-4fc2-a9bc-cee81e0053ca",
            "compositeImage": {
                "id": "cb50a0d7-8c9c-415d-95e5-93cb5b4f575d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84b0ca27-9b11-4293-b239-5221543de025",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfea210b-e637-46d9-bf05-6b2561b4bbd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b0ca27-9b11-4293-b239-5221543de025",
                    "LayerId": "144c38d6-a478-4d3a-833a-a3a39c5b90e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "144c38d6-a478-4d3a-833a-a3a39c5b90e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff70da5d-16fe-4fc2-a9bc-cee81e0053ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}