{
    "id": "811b732d-c8b1-4b34-8158-b2f31d58d473",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sItemButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f51bf380-9d6e-4902-8dd1-5bc34d258257",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "811b732d-c8b1-4b34-8158-b2f31d58d473",
            "compositeImage": {
                "id": "191090a3-36a1-48a6-a7b0-c4a91327fe04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f51bf380-9d6e-4902-8dd1-5bc34d258257",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77105838-8fa4-4b28-ae2a-4f8c5efcde25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f51bf380-9d6e-4902-8dd1-5bc34d258257",
                    "LayerId": "b6f194fd-6074-472d-b93a-ed382d67c50f"
                }
            ]
        },
        {
            "id": "e732ee73-c1bf-4fcb-8a3e-5e9dcc0fda67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "811b732d-c8b1-4b34-8158-b2f31d58d473",
            "compositeImage": {
                "id": "49c1beb1-be99-4c61-badf-f565cb43aed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e732ee73-c1bf-4fcb-8a3e-5e9dcc0fda67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e0a49d4-814b-488e-af38-e91201ff3958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e732ee73-c1bf-4fcb-8a3e-5e9dcc0fda67",
                    "LayerId": "b6f194fd-6074-472d-b93a-ed382d67c50f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "b6f194fd-6074-472d-b93a-ed382d67c50f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "811b732d-c8b1-4b34-8158-b2f31d58d473",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}