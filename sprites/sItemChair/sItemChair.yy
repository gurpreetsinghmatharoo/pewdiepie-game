{
    "id": "ccb184bb-de3a-44e1-a387-d68e1f4a47d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sItemChair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 4,
    "bbox_right": 29,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c212ca6c-b5d3-486b-abe0-33140c0bd7dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccb184bb-de3a-44e1-a387-d68e1f4a47d6",
            "compositeImage": {
                "id": "8de7bffa-c91d-4df3-bfbe-b05a0896d080",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c212ca6c-b5d3-486b-abe0-33140c0bd7dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54e64d87-1064-4ec7-a286-d681bd1f83e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c212ca6c-b5d3-486b-abe0-33140c0bd7dc",
                    "LayerId": "3d4a218f-80f7-4feb-8c9f-af9138dbb92a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3d4a218f-80f7-4feb-8c9f-af9138dbb92a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccb184bb-de3a-44e1-a387-d68e1f4a47d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 13,
    "yorig": 16
}