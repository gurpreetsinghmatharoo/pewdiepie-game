{
    "id": "7c5db204-c250-4f6b-9453-5a20a7e50dd6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sItemFlexGun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3cd28be-f5b1-4ce8-ba74-f1baad63bce2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c5db204-c250-4f6b-9453-5a20a7e50dd6",
            "compositeImage": {
                "id": "598fa00f-0073-42f8-aac3-e2f4c092fd1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3cd28be-f5b1-4ce8-ba74-f1baad63bce2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24f78765-a2d1-433d-9907-a8623e0f910f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3cd28be-f5b1-4ce8-ba74-f1baad63bce2",
                    "LayerId": "b36d23cc-7c95-43c0-b938-cd3c9b4e5852"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b36d23cc-7c95-43c0-b938-cd3c9b4e5852",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c5db204-c250-4f6b-9453-5a20a7e50dd6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}