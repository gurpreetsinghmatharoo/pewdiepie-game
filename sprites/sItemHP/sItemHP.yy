{
    "id": "01361446-4072-4f98-bc33-f9cff271e230",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sItemHP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6a4d18d-b837-49bd-a638-8d8ff6a84f0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01361446-4072-4f98-bc33-f9cff271e230",
            "compositeImage": {
                "id": "ef51b176-79f9-4a5d-a2ee-cacf405eb894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6a4d18d-b837-49bd-a638-8d8ff6a84f0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "003ee83b-7d44-4bca-82b2-aa5eeb18b98c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6a4d18d-b837-49bd-a638-8d8ff6a84f0c",
                    "LayerId": "8e65d403-5d89-4669-a1d1-c0c60f514ace"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "8e65d403-5d89-4669-a1d1-c0c60f514ace",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01361446-4072-4f98-bc33-f9cff271e230",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 0,
    "yorig": 0
}