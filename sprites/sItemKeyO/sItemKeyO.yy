{
    "id": "24a60254-86b4-454a-ad52-20beb8dda9d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sItemKeyO",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 24,
    "bbox_right": 31,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "796240a8-170c-4ae8-8259-867b2ec44be4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24a60254-86b4-454a-ad52-20beb8dda9d7",
            "compositeImage": {
                "id": "d393b268-2171-464a-9634-6d47643f75fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "796240a8-170c-4ae8-8259-867b2ec44be4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c45ce091-7ffb-4bad-9145-9d86b56e8892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "796240a8-170c-4ae8-8259-867b2ec44be4",
                    "LayerId": "c9d28829-8ba5-4335-9e4c-0a49936f5eac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c9d28829-8ba5-4335-9e4c-0a49936f5eac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24a60254-86b4-454a-ad52-20beb8dda9d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 5,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 31,
    "yorig": 16
}