{
    "id": "f2a038be-6721-41a9-ba92-98afe679dd65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sItemKeyP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24f71ca9-f9d8-40b7-904e-b41f49abccfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2a038be-6721-41a9-ba92-98afe679dd65",
            "compositeImage": {
                "id": "1e62a7da-12ed-46fd-a7a3-2f6bdab6b424",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24f71ca9-f9d8-40b7-904e-b41f49abccfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e239e4d-e6f3-4f42-a726-540970ba4ba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24f71ca9-f9d8-40b7-904e-b41f49abccfb",
                    "LayerId": "b85774e2-40d3-40ef-8d05-6f5af59f15b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b85774e2-40d3-40ef-8d05-6f5af59f15b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2a038be-6721-41a9-ba92-98afe679dd65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 16
}