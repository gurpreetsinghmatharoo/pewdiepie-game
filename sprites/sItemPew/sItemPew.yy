{
    "id": "51425de1-3f2b-422c-87a7-d8d74e62f238",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sItemPew",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "769876cc-85f0-4c80-93c3-72ea99730026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51425de1-3f2b-422c-87a7-d8d74e62f238",
            "compositeImage": {
                "id": "3e4c63ce-caa0-483b-bce7-62c6cd615656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "769876cc-85f0-4c80-93c3-72ea99730026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb90e912-3a6d-47cc-bd11-923d9a176b0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "769876cc-85f0-4c80-93c3-72ea99730026",
                    "LayerId": "cc0231ca-5338-4f87-8f59-799db489458b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cc0231ca-5338-4f87-8f59-799db489458b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51425de1-3f2b-422c-87a7-d8d74e62f238",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}