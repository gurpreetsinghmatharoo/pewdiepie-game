{
    "id": "2e674412-fa85-416f-adaf-6723541277f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sItemPickup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de493662-e71a-4a41-b9d6-b265387669e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e674412-fa85-416f-adaf-6723541277f1",
            "compositeImage": {
                "id": "e0fa4e71-c2a3-4e90-9aec-e852b6f4ff9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de493662-e71a-4a41-b9d6-b265387669e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46bed2c0-a0b5-4d59-a22e-c6140927952f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de493662-e71a-4a41-b9d6-b265387669e4",
                    "LayerId": "6e1367a3-2e77-42d0-8521-6a7751dc93e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6e1367a3-2e77-42d0-8521-6a7751dc93e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e674412-fa85-416f-adaf-6723541277f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}