{
    "id": "fc740411-01cf-40ef-b0d0-aecca772ab87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sJacksept_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 21,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ab22044-5d93-4c77-8b59-bbd2a16a9ee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc740411-01cf-40ef-b0d0-aecca772ab87",
            "compositeImage": {
                "id": "ff48aa98-fb5f-44d6-81f2-58f1174738db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ab22044-5d93-4c77-8b59-bbd2a16a9ee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5102ed16-7b47-4c75-a5e7-f8d7e238a04c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ab22044-5d93-4c77-8b59-bbd2a16a9ee8",
                    "LayerId": "68b892fb-57ff-40d7-b402-e2b2e7aae2d2"
                }
            ]
        },
        {
            "id": "5ffe61d6-e2a0-4137-851d-42f50e25060c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc740411-01cf-40ef-b0d0-aecca772ab87",
            "compositeImage": {
                "id": "42fd6cd9-826d-4357-9bea-0490006608a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ffe61d6-e2a0-4137-851d-42f50e25060c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c35d6f29-0868-4740-a1bf-57b09775eec2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ffe61d6-e2a0-4137-851d-42f50e25060c",
                    "LayerId": "68b892fb-57ff-40d7-b402-e2b2e7aae2d2"
                }
            ]
        },
        {
            "id": "761bd17c-beda-4737-a93d-d7b556386057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc740411-01cf-40ef-b0d0-aecca772ab87",
            "compositeImage": {
                "id": "ac74934d-6c61-4a25-b6a2-fb8dc2f78fd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "761bd17c-beda-4737-a93d-d7b556386057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6999326b-7d3f-4ce0-9c77-7d697d7e2d5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "761bd17c-beda-4737-a93d-d7b556386057",
                    "LayerId": "68b892fb-57ff-40d7-b402-e2b2e7aae2d2"
                }
            ]
        },
        {
            "id": "ca9dc350-851f-405d-a12d-51dfa297c82c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc740411-01cf-40ef-b0d0-aecca772ab87",
            "compositeImage": {
                "id": "53c1df50-7998-469d-bae9-6d8db843452a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca9dc350-851f-405d-a12d-51dfa297c82c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dad883f-e926-4113-9ece-bb43b717787c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca9dc350-851f-405d-a12d-51dfa297c82c",
                    "LayerId": "68b892fb-57ff-40d7-b402-e2b2e7aae2d2"
                }
            ]
        },
        {
            "id": "3d2a9d28-b864-4e96-9b59-712020368dcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc740411-01cf-40ef-b0d0-aecca772ab87",
            "compositeImage": {
                "id": "f96fe866-d381-43c8-a3ef-22f5818fab62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d2a9d28-b864-4e96-9b59-712020368dcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2306e9db-17be-4086-bdfd-ae0d798fdc5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d2a9d28-b864-4e96-9b59-712020368dcf",
                    "LayerId": "68b892fb-57ff-40d7-b402-e2b2e7aae2d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "68b892fb-57ff-40d7-b402-e2b2e7aae2d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc740411-01cf-40ef-b0d0-aecca772ab87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}