{
    "id": "43320a9f-edb7-4953-a147-35821024d3a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMagicPart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ca1068d-e2e7-4b1b-845c-b343c55aebdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43320a9f-edb7-4953-a147-35821024d3a6",
            "compositeImage": {
                "id": "451c3d5d-17f0-4ffa-b6da-1b2fd6de4a46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ca1068d-e2e7-4b1b-845c-b343c55aebdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dcec67b-944d-4b5a-abf7-868aa090dda7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ca1068d-e2e7-4b1b-845c-b343c55aebdd",
                    "LayerId": "c220c8cb-952e-45d8-a468-8e2861969fb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "c220c8cb-952e-45d8-a468-8e2861969fb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43320a9f-edb7-4953-a147-35821024d3a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}