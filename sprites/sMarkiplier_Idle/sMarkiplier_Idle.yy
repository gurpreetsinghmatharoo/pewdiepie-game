{
    "id": "d511615f-ed66-431b-816f-626802de6662",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMarkiplier_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 20,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63855650-f48e-4421-b4d0-8e1a0d02c1c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d511615f-ed66-431b-816f-626802de6662",
            "compositeImage": {
                "id": "c81924bb-6e43-43a2-84bc-e43ddc091f10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63855650-f48e-4421-b4d0-8e1a0d02c1c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c523978c-9fdb-42bb-a4c4-a006d06d0317",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63855650-f48e-4421-b4d0-8e1a0d02c1c4",
                    "LayerId": "72061c63-f3d6-42cb-aedd-c0300074a716"
                }
            ]
        },
        {
            "id": "d95770e0-f703-4eb3-9059-fdaf996f31aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d511615f-ed66-431b-816f-626802de6662",
            "compositeImage": {
                "id": "370fded3-a7c5-482f-aa92-91d3ebb6086a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d95770e0-f703-4eb3-9059-fdaf996f31aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5ef188f-01c4-4a56-a317-0cd2b553a6ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d95770e0-f703-4eb3-9059-fdaf996f31aa",
                    "LayerId": "72061c63-f3d6-42cb-aedd-c0300074a716"
                }
            ]
        },
        {
            "id": "507cbb2b-a25a-49e7-bf32-2bfd985fd38e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d511615f-ed66-431b-816f-626802de6662",
            "compositeImage": {
                "id": "44f99309-df0b-4df1-b4c2-ebe84b16303c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "507cbb2b-a25a-49e7-bf32-2bfd985fd38e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bea797a8-6138-4244-8f87-1928cb8a1f9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "507cbb2b-a25a-49e7-bf32-2bfd985fd38e",
                    "LayerId": "72061c63-f3d6-42cb-aedd-c0300074a716"
                }
            ]
        },
        {
            "id": "af6e3822-2ade-44d0-ba00-20e2db72f584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d511615f-ed66-431b-816f-626802de6662",
            "compositeImage": {
                "id": "386f6afc-997e-4a5d-be2b-d9c728b38199",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af6e3822-2ade-44d0-ba00-20e2db72f584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "021f4e40-a1dd-4bfc-819e-2b7f54287faf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af6e3822-2ade-44d0-ba00-20e2db72f584",
                    "LayerId": "72061c63-f3d6-42cb-aedd-c0300074a716"
                }
            ]
        },
        {
            "id": "90f9e0ee-1c2f-4d9c-b112-8b3d83ea1f85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d511615f-ed66-431b-816f-626802de6662",
            "compositeImage": {
                "id": "6d297ccd-db00-4298-a059-ffc67220b3a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90f9e0ee-1c2f-4d9c-b112-8b3d83ea1f85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d55bc02-5283-42b9-bf57-4433ba2d7817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90f9e0ee-1c2f-4d9c-b112-8b3d83ea1f85",
                    "LayerId": "72061c63-f3d6-42cb-aedd-c0300074a716"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "72061c63-f3d6-42cb-aedd-c0300074a716",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d511615f-ed66-431b-816f-626802de6662",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}