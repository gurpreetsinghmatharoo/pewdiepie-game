{
    "id": "0b493c41-777c-46a2-89ce-3050035620bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMartiplier",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 5,
    "bbox_right": 332,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99f15c35-fe5b-4c2a-bedc-221f122b777f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b493c41-777c-46a2-89ce-3050035620bb",
            "compositeImage": {
                "id": "4bc8a52f-4e93-4f52-8da8-3fb5169da958",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f15c35-fe5b-4c2a-bedc-221f122b777f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c31458fa-091a-4daf-9506-fae26f9e1bb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f15c35-fe5b-4c2a-bedc-221f122b777f",
                    "LayerId": "7b5d33c8-1676-456b-8ebd-56e4e2a71517"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "7b5d33c8-1676-456b-8ebd-56e4e2a71517",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b493c41-777c-46a2-89ce-3050035620bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 340,
    "xorig": 0,
    "yorig": 0
}