{
    "id": "a62ed434-06c3-4200-bcbb-1d48c646c0be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMarzia_Alert",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 22,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b418216-b904-4e59-9052-439a6495bf0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a62ed434-06c3-4200-bcbb-1d48c646c0be",
            "compositeImage": {
                "id": "9692e6ad-c764-4b0e-bd3a-0a6f306078a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b418216-b904-4e59-9052-439a6495bf0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f35217e-4948-4f05-b6bf-33ea10861d3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b418216-b904-4e59-9052-439a6495bf0b",
                    "LayerId": "15d87724-c96d-4f9f-a572-0bd95187fd99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "15d87724-c96d-4f9f-a572-0bd95187fd99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a62ed434-06c3-4200-bcbb-1d48c646c0be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}