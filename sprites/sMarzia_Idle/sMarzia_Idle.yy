{
    "id": "24d9f45e-fdde-4529-81c7-603846c2a5b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMarzia_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 20,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c305f393-a304-4022-b0db-8100635958eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24d9f45e-fdde-4529-81c7-603846c2a5b2",
            "compositeImage": {
                "id": "aaf8a8c8-95ff-4fab-8132-20d4fea2d956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c305f393-a304-4022-b0db-8100635958eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b670651-a5f8-465e-bb2a-bcf258229fea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c305f393-a304-4022-b0db-8100635958eb",
                    "LayerId": "5a84e500-56af-476a-83ba-f0d4b90fa3c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5a84e500-56af-476a-83ba-f0d4b90fa3c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24d9f45e-fdde-4529-81c7-603846c2a5b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}