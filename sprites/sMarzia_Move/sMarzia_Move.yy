{
    "id": "fafba86d-df3d-4a63-b780-b8e1ee03084f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMarzia_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f64c2da-a193-4e32-9936-d7b9302d7bfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fafba86d-df3d-4a63-b780-b8e1ee03084f",
            "compositeImage": {
                "id": "fe65da49-b8ab-463c-ab93-9f58a92785fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f64c2da-a193-4e32-9936-d7b9302d7bfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "881e1010-3a1a-4031-8889-3b82de4d0b7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f64c2da-a193-4e32-9936-d7b9302d7bfb",
                    "LayerId": "c7abbd1f-0266-4820-a01a-663830a77bc6"
                }
            ]
        },
        {
            "id": "b9b7b70a-720b-4fcf-a6df-3335294aeab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fafba86d-df3d-4a63-b780-b8e1ee03084f",
            "compositeImage": {
                "id": "a212b096-05d1-4a07-b0e1-ac6cc59a4851",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9b7b70a-720b-4fcf-a6df-3335294aeab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47dbd2c4-59e9-4be8-b9b4-dcf550bc5874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9b7b70a-720b-4fcf-a6df-3335294aeab9",
                    "LayerId": "c7abbd1f-0266-4820-a01a-663830a77bc6"
                }
            ]
        },
        {
            "id": "2d3f01d1-9593-4394-bfc5-494105bba50b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fafba86d-df3d-4a63-b780-b8e1ee03084f",
            "compositeImage": {
                "id": "a3f838a5-ce43-4656-854b-8ca0979c58ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d3f01d1-9593-4394-bfc5-494105bba50b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5b2015c-a53a-4523-ae88-d8129ede24ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d3f01d1-9593-4394-bfc5-494105bba50b",
                    "LayerId": "c7abbd1f-0266-4820-a01a-663830a77bc6"
                }
            ]
        },
        {
            "id": "814ba409-8706-4d0f-a4e1-a006c700bb8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fafba86d-df3d-4a63-b780-b8e1ee03084f",
            "compositeImage": {
                "id": "ae9d9645-dca9-4ddc-bf86-b3d1e1de687d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "814ba409-8706-4d0f-a4e1-a006c700bb8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ea24564-35a3-480c-a027-d9953716e4f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "814ba409-8706-4d0f-a4e1-a006c700bb8e",
                    "LayerId": "c7abbd1f-0266-4820-a01a-663830a77bc6"
                }
            ]
        },
        {
            "id": "22d19497-bddf-40c0-a3bd-fc66c3478ebd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fafba86d-df3d-4a63-b780-b8e1ee03084f",
            "compositeImage": {
                "id": "30729e37-9283-4542-a270-0b728b890aad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22d19497-bddf-40c0-a3bd-fc66c3478ebd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f18e8ed3-cec2-4ca6-a31f-c24d3b78616c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d19497-bddf-40c0-a3bd-fc66c3478ebd",
                    "LayerId": "c7abbd1f-0266-4820-a01a-663830a77bc6"
                }
            ]
        },
        {
            "id": "c7fdb654-e47f-4365-b3b3-e85bef42fc83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fafba86d-df3d-4a63-b780-b8e1ee03084f",
            "compositeImage": {
                "id": "854548d5-c3cf-4e38-85d7-3f559df1f5db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7fdb654-e47f-4365-b3b3-e85bef42fc83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0ac0199-f05e-4953-87ce-e24f26c1a325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7fdb654-e47f-4365-b3b3-e85bef42fc83",
                    "LayerId": "c7abbd1f-0266-4820-a01a-663830a77bc6"
                }
            ]
        },
        {
            "id": "1e204cf8-1f89-4948-923f-2fa769ec9924",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fafba86d-df3d-4a63-b780-b8e1ee03084f",
            "compositeImage": {
                "id": "be2e7ea7-13ca-424b-b51f-427c217f2e67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e204cf8-1f89-4948-923f-2fa769ec9924",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "facc5c4c-de33-415d-8b05-3e8e3a5d0ba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e204cf8-1f89-4948-923f-2fa769ec9924",
                    "LayerId": "c7abbd1f-0266-4820-a01a-663830a77bc6"
                }
            ]
        },
        {
            "id": "0aff1cda-75ae-421f-b836-e8e097e9a958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fafba86d-df3d-4a63-b780-b8e1ee03084f",
            "compositeImage": {
                "id": "915970ea-19e8-46d6-a6a3-5d0c2735f42e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aff1cda-75ae-421f-b836-e8e097e9a958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a298446-2070-47e0-9ac4-204e84e2fef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aff1cda-75ae-421f-b836-e8e097e9a958",
                    "LayerId": "c7abbd1f-0266-4820-a01a-663830a77bc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c7abbd1f-0266-4820-a01a-663830a77bc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fafba86d-df3d-4a63-b780-b8e1ee03084f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}