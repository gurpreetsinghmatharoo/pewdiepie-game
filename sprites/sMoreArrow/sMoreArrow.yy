{
    "id": "9a112b42-ab8e-47e9-9e8d-aceff6ab205e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMoreArrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 12,
    "bbox_right": 20,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfccb06d-679e-4854-aa72-daf9076f05d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a112b42-ab8e-47e9-9e8d-aceff6ab205e",
            "compositeImage": {
                "id": "499cd5bc-7e35-421d-a46c-fba43643afe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfccb06d-679e-4854-aa72-daf9076f05d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22d61487-daa1-4610-9c75-8fb1973adba6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfccb06d-679e-4854-aa72-daf9076f05d0",
                    "LayerId": "80dd9344-861d-44d3-ad0d-dfef73f5071c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "80dd9344-861d-44d3-ad0d-dfef73f5071c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a112b42-ab8e-47e9-9e8d-aceff6ab205e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 2,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 23,
    "yorig": 0
}