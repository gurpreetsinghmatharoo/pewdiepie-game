{
    "id": "e1b0ec0e-6f2e-48a7-858b-b66f3c0540e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sNPCCity0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 20,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1744122f-471e-4685-9428-f2671d332bfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1b0ec0e-6f2e-48a7-858b-b66f3c0540e8",
            "compositeImage": {
                "id": "cf06d221-a0cb-47b3-8373-3d5b1c6309d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1744122f-471e-4685-9428-f2671d332bfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b419e0-a3cc-406d-8198-f04974b0a1c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1744122f-471e-4685-9428-f2671d332bfe",
                    "LayerId": "0be9fa8b-d0f9-4562-905b-4630d31fe2f7"
                }
            ]
        },
        {
            "id": "5b533f9b-3852-45d8-9090-f8229aaa9ec5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1b0ec0e-6f2e-48a7-858b-b66f3c0540e8",
            "compositeImage": {
                "id": "d003582f-4b21-4bf9-901f-932374916aa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b533f9b-3852-45d8-9090-f8229aaa9ec5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af66de48-8f01-4953-abe6-d9ef062ba43c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b533f9b-3852-45d8-9090-f8229aaa9ec5",
                    "LayerId": "0be9fa8b-d0f9-4562-905b-4630d31fe2f7"
                }
            ]
        },
        {
            "id": "c59f2cb8-cd84-404a-ab7c-cbb6a30c75ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1b0ec0e-6f2e-48a7-858b-b66f3c0540e8",
            "compositeImage": {
                "id": "a18f9559-b892-4fb3-b4d5-4f71b7753d2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c59f2cb8-cd84-404a-ab7c-cbb6a30c75ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34f84f3c-dad4-4862-be8a-2711a88674d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c59f2cb8-cd84-404a-ab7c-cbb6a30c75ce",
                    "LayerId": "0be9fa8b-d0f9-4562-905b-4630d31fe2f7"
                }
            ]
        },
        {
            "id": "7037abcf-cc59-425f-9c82-8c530a9e8e66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1b0ec0e-6f2e-48a7-858b-b66f3c0540e8",
            "compositeImage": {
                "id": "bfda28da-fc2b-4bc8-bc70-83393679365e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7037abcf-cc59-425f-9c82-8c530a9e8e66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dd2d9e9-f9ed-4889-9b32-ba9dd138b423",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7037abcf-cc59-425f-9c82-8c530a9e8e66",
                    "LayerId": "0be9fa8b-d0f9-4562-905b-4630d31fe2f7"
                }
            ]
        },
        {
            "id": "84e2a400-c247-4d63-bf4a-229b6121b732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1b0ec0e-6f2e-48a7-858b-b66f3c0540e8",
            "compositeImage": {
                "id": "528a45f0-8b7b-4394-99eb-2b66b3e38cbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84e2a400-c247-4d63-bf4a-229b6121b732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c27c6486-4c98-4b72-839d-3f04e6675852",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84e2a400-c247-4d63-bf4a-229b6121b732",
                    "LayerId": "0be9fa8b-d0f9-4562-905b-4630d31fe2f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0be9fa8b-d0f9-4562-905b-4630d31fe2f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1b0ec0e-6f2e-48a7-858b-b66f3c0540e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}