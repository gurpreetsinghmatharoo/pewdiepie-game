{
    "id": "38a12f03-932a-4db9-935d-861fe6fff187",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPewBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3ffeb60-b80f-4690-a59e-d428257f6bea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38a12f03-932a-4db9-935d-861fe6fff187",
            "compositeImage": {
                "id": "89bb13a3-c6d7-481c-ad69-1be1ba4b1518",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3ffeb60-b80f-4690-a59e-d428257f6bea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c73893c-492a-47c6-a432-06c2eeecfbb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3ffeb60-b80f-4690-a59e-d428257f6bea",
                    "LayerId": "0abd58fa-a324-4287-a979-b217d250f7d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "0abd58fa-a324-4287-a979-b217d250f7d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38a12f03-932a-4db9-935d-861fe6fff187",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 0
}