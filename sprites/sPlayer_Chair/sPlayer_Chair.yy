{
    "id": "a0ed1867-bd24-4476-a264-f0c55f797502",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_Chair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 24,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b5b4671-5576-41ac-ab29-890922d49e2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0ed1867-bd24-4476-a264-f0c55f797502",
            "compositeImage": {
                "id": "7ac77f82-55e3-4356-a9e6-d148cf624801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b5b4671-5576-41ac-ab29-890922d49e2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b3550f4-7fd9-40f7-8467-69e915356388",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b5b4671-5576-41ac-ab29-890922d49e2e",
                    "LayerId": "7a445e61-27f9-427b-a73e-4fb50132a6eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7a445e61-27f9-427b-a73e-4fb50132a6eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0ed1867-bd24-4476-a264-f0c55f797502",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}