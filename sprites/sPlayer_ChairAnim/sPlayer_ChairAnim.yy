{
    "id": "f3f420a7-a28d-44e8-80b5-6692eded23ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_ChairAnim",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de62f59c-5ee5-4cea-8d68-08614647c89c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f420a7-a28d-44e8-80b5-6692eded23ee",
            "compositeImage": {
                "id": "db7b9546-943e-4cf9-bf9f-c011c61be0be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de62f59c-5ee5-4cea-8d68-08614647c89c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d99b51e-c492-4776-93c2-c000c3c18541",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de62f59c-5ee5-4cea-8d68-08614647c89c",
                    "LayerId": "c8957e70-4865-4b76-aa6d-5ce1fd878d6d"
                }
            ]
        },
        {
            "id": "f3c3866e-3a15-44dc-89ad-0672e59fa92e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f420a7-a28d-44e8-80b5-6692eded23ee",
            "compositeImage": {
                "id": "9edf9c2b-9ec5-436d-a362-8188903ce64b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3c3866e-3a15-44dc-89ad-0672e59fa92e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f77a1cb-83ef-46e3-8e5e-0492d83b6d7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3c3866e-3a15-44dc-89ad-0672e59fa92e",
                    "LayerId": "c8957e70-4865-4b76-aa6d-5ce1fd878d6d"
                }
            ]
        },
        {
            "id": "28eed10f-805e-4a5e-b64d-2dfd8c38dfe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f420a7-a28d-44e8-80b5-6692eded23ee",
            "compositeImage": {
                "id": "91b44569-5289-4879-bfce-b055990bea9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28eed10f-805e-4a5e-b64d-2dfd8c38dfe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ac098d5-2cdc-4e48-96b1-34458bea4254",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28eed10f-805e-4a5e-b64d-2dfd8c38dfe8",
                    "LayerId": "c8957e70-4865-4b76-aa6d-5ce1fd878d6d"
                }
            ]
        },
        {
            "id": "1054dc54-c15d-446c-af64-c7d9ac93299b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f420a7-a28d-44e8-80b5-6692eded23ee",
            "compositeImage": {
                "id": "996d08af-9a98-4021-be16-73e02fd11cdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1054dc54-c15d-446c-af64-c7d9ac93299b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "737b5e0c-6e30-4484-9ae6-a337acc631ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1054dc54-c15d-446c-af64-c7d9ac93299b",
                    "LayerId": "c8957e70-4865-4b76-aa6d-5ce1fd878d6d"
                }
            ]
        },
        {
            "id": "ac4b4080-c20c-4836-86a5-1eab34adddfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f420a7-a28d-44e8-80b5-6692eded23ee",
            "compositeImage": {
                "id": "2990fa6e-e04d-48bc-b85c-77d4afa52c7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac4b4080-c20c-4836-86a5-1eab34adddfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b08551e4-0193-40ad-9c54-ca9794b84cf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac4b4080-c20c-4836-86a5-1eab34adddfe",
                    "LayerId": "c8957e70-4865-4b76-aa6d-5ce1fd878d6d"
                }
            ]
        },
        {
            "id": "98c16b82-dcfe-400a-be8b-f6713d902db8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f420a7-a28d-44e8-80b5-6692eded23ee",
            "compositeImage": {
                "id": "bfdeeadb-cca4-49ac-bbbe-32c2c47a9336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98c16b82-dcfe-400a-be8b-f6713d902db8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d5b0f70-90f0-4a5c-a69a-00126b91cbb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98c16b82-dcfe-400a-be8b-f6713d902db8",
                    "LayerId": "c8957e70-4865-4b76-aa6d-5ce1fd878d6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c8957e70-4865-4b76-aa6d-5ce1fd878d6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3f420a7-a28d-44e8-80b5-6692eded23ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}