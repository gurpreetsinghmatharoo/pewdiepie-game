{
    "id": "a2859eb9-27ca-45dc-885c-ced58f3e3a35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_ChairEnter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9df6ba20-077c-47bd-af76-7fefed508ea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2859eb9-27ca-45dc-885c-ced58f3e3a35",
            "compositeImage": {
                "id": "3b8b9121-643d-4255-9543-a5b1fb632982",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9df6ba20-077c-47bd-af76-7fefed508ea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58e25ebb-ac20-4845-99f0-d662ee24bbb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9df6ba20-077c-47bd-af76-7fefed508ea9",
                    "LayerId": "638c2765-1d7e-48b0-b9af-00fa52193e47"
                }
            ]
        },
        {
            "id": "37195173-1f1b-424a-81bb-b238cdf03ba1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2859eb9-27ca-45dc-885c-ced58f3e3a35",
            "compositeImage": {
                "id": "b8c13ca5-5b6a-43c8-8fbf-02f2d5dc3f8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37195173-1f1b-424a-81bb-b238cdf03ba1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a228ccb8-d9d9-4813-987b-babe90338d1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37195173-1f1b-424a-81bb-b238cdf03ba1",
                    "LayerId": "638c2765-1d7e-48b0-b9af-00fa52193e47"
                }
            ]
        },
        {
            "id": "2845b2e6-c057-453e-b073-62b6edc06390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2859eb9-27ca-45dc-885c-ced58f3e3a35",
            "compositeImage": {
                "id": "bcaa6f06-5d13-4ea4-a394-e9a282c89e34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2845b2e6-c057-453e-b073-62b6edc06390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d045015-d4e4-483f-b3f8-0f322ddf3841",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2845b2e6-c057-453e-b073-62b6edc06390",
                    "LayerId": "638c2765-1d7e-48b0-b9af-00fa52193e47"
                }
            ]
        },
        {
            "id": "ee79d985-cd64-46db-b3c5-7181b6e8e64b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2859eb9-27ca-45dc-885c-ced58f3e3a35",
            "compositeImage": {
                "id": "72feb550-a222-4daa-a3ee-e1b02e54c4af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee79d985-cd64-46db-b3c5-7181b6e8e64b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39ba6d28-be5a-4cc4-ba48-738ba181f449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee79d985-cd64-46db-b3c5-7181b6e8e64b",
                    "LayerId": "638c2765-1d7e-48b0-b9af-00fa52193e47"
                }
            ]
        },
        {
            "id": "255dfcc9-f75b-4be9-8476-fc1840f36ecb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2859eb9-27ca-45dc-885c-ced58f3e3a35",
            "compositeImage": {
                "id": "af2c0980-a7d9-44ec-be65-b8dfff8b8408",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "255dfcc9-f75b-4be9-8476-fc1840f36ecb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79031410-38f3-482a-a576-baaf6a4bbc97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "255dfcc9-f75b-4be9-8476-fc1840f36ecb",
                    "LayerId": "638c2765-1d7e-48b0-b9af-00fa52193e47"
                }
            ]
        },
        {
            "id": "fb6e7caa-fa5e-48b0-a85a-717258cb20e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2859eb9-27ca-45dc-885c-ced58f3e3a35",
            "compositeImage": {
                "id": "d4a59c95-c4fd-4ed8-a4e0-4016c3514d97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb6e7caa-fa5e-48b0-a85a-717258cb20e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6163ad4e-9d4c-46c0-b907-3553a8391c16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb6e7caa-fa5e-48b0-a85a-717258cb20e4",
                    "LayerId": "638c2765-1d7e-48b0-b9af-00fa52193e47"
                }
            ]
        },
        {
            "id": "ac9c471b-f6d2-469e-bbc5-3f8f892d7bfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2859eb9-27ca-45dc-885c-ced58f3e3a35",
            "compositeImage": {
                "id": "55441c15-bb0a-494c-a0d2-308f362bd7e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac9c471b-f6d2-469e-bbc5-3f8f892d7bfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "734ea188-5c37-4598-8bfb-9381b7d97d45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac9c471b-f6d2-469e-bbc5-3f8f892d7bfd",
                    "LayerId": "638c2765-1d7e-48b0-b9af-00fa52193e47"
                }
            ]
        },
        {
            "id": "54e62275-0da9-4dde-b3bb-6eb15c13f148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2859eb9-27ca-45dc-885c-ced58f3e3a35",
            "compositeImage": {
                "id": "b4f9e51c-a847-42dd-8ce7-5ba09e30cd9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54e62275-0da9-4dde-b3bb-6eb15c13f148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e758222-1008-4ca2-b202-5058248216bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54e62275-0da9-4dde-b3bb-6eb15c13f148",
                    "LayerId": "638c2765-1d7e-48b0-b9af-00fa52193e47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "638c2765-1d7e-48b0-b9af-00fa52193e47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2859eb9-27ca-45dc-885c-ced58f3e3a35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}