{
    "id": "ca985851-edc0-4f9c-983a-720774f9d5ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_ChairLie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "854e978b-5243-463c-b593-a694664b94bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca985851-edc0-4f9c-983a-720774f9d5ba",
            "compositeImage": {
                "id": "35f93522-84e0-4ed1-8fff-01949be568ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "854e978b-5243-463c-b593-a694664b94bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94256010-d508-4edd-9771-03121b8096b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "854e978b-5243-463c-b593-a694664b94bf",
                    "LayerId": "15af5145-c44e-4f6d-9856-a6e68d3801a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "15af5145-c44e-4f6d-9856-a6e68d3801a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca985851-edc0-4f9c-983a-720774f9d5ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}