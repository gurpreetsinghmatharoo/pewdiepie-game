{
    "id": "60c72d52-b9c6-4363-9c59-e1ff07aa6f0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_FlexGun_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 26,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d507741c-4160-44d6-9d4c-5ce72a123a6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60c72d52-b9c6-4363-9c59-e1ff07aa6f0a",
            "compositeImage": {
                "id": "ac1fe227-11b9-4061-8f0f-75973823a65f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d507741c-4160-44d6-9d4c-5ce72a123a6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56568db7-cae0-4c11-a4bf-6388229934bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d507741c-4160-44d6-9d4c-5ce72a123a6e",
                    "LayerId": "d6a0fc84-595e-4b47-acc4-003d8aacc70c"
                }
            ]
        },
        {
            "id": "97d14b33-cd7c-43f4-bb82-dbb9e3e57db0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60c72d52-b9c6-4363-9c59-e1ff07aa6f0a",
            "compositeImage": {
                "id": "ad271a76-b110-4bc3-a1f8-54c6138c2146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97d14b33-cd7c-43f4-bb82-dbb9e3e57db0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4abf91c-5dac-4591-8c9d-15a442fd1b87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97d14b33-cd7c-43f4-bb82-dbb9e3e57db0",
                    "LayerId": "d6a0fc84-595e-4b47-acc4-003d8aacc70c"
                }
            ]
        },
        {
            "id": "a216a113-3779-4fbe-8bc3-bb777eee740b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60c72d52-b9c6-4363-9c59-e1ff07aa6f0a",
            "compositeImage": {
                "id": "724114bd-9663-413b-9d37-27b338d592bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a216a113-3779-4fbe-8bc3-bb777eee740b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f556ad27-4980-463b-bad4-125bce19fe66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a216a113-3779-4fbe-8bc3-bb777eee740b",
                    "LayerId": "d6a0fc84-595e-4b47-acc4-003d8aacc70c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d6a0fc84-595e-4b47-acc4-003d8aacc70c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60c72d52-b9c6-4363-9c59-e1ff07aa6f0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}