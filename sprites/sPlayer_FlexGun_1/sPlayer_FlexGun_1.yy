{
    "id": "cb8aa47a-dd10-4b50-bb77-e7493d914baf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_FlexGun_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 23,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "769ab300-19d3-4cbc-9350-a71cb2a77790",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb8aa47a-dd10-4b50-bb77-e7493d914baf",
            "compositeImage": {
                "id": "04b0aefa-2a83-4798-9e4d-7228f1fa67bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "769ab300-19d3-4cbc-9350-a71cb2a77790",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce6566c2-3cdb-4adb-a5fd-f46938465769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "769ab300-19d3-4cbc-9350-a71cb2a77790",
                    "LayerId": "a43736bc-2c0e-4d23-8543-fa5c5be5bb7d"
                }
            ]
        },
        {
            "id": "851e0ed2-00c8-471c-8373-f056d849062b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb8aa47a-dd10-4b50-bb77-e7493d914baf",
            "compositeImage": {
                "id": "a3352267-cfe8-407e-91d5-48e4b54125e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "851e0ed2-00c8-471c-8373-f056d849062b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35523301-a510-4443-a73e-756fef394c8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "851e0ed2-00c8-471c-8373-f056d849062b",
                    "LayerId": "a43736bc-2c0e-4d23-8543-fa5c5be5bb7d"
                }
            ]
        },
        {
            "id": "e1e53b82-4075-4bf2-a598-d767d86abb05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb8aa47a-dd10-4b50-bb77-e7493d914baf",
            "compositeImage": {
                "id": "06358d43-8926-469e-95f6-6f778503ba80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1e53b82-4075-4bf2-a598-d767d86abb05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8fceb69-a8f7-49bd-bd5a-8aa0b61047b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1e53b82-4075-4bf2-a598-d767d86abb05",
                    "LayerId": "a43736bc-2c0e-4d23-8543-fa5c5be5bb7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a43736bc-2c0e-4d23-8543-fa5c5be5bb7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb8aa47a-dd10-4b50-bb77-e7493d914baf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}