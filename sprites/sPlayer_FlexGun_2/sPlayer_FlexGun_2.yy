{
    "id": "6c0eab33-8a23-4205-9d4f-7605721c3905",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_FlexGun_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 21,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aef8f57b-3b8b-4043-940f-c70e512f7d73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c0eab33-8a23-4205-9d4f-7605721c3905",
            "compositeImage": {
                "id": "8efff979-cf52-4fbc-ad0c-9c20674502fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aef8f57b-3b8b-4043-940f-c70e512f7d73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5da9ce2-a3ec-4a48-9595-1f8df28639b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aef8f57b-3b8b-4043-940f-c70e512f7d73",
                    "LayerId": "80c54064-de0a-41be-a765-d4bd2e21b395"
                }
            ]
        },
        {
            "id": "e495f873-38da-48be-b5b5-c1f4b791db4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c0eab33-8a23-4205-9d4f-7605721c3905",
            "compositeImage": {
                "id": "369410f5-9614-4257-b391-fc163d24cea7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e495f873-38da-48be-b5b5-c1f4b791db4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a22039bd-83e1-4625-acd4-99daf0765472",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e495f873-38da-48be-b5b5-c1f4b791db4e",
                    "LayerId": "80c54064-de0a-41be-a765-d4bd2e21b395"
                }
            ]
        },
        {
            "id": "36262136-dd65-487e-8ae1-712621a844fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c0eab33-8a23-4205-9d4f-7605721c3905",
            "compositeImage": {
                "id": "029312b2-9716-44ea-b981-4ac46dbef099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36262136-dd65-487e-8ae1-712621a844fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fd3290e-b5aa-40d1-81cb-b18261b012c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36262136-dd65-487e-8ae1-712621a844fe",
                    "LayerId": "80c54064-de0a-41be-a765-d4bd2e21b395"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "80c54064-de0a-41be-a765-d4bd2e21b395",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c0eab33-8a23-4205-9d4f-7605721c3905",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}