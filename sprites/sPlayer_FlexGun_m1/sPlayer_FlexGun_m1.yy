{
    "id": "43982919-f56b-49d9-82ee-22e5d94c4a1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_FlexGun_m1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 23,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e938df36-59f9-494d-b684-54922d860da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43982919-f56b-49d9-82ee-22e5d94c4a1b",
            "compositeImage": {
                "id": "cdb163b9-dba8-4f09-afec-b02fe825ccf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e938df36-59f9-494d-b684-54922d860da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d09c40c5-cf24-481b-8124-f364511683f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e938df36-59f9-494d-b684-54922d860da1",
                    "LayerId": "6edc0654-2493-4487-9755-523256fcfade"
                }
            ]
        },
        {
            "id": "c709351b-9747-456d-8dde-27183c77de8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43982919-f56b-49d9-82ee-22e5d94c4a1b",
            "compositeImage": {
                "id": "e67b382a-719b-4fef-982b-1bf8e69ad52e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c709351b-9747-456d-8dde-27183c77de8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b562de4-a616-4ef5-a6b0-fd2a6ea09d9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c709351b-9747-456d-8dde-27183c77de8c",
                    "LayerId": "6edc0654-2493-4487-9755-523256fcfade"
                }
            ]
        },
        {
            "id": "7cee1638-3534-4371-9804-a4cb96b72275",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43982919-f56b-49d9-82ee-22e5d94c4a1b",
            "compositeImage": {
                "id": "0569af7e-f61b-4423-b5cf-c73d91ed1324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cee1638-3534-4371-9804-a4cb96b72275",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcdabb8d-487e-40fa-827e-710e093f4204",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cee1638-3534-4371-9804-a4cb96b72275",
                    "LayerId": "6edc0654-2493-4487-9755-523256fcfade"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6edc0654-2493-4487-9755-523256fcfade",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43982919-f56b-49d9-82ee-22e5d94c4a1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}