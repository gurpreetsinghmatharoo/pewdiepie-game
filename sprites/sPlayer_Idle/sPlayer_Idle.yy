{
    "id": "f4c59696-fe65-48f6-9012-a1551e9a2f92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 20,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89a34457-494b-4c29-aaf7-c95dfb63611c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c59696-fe65-48f6-9012-a1551e9a2f92",
            "compositeImage": {
                "id": "673f6149-a006-4e02-be68-6a8718c172ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89a34457-494b-4c29-aaf7-c95dfb63611c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c23f276-1ee7-42b4-a54c-1b47dc66470f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89a34457-494b-4c29-aaf7-c95dfb63611c",
                    "LayerId": "9c2c6913-993f-476a-92a8-d5063c0e15ac"
                }
            ]
        },
        {
            "id": "b2a723a0-6f82-4f46-8928-0b2cf2a157ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c59696-fe65-48f6-9012-a1551e9a2f92",
            "compositeImage": {
                "id": "e87f16f1-7f0c-45dc-a86c-4b2427d815a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2a723a0-6f82-4f46-8928-0b2cf2a157ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0715c11-e581-4b05-958a-13b8db54785b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2a723a0-6f82-4f46-8928-0b2cf2a157ec",
                    "LayerId": "9c2c6913-993f-476a-92a8-d5063c0e15ac"
                }
            ]
        },
        {
            "id": "6494ec25-699c-41b9-a628-3831c7a13e38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c59696-fe65-48f6-9012-a1551e9a2f92",
            "compositeImage": {
                "id": "98299d5d-f751-4d25-8655-c06563208071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6494ec25-699c-41b9-a628-3831c7a13e38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62f6026b-b3fc-4603-ac75-bbc1f7a5a7b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6494ec25-699c-41b9-a628-3831c7a13e38",
                    "LayerId": "9c2c6913-993f-476a-92a8-d5063c0e15ac"
                }
            ]
        },
        {
            "id": "cf0f5624-0c28-4206-bfce-bca50b53f98f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c59696-fe65-48f6-9012-a1551e9a2f92",
            "compositeImage": {
                "id": "e598e54b-87e6-4f46-b9ab-ff5d2604773a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf0f5624-0c28-4206-bfce-bca50b53f98f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3439b2ff-d0d1-4b4e-b04a-8aeba384b93e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf0f5624-0c28-4206-bfce-bca50b53f98f",
                    "LayerId": "9c2c6913-993f-476a-92a8-d5063c0e15ac"
                }
            ]
        },
        {
            "id": "99949ae4-a6ac-4e8e-aa69-658a9c8cccea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c59696-fe65-48f6-9012-a1551e9a2f92",
            "compositeImage": {
                "id": "86f1b5d5-d130-433a-a42e-861a0be52fdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99949ae4-a6ac-4e8e-aa69-658a9c8cccea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4933c6be-245a-4890-9fa5-5d5055f50cba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99949ae4-a6ac-4e8e-aa69-658a9c8cccea",
                    "LayerId": "9c2c6913-993f-476a-92a8-d5063c0e15ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9c2c6913-993f-476a-92a8-d5063c0e15ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4c59696-fe65-48f6-9012-a1551e9a2f92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}