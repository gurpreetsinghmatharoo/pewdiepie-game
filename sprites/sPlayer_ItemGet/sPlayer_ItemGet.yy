{
    "id": "5b7ee8e8-f83e-48c2-ab5f-a5d742c99860",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_ItemGet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 21,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aba2b6ee-dfc0-4b8c-a9c6-efa80616392c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b7ee8e8-f83e-48c2-ab5f-a5d742c99860",
            "compositeImage": {
                "id": "6068eac2-d3a8-4bdb-b597-d8db11791e4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aba2b6ee-dfc0-4b8c-a9c6-efa80616392c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a337317-d2f3-4a47-bf68-36521661a8e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aba2b6ee-dfc0-4b8c-a9c6-efa80616392c",
                    "LayerId": "6fd3e6d1-6c16-49d5-bc70-9b1a3d1aff92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6fd3e6d1-6c16-49d5-bc70-9b1a3d1aff92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b7ee8e8-f83e-48c2-ab5f-a5d742c99860",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}