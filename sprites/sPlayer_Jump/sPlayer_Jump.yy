{
    "id": "ebde56a5-6ffa-4069-881c-2cae563a8c45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_Jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e21a9db7-20bd-4bb5-8ff6-9da44b2449d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebde56a5-6ffa-4069-881c-2cae563a8c45",
            "compositeImage": {
                "id": "2dfe4359-dad1-47b8-915b-8194f4bfe7d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e21a9db7-20bd-4bb5-8ff6-9da44b2449d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f5e7b5a-62aa-495e-8b6e-0c2b6c423270",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e21a9db7-20bd-4bb5-8ff6-9da44b2449d1",
                    "LayerId": "d700671d-cd62-4e08-b39f-6f960d59b5ce"
                }
            ]
        },
        {
            "id": "c8814a5a-b7bd-43a7-800d-91e365ca9bbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebde56a5-6ffa-4069-881c-2cae563a8c45",
            "compositeImage": {
                "id": "eb1d9154-d739-4181-a9e4-690fc11e177c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8814a5a-b7bd-43a7-800d-91e365ca9bbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84d9d13b-711f-44e4-adf2-24399e9f3bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8814a5a-b7bd-43a7-800d-91e365ca9bbe",
                    "LayerId": "d700671d-cd62-4e08-b39f-6f960d59b5ce"
                }
            ]
        },
        {
            "id": "a103a9f6-310a-4b5a-a7ac-754f15b86e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebde56a5-6ffa-4069-881c-2cae563a8c45",
            "compositeImage": {
                "id": "55f6c89a-07fa-440b-ad31-b4efcd1f0206",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a103a9f6-310a-4b5a-a7ac-754f15b86e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ae7f90a-3db9-4cf4-81c7-484f111f2a49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a103a9f6-310a-4b5a-a7ac-754f15b86e1e",
                    "LayerId": "d700671d-cd62-4e08-b39f-6f960d59b5ce"
                }
            ]
        },
        {
            "id": "9b618d4d-b407-4a0e-90f9-39ea8833a1ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebde56a5-6ffa-4069-881c-2cae563a8c45",
            "compositeImage": {
                "id": "f723c9e6-52db-4fce-812c-a34114cd9464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b618d4d-b407-4a0e-90f9-39ea8833a1ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cb5df0f-844c-48df-9594-a984e3103ce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b618d4d-b407-4a0e-90f9-39ea8833a1ca",
                    "LayerId": "d700671d-cd62-4e08-b39f-6f960d59b5ce"
                }
            ]
        },
        {
            "id": "0ccc7e73-c15b-424c-9219-1c6390eb7125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebde56a5-6ffa-4069-881c-2cae563a8c45",
            "compositeImage": {
                "id": "c8d648e4-2100-40c6-9559-b5a9ffda8a2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ccc7e73-c15b-424c-9219-1c6390eb7125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "643340fc-90ce-46fc-bcc1-245bea4ec072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ccc7e73-c15b-424c-9219-1c6390eb7125",
                    "LayerId": "d700671d-cd62-4e08-b39f-6f960d59b5ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d700671d-cd62-4e08-b39f-6f960d59b5ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebde56a5-6ffa-4069-881c-2cae563a8c45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}