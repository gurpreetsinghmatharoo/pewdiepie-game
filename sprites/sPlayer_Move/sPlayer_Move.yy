{
    "id": "827bcbb0-eef8-42ce-bb33-74455ecbc986",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 23,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1bfdc0d-604d-4bb5-a9a9-8d1865b315ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "827bcbb0-eef8-42ce-bb33-74455ecbc986",
            "compositeImage": {
                "id": "5eba41bb-0925-48a4-9d52-c13cee829556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1bfdc0d-604d-4bb5-a9a9-8d1865b315ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4967a701-2f9e-4c95-a0fb-222ef12a4880",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1bfdc0d-604d-4bb5-a9a9-8d1865b315ee",
                    "LayerId": "e3adcf27-dbf1-4058-bb02-ece700f53454"
                }
            ]
        },
        {
            "id": "7ea45f98-d8bd-45e0-b6cd-75334e90f445",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "827bcbb0-eef8-42ce-bb33-74455ecbc986",
            "compositeImage": {
                "id": "09322e84-e0c0-4fac-a6d0-6795c2369d96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ea45f98-d8bd-45e0-b6cd-75334e90f445",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6ee45c0-3e30-446e-a9ae-8551c74b265d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ea45f98-d8bd-45e0-b6cd-75334e90f445",
                    "LayerId": "e3adcf27-dbf1-4058-bb02-ece700f53454"
                }
            ]
        },
        {
            "id": "1c9cf30a-e6e0-426e-adeb-bbc5ee627363",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "827bcbb0-eef8-42ce-bb33-74455ecbc986",
            "compositeImage": {
                "id": "2b82f4d4-8e08-47a1-8451-1115111855a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c9cf30a-e6e0-426e-adeb-bbc5ee627363",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1543f83c-0b63-4001-860a-8ffda7a7b96f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c9cf30a-e6e0-426e-adeb-bbc5ee627363",
                    "LayerId": "e3adcf27-dbf1-4058-bb02-ece700f53454"
                }
            ]
        },
        {
            "id": "5b0f5981-6e7e-49c1-a120-36f77619d1e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "827bcbb0-eef8-42ce-bb33-74455ecbc986",
            "compositeImage": {
                "id": "74f99521-91cb-4a59-af38-db96a81b9724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b0f5981-6e7e-49c1-a120-36f77619d1e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d0e76f4-24f0-47d4-bf87-bf3425e54b71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b0f5981-6e7e-49c1-a120-36f77619d1e7",
                    "LayerId": "e3adcf27-dbf1-4058-bb02-ece700f53454"
                }
            ]
        },
        {
            "id": "b028b373-7042-4b65-943e-56a1ac853223",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "827bcbb0-eef8-42ce-bb33-74455ecbc986",
            "compositeImage": {
                "id": "9a3b3cc3-a10d-448a-a7cd-7fb8deb758fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b028b373-7042-4b65-943e-56a1ac853223",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec4f6bcf-419e-4430-84b0-ccb610db8d50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b028b373-7042-4b65-943e-56a1ac853223",
                    "LayerId": "e3adcf27-dbf1-4058-bb02-ece700f53454"
                }
            ]
        },
        {
            "id": "b20beb46-4168-42e9-a439-9c1a35958e61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "827bcbb0-eef8-42ce-bb33-74455ecbc986",
            "compositeImage": {
                "id": "d345c0ea-6b73-4c0b-89fe-7a8c10e32643",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b20beb46-4168-42e9-a439-9c1a35958e61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09450a87-822d-4725-9220-a4e56bb94764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b20beb46-4168-42e9-a439-9c1a35958e61",
                    "LayerId": "e3adcf27-dbf1-4058-bb02-ece700f53454"
                }
            ]
        },
        {
            "id": "a6f0075a-2a0b-4a65-866b-82dc972e1f6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "827bcbb0-eef8-42ce-bb33-74455ecbc986",
            "compositeImage": {
                "id": "9e725d38-ab46-4ded-be44-bf32e63e95ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6f0075a-2a0b-4a65-866b-82dc972e1f6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0aeebf4d-07b4-4a0a-8809-55f75d93f15d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6f0075a-2a0b-4a65-866b-82dc972e1f6d",
                    "LayerId": "e3adcf27-dbf1-4058-bb02-ece700f53454"
                }
            ]
        },
        {
            "id": "12343233-f8e4-4c99-ab48-3fd16f98d0c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "827bcbb0-eef8-42ce-bb33-74455ecbc986",
            "compositeImage": {
                "id": "0f632c9d-9a19-40bc-a9a7-1d0216df5586",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12343233-f8e4-4c99-ab48-3fd16f98d0c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "806e1106-8758-48f4-8ec2-f48bd20b5a6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12343233-f8e4-4c99-ab48-3fd16f98d0c1",
                    "LayerId": "e3adcf27-dbf1-4058-bb02-ece700f53454"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e3adcf27-dbf1-4058-bb02-ece700f53454",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "827bcbb0-eef8-42ce-bb33-74455ecbc986",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}