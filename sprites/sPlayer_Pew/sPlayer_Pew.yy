{
    "id": "b697cf66-aa83-4760-a94b-28f65382be31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_Pew",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 25,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cfc25d8-b8c2-4352-9564-bceb53108078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b697cf66-aa83-4760-a94b-28f65382be31",
            "compositeImage": {
                "id": "31c56a07-a1c6-41e6-bd40-c45e019734fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cfc25d8-b8c2-4352-9564-bceb53108078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ab565bf-baf2-4e17-94bc-9543aecf9408",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cfc25d8-b8c2-4352-9564-bceb53108078",
                    "LayerId": "c525a58e-d44f-4400-9d9e-2784e998bc64"
                }
            ]
        },
        {
            "id": "af4733f6-c48e-471b-a65b-88a58c8e63c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b697cf66-aa83-4760-a94b-28f65382be31",
            "compositeImage": {
                "id": "2b0e038f-d8f1-4c3d-a755-b15321cbb752",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af4733f6-c48e-471b-a65b-88a58c8e63c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e1920f-e57c-42e6-85ae-2464e85c9f1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af4733f6-c48e-471b-a65b-88a58c8e63c7",
                    "LayerId": "c525a58e-d44f-4400-9d9e-2784e998bc64"
                }
            ]
        },
        {
            "id": "c92707c4-bdba-433d-9b80-00c5377fb023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b697cf66-aa83-4760-a94b-28f65382be31",
            "compositeImage": {
                "id": "16dfea9c-8fe2-4042-b816-0e6ef48c87ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c92707c4-bdba-433d-9b80-00c5377fb023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bcdd26c-8996-4486-a4c7-4a408b5beb0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c92707c4-bdba-433d-9b80-00c5377fb023",
                    "LayerId": "c525a58e-d44f-4400-9d9e-2784e998bc64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c525a58e-d44f-4400-9d9e-2784e998bc64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b697cf66-aa83-4760-a94b-28f65382be31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}