{
    "id": "c7a7d66c-2b98-4e36-8be6-a95b0ffde1c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_Pew2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 26,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d135a610-2915-4669-87ed-d53b20864165",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7a7d66c-2b98-4e36-8be6-a95b0ffde1c9",
            "compositeImage": {
                "id": "f6a363cb-4145-4a9f-a744-c57a64a8deba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d135a610-2915-4669-87ed-d53b20864165",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bd5388c-6f1e-42ca-a745-4cd2f88a8833",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d135a610-2915-4669-87ed-d53b20864165",
                    "LayerId": "370ad160-44eb-4b04-ae7b-838d0a36705c"
                }
            ]
        },
        {
            "id": "3b7247e6-e9a5-474b-9779-0d5669cc2795",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7a7d66c-2b98-4e36-8be6-a95b0ffde1c9",
            "compositeImage": {
                "id": "c1488652-7c5d-48dd-b5ab-fd02e9babcf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b7247e6-e9a5-474b-9779-0d5669cc2795",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "965404d5-74f8-4126-ab82-052d7f620f56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b7247e6-e9a5-474b-9779-0d5669cc2795",
                    "LayerId": "370ad160-44eb-4b04-ae7b-838d0a36705c"
                }
            ]
        },
        {
            "id": "38316a14-edfc-440d-aafe-c9035248cbff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7a7d66c-2b98-4e36-8be6-a95b0ffde1c9",
            "compositeImage": {
                "id": "fc20a7c1-82e1-457e-a167-9844aa171e03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38316a14-edfc-440d-aafe-c9035248cbff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c15ae573-3cbf-41ba-aedf-e348a38cad7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38316a14-edfc-440d-aafe-c9035248cbff",
                    "LayerId": "370ad160-44eb-4b04-ae7b-838d0a36705c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "370ad160-44eb-4b04-ae7b-838d0a36705c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7a7d66c-2b98-4e36-8be6-a95b0ffde1c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}