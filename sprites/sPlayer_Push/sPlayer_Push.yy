{
    "id": "9ba95298-38e3-4c25-b578-5e45ff6d14f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_Push",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 23,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "927df316-a46a-468a-b3d5-39342380e8b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ba95298-38e3-4c25-b578-5e45ff6d14f5",
            "compositeImage": {
                "id": "c78f6661-1081-4f24-9dd2-72b143682d4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "927df316-a46a-468a-b3d5-39342380e8b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5128a0f6-b2d6-450c-a3f0-5ba7133bb9a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "927df316-a46a-468a-b3d5-39342380e8b1",
                    "LayerId": "f3d15960-41a7-497d-ab0a-d06fec83e832"
                }
            ]
        },
        {
            "id": "2ebecb9c-54df-4e9c-93be-207647025b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ba95298-38e3-4c25-b578-5e45ff6d14f5",
            "compositeImage": {
                "id": "0136c097-79cd-41a1-99bc-ba91f863f67e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ebecb9c-54df-4e9c-93be-207647025b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47e98cea-69cb-4993-88d4-95da5cb18ef2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ebecb9c-54df-4e9c-93be-207647025b58",
                    "LayerId": "f3d15960-41a7-497d-ab0a-d06fec83e832"
                }
            ]
        },
        {
            "id": "b9b7131d-023e-4724-9c79-3aa13c483d9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ba95298-38e3-4c25-b578-5e45ff6d14f5",
            "compositeImage": {
                "id": "c77a3fca-18ef-42db-bcf2-ab49e475cd5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9b7131d-023e-4724-9c79-3aa13c483d9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bbbf0ef-8478-4696-bc83-cc8537a01c0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9b7131d-023e-4724-9c79-3aa13c483d9c",
                    "LayerId": "f3d15960-41a7-497d-ab0a-d06fec83e832"
                }
            ]
        },
        {
            "id": "8ffcf5ea-99df-4911-94f0-066de2c74f16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ba95298-38e3-4c25-b578-5e45ff6d14f5",
            "compositeImage": {
                "id": "73559860-f75b-43fc-a0f2-9b5d2cfb2cd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ffcf5ea-99df-4911-94f0-066de2c74f16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21ca2490-4d74-462b-9874-f54174d7ed24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ffcf5ea-99df-4911-94f0-066de2c74f16",
                    "LayerId": "f3d15960-41a7-497d-ab0a-d06fec83e832"
                }
            ]
        },
        {
            "id": "c0a51e0e-ab06-4796-9636-363a943ce67d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ba95298-38e3-4c25-b578-5e45ff6d14f5",
            "compositeImage": {
                "id": "e85c0f9e-9d70-43c6-82be-93fb758fcbf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0a51e0e-ab06-4796-9636-363a943ce67d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3db34479-3761-4a16-9bcd-be7cfe1a85fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0a51e0e-ab06-4796-9636-363a943ce67d",
                    "LayerId": "f3d15960-41a7-497d-ab0a-d06fec83e832"
                }
            ]
        },
        {
            "id": "0bbe5ef2-381c-4d5a-b5d3-d01b578a7b4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ba95298-38e3-4c25-b578-5e45ff6d14f5",
            "compositeImage": {
                "id": "4a3f994b-d97f-4d7f-8358-b3a261f21cc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bbe5ef2-381c-4d5a-b5d3-d01b578a7b4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44acfa3a-ff47-40c7-a821-369c1a43b5a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bbe5ef2-381c-4d5a-b5d3-d01b578a7b4b",
                    "LayerId": "f3d15960-41a7-497d-ab0a-d06fec83e832"
                }
            ]
        },
        {
            "id": "244a1c6f-a92a-4b9e-bbff-9568d01f7e3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ba95298-38e3-4c25-b578-5e45ff6d14f5",
            "compositeImage": {
                "id": "210ce146-c1cb-407a-a1de-bf8437d102d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "244a1c6f-a92a-4b9e-bbff-9568d01f7e3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "005969cd-3357-47ee-a9ca-7a079c7368b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "244a1c6f-a92a-4b9e-bbff-9568d01f7e3d",
                    "LayerId": "f3d15960-41a7-497d-ab0a-d06fec83e832"
                }
            ]
        },
        {
            "id": "60144b5a-11b7-44dd-a3b1-4044d663ba1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ba95298-38e3-4c25-b578-5e45ff6d14f5",
            "compositeImage": {
                "id": "49ee5c73-f36e-4e98-8de3-c15b44f71880",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60144b5a-11b7-44dd-a3b1-4044d663ba1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fb66e67-b920-4678-85af-81f76d08ac5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60144b5a-11b7-44dd-a3b1-4044d663ba1e",
                    "LayerId": "f3d15960-41a7-497d-ab0a-d06fec83e832"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f3d15960-41a7-497d-ab0a-d06fec83e832",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ba95298-38e3-4c25-b578-5e45ff6d14f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}