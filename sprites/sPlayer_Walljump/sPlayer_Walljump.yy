{
    "id": "7df9c3d8-fbb7-43ed-9b23-74a9e93afaad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer_Walljump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 19,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9dce84ff-9949-45f6-bfc8-d7f1cc66daa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7df9c3d8-fbb7-43ed-9b23-74a9e93afaad",
            "compositeImage": {
                "id": "868a312e-d2d0-4435-9934-62ebc9fa5d3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dce84ff-9949-45f6-bfc8-d7f1cc66daa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a262210f-d7c8-4bdd-83a9-11e4c79100d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dce84ff-9949-45f6-bfc8-d7f1cc66daa5",
                    "LayerId": "4678d9eb-5f81-44f2-889c-4ec74da229c2"
                }
            ]
        },
        {
            "id": "cc4b0a83-f739-46f0-a33c-44ba41083c3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7df9c3d8-fbb7-43ed-9b23-74a9e93afaad",
            "compositeImage": {
                "id": "86b41e2c-4403-4d28-89af-d9f74b4b6320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc4b0a83-f739-46f0-a33c-44ba41083c3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a59d3165-fec7-4941-88f9-c2334f92a489",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc4b0a83-f739-46f0-a33c-44ba41083c3a",
                    "LayerId": "4678d9eb-5f81-44f2-889c-4ec74da229c2"
                }
            ]
        },
        {
            "id": "885bac65-3659-44ab-973a-c3a597419347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7df9c3d8-fbb7-43ed-9b23-74a9e93afaad",
            "compositeImage": {
                "id": "dd7a325c-fa85-488a-aa50-cf519ba38b28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "885bac65-3659-44ab-973a-c3a597419347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2aae7da-1113-409c-80de-a78f5148289f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "885bac65-3659-44ab-973a-c3a597419347",
                    "LayerId": "4678d9eb-5f81-44f2-889c-4ec74da229c2"
                }
            ]
        },
        {
            "id": "fa665cd8-95eb-40d4-878b-567d87178551",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7df9c3d8-fbb7-43ed-9b23-74a9e93afaad",
            "compositeImage": {
                "id": "fd017720-5cc2-417d-9a81-1aebd3070134",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa665cd8-95eb-40d4-878b-567d87178551",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "269f8b91-c235-41dc-86c1-5ef06c5756b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa665cd8-95eb-40d4-878b-567d87178551",
                    "LayerId": "4678d9eb-5f81-44f2-889c-4ec74da229c2"
                }
            ]
        },
        {
            "id": "d208d7f9-3468-43c3-ba15-e8a0101b598d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7df9c3d8-fbb7-43ed-9b23-74a9e93afaad",
            "compositeImage": {
                "id": "f03f752a-58eb-4ee6-98e5-962d99721761",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d208d7f9-3468-43c3-ba15-e8a0101b598d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb70d9bf-f860-42ea-b96d-5ffe2cfed620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d208d7f9-3468-43c3-ba15-e8a0101b598d",
                    "LayerId": "4678d9eb-5f81-44f2-889c-4ec74da229c2"
                }
            ]
        },
        {
            "id": "112cf6e6-4183-421e-9651-fe167b3b550b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7df9c3d8-fbb7-43ed-9b23-74a9e93afaad",
            "compositeImage": {
                "id": "6bee1949-1f79-495a-b7c8-b39ed756d245",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "112cf6e6-4183-421e-9651-fe167b3b550b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e678e410-5dba-47d9-916c-7aa38712193f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "112cf6e6-4183-421e-9651-fe167b3b550b",
                    "LayerId": "4678d9eb-5f81-44f2-889c-4ec74da229c2"
                }
            ]
        },
        {
            "id": "1a27e72d-b9d2-4371-9b8a-bdc6df2e66c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7df9c3d8-fbb7-43ed-9b23-74a9e93afaad",
            "compositeImage": {
                "id": "9eee8b78-a26f-48b4-8fcc-9a31ac1999d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a27e72d-b9d2-4371-9b8a-bdc6df2e66c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f33cc6fc-c705-44d2-a0c6-c1a718a8c5f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a27e72d-b9d2-4371-9b8a-bdc6df2e66c8",
                    "LayerId": "4678d9eb-5f81-44f2-889c-4ec74da229c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4678d9eb-5f81-44f2-889c-4ec74da229c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7df9c3d8-fbb7-43ed-9b23-74a9e93afaad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}