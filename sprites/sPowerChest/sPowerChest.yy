{
    "id": "7b24afab-ef8d-4e33-8535-851c001e52dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPowerChest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e381dfef-0439-4104-aebb-a8a2a999de9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b24afab-ef8d-4e33-8535-851c001e52dd",
            "compositeImage": {
                "id": "84bd636c-c72e-4f90-92ad-188a96c63dc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e381dfef-0439-4104-aebb-a8a2a999de9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "124d4240-0fc3-4211-9905-8e76fea2826e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e381dfef-0439-4104-aebb-a8a2a999de9b",
                    "LayerId": "df7feca1-3e10-4557-a418-84205c5329e6"
                }
            ]
        },
        {
            "id": "646d40f0-3231-42df-8830-e53d25d72722",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b24afab-ef8d-4e33-8535-851c001e52dd",
            "compositeImage": {
                "id": "2eaad48e-8884-4451-a601-6b88ba5a2e02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "646d40f0-3231-42df-8830-e53d25d72722",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c9fb1ff-9bd9-4245-93a0-16aeed8b0cb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "646d40f0-3231-42df-8830-e53d25d72722",
                    "LayerId": "df7feca1-3e10-4557-a418-84205c5329e6"
                }
            ]
        },
        {
            "id": "b770563e-839c-40de-8096-695b0cefdcd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b24afab-ef8d-4e33-8535-851c001e52dd",
            "compositeImage": {
                "id": "34cec2d7-33c5-4d8f-bc78-d092704218d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b770563e-839c-40de-8096-695b0cefdcd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6daed647-992e-44ea-a8a0-0e7774622a80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b770563e-839c-40de-8096-695b0cefdcd1",
                    "LayerId": "df7feca1-3e10-4557-a418-84205c5329e6"
                }
            ]
        },
        {
            "id": "5b815b83-f469-4719-832d-9d09b8ad3564",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b24afab-ef8d-4e33-8535-851c001e52dd",
            "compositeImage": {
                "id": "4a4b6900-a981-429d-beb4-1793a339f5c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b815b83-f469-4719-832d-9d09b8ad3564",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d57d6d4-0a0c-4ae2-8de0-ef071167f5ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b815b83-f469-4719-832d-9d09b8ad3564",
                    "LayerId": "df7feca1-3e10-4557-a418-84205c5329e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "df7feca1-3e10-4557-a418-84205c5329e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b24afab-ef8d-4e33-8535-851c001e52dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}