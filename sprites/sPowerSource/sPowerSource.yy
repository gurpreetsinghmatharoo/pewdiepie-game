{
    "id": "481c5ad6-4f2c-4c6f-aef6-b564823fe581",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPowerSource",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 7,
    "bbox_right": 40,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e4ffe54-62ec-4357-a815-16d559b5705d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "481c5ad6-4f2c-4c6f-aef6-b564823fe581",
            "compositeImage": {
                "id": "5a7a8a69-4754-416c-8e39-95ae0ba3cedb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e4ffe54-62ec-4357-a815-16d559b5705d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71dc692e-1e24-4e48-bb3e-667173d917b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e4ffe54-62ec-4357-a815-16d559b5705d",
                    "LayerId": "3918746f-7f07-4994-b5f9-e290f67cc077"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "3918746f-7f07-4994-b5f9-e290f67cc077",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "481c5ad6-4f2c-4c6f-aef6-b564823fe581",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}