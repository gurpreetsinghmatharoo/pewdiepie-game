{
    "id": "d7c9ea15-e620-4cab-848c-e3bdb9d08ff3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPurpleGate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bb140e3-9693-445b-aec9-8097f3a9b12c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7c9ea15-e620-4cab-848c-e3bdb9d08ff3",
            "compositeImage": {
                "id": "b1f3256b-b176-43f2-83d3-b6ef0be20951",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bb140e3-9693-445b-aec9-8097f3a9b12c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12c943df-8372-4f12-9288-54eea1ab5836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bb140e3-9693-445b-aec9-8097f3a9b12c",
                    "LayerId": "de3adeaa-787f-4dfd-ac92-5a3c00f09fe5"
                }
            ]
        },
        {
            "id": "2c637939-5b08-4fc7-b139-870c42461da4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7c9ea15-e620-4cab-848c-e3bdb9d08ff3",
            "compositeImage": {
                "id": "b162455f-17e9-4c67-a578-57786bd577b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c637939-5b08-4fc7-b139-870c42461da4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a44c1393-3681-4989-856c-c32dc4ccd1b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c637939-5b08-4fc7-b139-870c42461da4",
                    "LayerId": "de3adeaa-787f-4dfd-ac92-5a3c00f09fe5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "de3adeaa-787f-4dfd-ac92-5a3c00f09fe5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7c9ea15-e620-4cab-848c-e3bdb9d08ff3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 0
}