{
    "id": "be5be2fd-ff51-431d-97ff-40e3a007a3e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPurpleKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d95e74fa-7419-47dd-8521-23efc84c6110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5be2fd-ff51-431d-97ff-40e3a007a3e7",
            "compositeImage": {
                "id": "88704ac2-8571-425d-8e3a-9e1dc61431ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d95e74fa-7419-47dd-8521-23efc84c6110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad591b3b-e9b2-4c36-b0d0-1fbc34eb2647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d95e74fa-7419-47dd-8521-23efc84c6110",
                    "LayerId": "21adfe06-6e25-4d4d-9086-233c23a5da67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "21adfe06-6e25-4d4d-9086-233c23a5da67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be5be2fd-ff51-431d-97ff-40e3a007a3e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}