{
    "id": "a513a291-b89b-4e14-8e02-b7392cf157ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShadowDude_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 24,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71b63b7d-64a8-436b-8861-5567737a3b4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a513a291-b89b-4e14-8e02-b7392cf157ce",
            "compositeImage": {
                "id": "016de9d0-7b68-4b87-af00-fe540707abc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b63b7d-64a8-436b-8861-5567737a3b4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea96b32a-323b-4c09-9d44-fd9f62ce28d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b63b7d-64a8-436b-8861-5567737a3b4b",
                    "LayerId": "d12e690a-9628-46f5-b193-d1bcb0796ebe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d12e690a-9628-46f5-b193-d1bcb0796ebe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a513a291-b89b-4e14-8e02-b7392cf157ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}