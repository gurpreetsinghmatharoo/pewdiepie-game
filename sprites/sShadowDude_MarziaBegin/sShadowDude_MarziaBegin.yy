{
    "id": "4befaa9c-ea02-465a-91b9-1d87c979ea3a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShadowDude_MarziaBegin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 29,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c47b444-0bd9-40a7-9143-96ae8b4b421a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4befaa9c-ea02-465a-91b9-1d87c979ea3a",
            "compositeImage": {
                "id": "1acd4f22-4d7b-476d-9bc3-e0c62ae348ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c47b444-0bd9-40a7-9143-96ae8b4b421a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9de2b83-685f-4c25-8045-2161d713f6dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c47b444-0bd9-40a7-9143-96ae8b4b421a",
                    "LayerId": "8545dada-9b11-4b35-bfb2-a5556e112594"
                }
            ]
        },
        {
            "id": "54e81515-6b23-4bc2-bf6d-38f1aee7e8bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4befaa9c-ea02-465a-91b9-1d87c979ea3a",
            "compositeImage": {
                "id": "42df93a6-ffc1-4c12-b0dd-9ff76ef58c43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54e81515-6b23-4bc2-bf6d-38f1aee7e8bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f0a29f2-2295-415c-8aab-c9b81dbdfa5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54e81515-6b23-4bc2-bf6d-38f1aee7e8bd",
                    "LayerId": "8545dada-9b11-4b35-bfb2-a5556e112594"
                }
            ]
        },
        {
            "id": "17ceb418-84c9-4bde-a05d-e1284ab173f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4befaa9c-ea02-465a-91b9-1d87c979ea3a",
            "compositeImage": {
                "id": "dde09301-00b1-4a15-9420-7c1b05f9d87f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17ceb418-84c9-4bde-a05d-e1284ab173f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "345cfc41-d15c-4036-8c21-a9f7bb7c9897",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17ceb418-84c9-4bde-a05d-e1284ab173f3",
                    "LayerId": "8545dada-9b11-4b35-bfb2-a5556e112594"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8545dada-9b11-4b35-bfb2-a5556e112594",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4befaa9c-ea02-465a-91b9-1d87c979ea3a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}