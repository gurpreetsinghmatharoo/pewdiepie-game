{
    "id": "141739dd-6baa-4a79-b5b8-02f277cbfa21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShadowDude_MarziaWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 27,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c677b8c0-ab16-4119-bd16-39fbb324dcf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141739dd-6baa-4a79-b5b8-02f277cbfa21",
            "compositeImage": {
                "id": "ba3b7d56-2d1b-478d-93f5-93647652b51e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c677b8c0-ab16-4119-bd16-39fbb324dcf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eae196e-edaa-4e31-a782-3d2d11f85b0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c677b8c0-ab16-4119-bd16-39fbb324dcf9",
                    "LayerId": "33349c38-1f95-4802-9333-a1a14520386e"
                }
            ]
        },
        {
            "id": "27583436-d628-4867-a304-052ff13bca4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141739dd-6baa-4a79-b5b8-02f277cbfa21",
            "compositeImage": {
                "id": "4437d27c-33da-4544-86c4-19837ecb3599",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27583436-d628-4867-a304-052ff13bca4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dc939cd-063c-4369-9550-5f16e814834d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27583436-d628-4867-a304-052ff13bca4b",
                    "LayerId": "33349c38-1f95-4802-9333-a1a14520386e"
                }
            ]
        },
        {
            "id": "4858d741-27fe-49f7-bd20-a68ea2a8e7be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141739dd-6baa-4a79-b5b8-02f277cbfa21",
            "compositeImage": {
                "id": "ccf36bd2-b316-49cd-bc72-ab9fd14a5400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4858d741-27fe-49f7-bd20-a68ea2a8e7be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5ea21f1-2bb6-4962-a8e6-6a9d359da01d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4858d741-27fe-49f7-bd20-a68ea2a8e7be",
                    "LayerId": "33349c38-1f95-4802-9333-a1a14520386e"
                }
            ]
        },
        {
            "id": "6366480f-9aa0-41a2-bb50-d1d15837e2be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141739dd-6baa-4a79-b5b8-02f277cbfa21",
            "compositeImage": {
                "id": "20a190a0-8df0-43e4-9173-01ebf290dda5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6366480f-9aa0-41a2-bb50-d1d15837e2be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "763dbd98-b48f-4838-87db-3eb1ccba9dd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6366480f-9aa0-41a2-bb50-d1d15837e2be",
                    "LayerId": "33349c38-1f95-4802-9333-a1a14520386e"
                }
            ]
        },
        {
            "id": "bd15213c-144a-4354-8b4e-f2e14fb6d925",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141739dd-6baa-4a79-b5b8-02f277cbfa21",
            "compositeImage": {
                "id": "984c9fd4-f448-4021-ae67-1facc65fcd25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd15213c-144a-4354-8b4e-f2e14fb6d925",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6502c8e0-64fa-4920-a641-d27932ee2c9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd15213c-144a-4354-8b4e-f2e14fb6d925",
                    "LayerId": "33349c38-1f95-4802-9333-a1a14520386e"
                }
            ]
        },
        {
            "id": "69e806e6-acb7-46e6-9fd3-48400f89d540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141739dd-6baa-4a79-b5b8-02f277cbfa21",
            "compositeImage": {
                "id": "563db0b3-5aeb-4a95-8b0c-862a66191142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69e806e6-acb7-46e6-9fd3-48400f89d540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c90ea44b-c0ff-4883-8afb-08388b2c0eea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69e806e6-acb7-46e6-9fd3-48400f89d540",
                    "LayerId": "33349c38-1f95-4802-9333-a1a14520386e"
                }
            ]
        },
        {
            "id": "8a5bcbd4-630a-4385-ac0f-25d2036117e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141739dd-6baa-4a79-b5b8-02f277cbfa21",
            "compositeImage": {
                "id": "83928c30-a309-4b0f-b809-916c34a5eded",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a5bcbd4-630a-4385-ac0f-25d2036117e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af0eb897-1a13-4895-b990-96805bf5ff2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a5bcbd4-630a-4385-ac0f-25d2036117e2",
                    "LayerId": "33349c38-1f95-4802-9333-a1a14520386e"
                }
            ]
        },
        {
            "id": "21b6d97f-8109-4eb7-9cf8-4c24bc43d6ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "141739dd-6baa-4a79-b5b8-02f277cbfa21",
            "compositeImage": {
                "id": "9c165494-4122-419a-9ea5-5624292d67cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21b6d97f-8109-4eb7-9cf8-4c24bc43d6ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a835e31-6bdc-4f26-ace2-30751418822c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21b6d97f-8109-4eb7-9cf8-4c24bc43d6ec",
                    "LayerId": "33349c38-1f95-4802-9333-a1a14520386e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "33349c38-1f95-4802-9333-a1a14520386e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "141739dd-6baa-4a79-b5b8-02f277cbfa21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}