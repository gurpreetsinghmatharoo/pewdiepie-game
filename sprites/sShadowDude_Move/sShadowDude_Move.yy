{
    "id": "d661002c-20b2-478e-acfe-89359cd2c86f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sShadowDude_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 24,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b4fda39-2bcb-49a1-a114-839d48ada339",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d661002c-20b2-478e-acfe-89359cd2c86f",
            "compositeImage": {
                "id": "661255f9-91aa-4adb-93a2-8ace5a291513",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b4fda39-2bcb-49a1-a114-839d48ada339",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7db32b06-fa10-493a-b0c2-4ee75f02e0fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b4fda39-2bcb-49a1-a114-839d48ada339",
                    "LayerId": "b05598c3-fe86-41c6-831a-9bc14c564039"
                }
            ]
        },
        {
            "id": "7374b73e-7639-4bdd-97c7-f333969b10e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d661002c-20b2-478e-acfe-89359cd2c86f",
            "compositeImage": {
                "id": "3f6344d2-41c8-4002-bdaf-1a7bc878eab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7374b73e-7639-4bdd-97c7-f333969b10e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0f41682-2e3a-4fa8-86fd-30ac6692333a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7374b73e-7639-4bdd-97c7-f333969b10e5",
                    "LayerId": "b05598c3-fe86-41c6-831a-9bc14c564039"
                }
            ]
        },
        {
            "id": "9825916b-c35e-49d1-83d1-0dc965d26547",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d661002c-20b2-478e-acfe-89359cd2c86f",
            "compositeImage": {
                "id": "e8d44466-e7ec-4ea3-a293-062477452e36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9825916b-c35e-49d1-83d1-0dc965d26547",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0dc8f0b-5c39-4243-8471-d6e4bb00b5a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9825916b-c35e-49d1-83d1-0dc965d26547",
                    "LayerId": "b05598c3-fe86-41c6-831a-9bc14c564039"
                }
            ]
        },
        {
            "id": "f98c379a-611e-46ea-913c-0cb843ae7d8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d661002c-20b2-478e-acfe-89359cd2c86f",
            "compositeImage": {
                "id": "2bfdbd65-7f03-45c4-b315-b1c833388095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f98c379a-611e-46ea-913c-0cb843ae7d8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d47357-6860-40fa-a7dd-3e28c9ffd5b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f98c379a-611e-46ea-913c-0cb843ae7d8f",
                    "LayerId": "b05598c3-fe86-41c6-831a-9bc14c564039"
                }
            ]
        },
        {
            "id": "4adb1349-a3a5-41e8-a060-52e3c1f2d9c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d661002c-20b2-478e-acfe-89359cd2c86f",
            "compositeImage": {
                "id": "eda9c9cd-3a43-44de-b339-e9bf2a1c4b58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4adb1349-a3a5-41e8-a060-52e3c1f2d9c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b14de2-20f0-4a8c-ab80-f9154d3559ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4adb1349-a3a5-41e8-a060-52e3c1f2d9c4",
                    "LayerId": "b05598c3-fe86-41c6-831a-9bc14c564039"
                }
            ]
        },
        {
            "id": "86673a8d-830c-44c5-abb8-6d6c6cf2df2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d661002c-20b2-478e-acfe-89359cd2c86f",
            "compositeImage": {
                "id": "4ba0a3c6-eb0b-4275-809c-d995b1b004c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86673a8d-830c-44c5-abb8-6d6c6cf2df2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd1518b5-1b9c-4187-b5d5-2f4dcc0329e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86673a8d-830c-44c5-abb8-6d6c6cf2df2e",
                    "LayerId": "b05598c3-fe86-41c6-831a-9bc14c564039"
                }
            ]
        },
        {
            "id": "a236268e-ddf2-4ba0-a8ba-363c53e159e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d661002c-20b2-478e-acfe-89359cd2c86f",
            "compositeImage": {
                "id": "09eac4ae-f9de-4070-804e-6a95863ad024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a236268e-ddf2-4ba0-a8ba-363c53e159e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1347b4c2-eaf0-4ff0-8a29-88e18aea618f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a236268e-ddf2-4ba0-a8ba-363c53e159e3",
                    "LayerId": "b05598c3-fe86-41c6-831a-9bc14c564039"
                }
            ]
        },
        {
            "id": "a26bb1ee-ffc7-41e1-85ba-977dc5310b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d661002c-20b2-478e-acfe-89359cd2c86f",
            "compositeImage": {
                "id": "36dfeddd-15be-47b7-8599-4cf5218a973c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a26bb1ee-ffc7-41e1-85ba-977dc5310b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "226ad2a0-0afb-4d87-8dac-a7024803767e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a26bb1ee-ffc7-41e1-85ba-977dc5310b6e",
                    "LayerId": "b05598c3-fe86-41c6-831a-9bc14c564039"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b05598c3-fe86-41c6-831a-9bc14c564039",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d661002c-20b2-478e-acfe-89359cd2c86f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}