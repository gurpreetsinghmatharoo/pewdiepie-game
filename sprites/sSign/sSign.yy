{
    "id": "e90b28f3-f1b9-4a00-839b-c9005dcd87a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c587a30-c8d4-4da7-9e68-8ac6cceedfa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e90b28f3-f1b9-4a00-839b-c9005dcd87a6",
            "compositeImage": {
                "id": "d183e547-b878-41d2-924a-79a13d3e6863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c587a30-c8d4-4da7-9e68-8ac6cceedfa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e867928-d062-415b-8224-4f31da987243",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c587a30-c8d4-4da7-9e68-8ac6cceedfa9",
                    "LayerId": "894669bd-ebd6-4ead-a212-b845dfb6a9d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "894669bd-ebd6-4ead-a212-b845dfb6a9d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e90b28f3-f1b9-4a00-839b-c9005dcd87a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}