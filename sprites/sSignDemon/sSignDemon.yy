{
    "id": "bc3d730b-02f5-4a61-bbd0-4568029091fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSignDemon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d288cb0-f8d2-4e7e-b726-1c183938bb2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3d730b-02f5-4a61-bbd0-4568029091fc",
            "compositeImage": {
                "id": "0b5e4208-7411-4f80-9783-6abedf8962e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d288cb0-f8d2-4e7e-b726-1c183938bb2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b03cb6f5-27e8-4105-8a8f-102f6c2c37f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d288cb0-f8d2-4e7e-b726-1c183938bb2a",
                    "LayerId": "75e04bcf-f125-4ec0-8299-0f48e8788765"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "75e04bcf-f125-4ec0-8299-0f48e8788765",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc3d730b-02f5-4a61-bbd0-4568029091fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}