{
    "id": "b5ac1ee1-5ab7-46a9-a033-eb9504cb4ed6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSignDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f95249f-2136-4d10-bf1b-9c8800431ef1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5ac1ee1-5ab7-46a9-a033-eb9504cb4ed6",
            "compositeImage": {
                "id": "4062ca5a-cc43-4856-b76d-47d99a67639f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f95249f-2136-4d10-bf1b-9c8800431ef1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a6085b2-0bc9-486f-ab78-bd4406f32a9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f95249f-2136-4d10-bf1b-9c8800431ef1",
                    "LayerId": "bdc5f9b0-843e-44ce-ba5d-d88cfb858d8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bdc5f9b0-843e-44ce-ba5d-d88cfb858d8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5ac1ee1-5ab7-46a9-a033-eb9504cb4ed6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}