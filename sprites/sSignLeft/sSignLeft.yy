{
    "id": "d824eede-2fce-4f1d-a375-7904a25bf7ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSignLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e174423-59a9-4a94-a335-14a8d82feee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d824eede-2fce-4f1d-a375-7904a25bf7ba",
            "compositeImage": {
                "id": "c2d3fa90-7f4c-4bd7-b3ac-4571d9dab0cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e174423-59a9-4a94-a335-14a8d82feee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12a2b4c5-29d1-4e84-ae82-834d12b3b099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e174423-59a9-4a94-a335-14a8d82feee5",
                    "LayerId": "4d3fd45d-b566-412f-90d5-0107df1a1f86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4d3fd45d-b566-412f-90d5-0107df1a1f86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d824eede-2fce-4f1d-a375-7904a25bf7ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}