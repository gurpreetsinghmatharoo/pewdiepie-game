{
    "id": "81b2017d-a01d-4492-a3db-14c28f359385",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSignRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afae333e-9dc7-4a7a-921e-364028519622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81b2017d-a01d-4492-a3db-14c28f359385",
            "compositeImage": {
                "id": "593c4758-5fb1-4d2f-acf4-c8825bb3e495",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afae333e-9dc7-4a7a-921e-364028519622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c59c82fd-a4b0-400d-97d8-ff4eff545b56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afae333e-9dc7-4a7a-921e-364028519622",
                    "LayerId": "e83159a8-963f-4996-a64d-de2f49f59998"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e83159a8-963f-4996-a64d-de2f49f59998",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81b2017d-a01d-4492-a3db-14c28f359385",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}