{
    "id": "29fce99a-c877-429d-9853-16fdfa7510b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSignYT",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23bc4e3d-7799-4e6d-a4f8-f4382a8b2026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29fce99a-c877-429d-9853-16fdfa7510b3",
            "compositeImage": {
                "id": "1444fc45-5e32-4474-94fd-9376a3a02859",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23bc4e3d-7799-4e6d-a4f8-f4382a8b2026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20ea479e-cec5-4abc-b4a3-752accbe3c7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23bc4e3d-7799-4e6d-a4f8-f4382a8b2026",
                    "LayerId": "c6bba80a-3660-4168-8b9d-8bad725e12e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c6bba80a-3660-4168-8b9d-8bad725e12e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29fce99a-c877-429d-9853-16fdfa7510b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}