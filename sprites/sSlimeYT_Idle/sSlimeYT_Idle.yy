{
    "id": "adea26f5-81b3-4820-b2a0-36dd6ac2590c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlimeYT_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f51af0be-5677-4729-a95a-1ec490700415",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adea26f5-81b3-4820-b2a0-36dd6ac2590c",
            "compositeImage": {
                "id": "84068a7c-ac91-4d98-85d1-36e0ea6e3767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f51af0be-5677-4729-a95a-1ec490700415",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1e259b2-ddac-4e67-b4c0-65e43e8db47a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f51af0be-5677-4729-a95a-1ec490700415",
                    "LayerId": "3cbb6088-ea97-41e6-988b-88501dd7b7d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3cbb6088-ea97-41e6-988b-88501dd7b7d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adea26f5-81b3-4820-b2a0-36dd6ac2590c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 26
}