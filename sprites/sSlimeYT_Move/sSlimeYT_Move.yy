{
    "id": "34905a4d-5adb-42a5-8f08-b5ea1b98a52e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSlimeYT_Move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85bd556e-173f-44eb-844a-621859b4fa1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34905a4d-5adb-42a5-8f08-b5ea1b98a52e",
            "compositeImage": {
                "id": "6863ef57-e6b9-4cc0-9286-b5ff5e17301d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85bd556e-173f-44eb-844a-621859b4fa1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15617b38-9d0a-4407-9805-ec6abbd2277d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85bd556e-173f-44eb-844a-621859b4fa1a",
                    "LayerId": "be125835-9167-4a26-a031-f6709cc61e18"
                }
            ]
        },
        {
            "id": "dddb4c0d-f53d-40f9-971e-e4447143b692",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34905a4d-5adb-42a5-8f08-b5ea1b98a52e",
            "compositeImage": {
                "id": "baf6377b-ffb0-4313-b9f4-4c388459250d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dddb4c0d-f53d-40f9-971e-e4447143b692",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bed8c59-3115-43b4-82f4-c62f5d206722",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dddb4c0d-f53d-40f9-971e-e4447143b692",
                    "LayerId": "be125835-9167-4a26-a031-f6709cc61e18"
                }
            ]
        },
        {
            "id": "c273a264-050e-4bf0-b225-13979134507d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34905a4d-5adb-42a5-8f08-b5ea1b98a52e",
            "compositeImage": {
                "id": "3d570ccf-371d-4c8b-80e2-f051aea46c39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c273a264-050e-4bf0-b225-13979134507d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3575c3e6-3e75-4e17-b4a6-65e980a14bec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c273a264-050e-4bf0-b225-13979134507d",
                    "LayerId": "be125835-9167-4a26-a031-f6709cc61e18"
                }
            ]
        },
        {
            "id": "2734194e-946c-4092-9a4f-06067a33061e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34905a4d-5adb-42a5-8f08-b5ea1b98a52e",
            "compositeImage": {
                "id": "74ec63aa-48d9-4e4c-a1a8-be353147ee6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2734194e-946c-4092-9a4f-06067a33061e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad582692-4694-4b10-bee9-a40b9d47e6b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2734194e-946c-4092-9a4f-06067a33061e",
                    "LayerId": "be125835-9167-4a26-a031-f6709cc61e18"
                }
            ]
        },
        {
            "id": "a7d92710-1f10-43df-975c-cf4234f90e2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34905a4d-5adb-42a5-8f08-b5ea1b98a52e",
            "compositeImage": {
                "id": "e656bd18-2ccf-4d74-9326-b50463c8d856",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7d92710-1f10-43df-975c-cf4234f90e2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed45c78a-cd9b-4cc5-b42c-64bd4f7238f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7d92710-1f10-43df-975c-cf4234f90e2b",
                    "LayerId": "be125835-9167-4a26-a031-f6709cc61e18"
                }
            ]
        },
        {
            "id": "dafc8dbd-bba2-485e-ba3d-99830e2ca087",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34905a4d-5adb-42a5-8f08-b5ea1b98a52e",
            "compositeImage": {
                "id": "0328c1b0-6fa9-4342-9a2c-9cebdb390035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dafc8dbd-bba2-485e-ba3d-99830e2ca087",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3228547d-8ce2-463b-88c0-de0cfe1c5d59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dafc8dbd-bba2-485e-ba3d-99830e2ca087",
                    "LayerId": "be125835-9167-4a26-a031-f6709cc61e18"
                }
            ]
        },
        {
            "id": "73d71580-497b-4694-ae40-a5874f87e034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34905a4d-5adb-42a5-8f08-b5ea1b98a52e",
            "compositeImage": {
                "id": "8e77dec5-4d77-4abd-9e25-e5662e66ca67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73d71580-497b-4694-ae40-a5874f87e034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "385f49c0-7f40-4ac5-aa94-91dd5eabae48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73d71580-497b-4694-ae40-a5874f87e034",
                    "LayerId": "be125835-9167-4a26-a031-f6709cc61e18"
                }
            ]
        },
        {
            "id": "95093452-b7e1-47f0-b95d-873878d608cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34905a4d-5adb-42a5-8f08-b5ea1b98a52e",
            "compositeImage": {
                "id": "4d22e90a-fcee-42a3-9797-d6f77805ec14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95093452-b7e1-47f0-b95d-873878d608cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92557dbb-689a-4c4b-8d28-5c1ed45150cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95093452-b7e1-47f0-b95d-873878d608cf",
                    "LayerId": "be125835-9167-4a26-a031-f6709cc61e18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "be125835-9167-4a26-a031-f6709cc61e18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34905a4d-5adb-42a5-8f08-b5ea1b98a52e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 26
}