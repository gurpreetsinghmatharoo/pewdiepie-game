{
    "id": "cc31db95-e21e-4eaf-88c1-31670f164a40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpawnPoint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64920d85-33b9-4a05-883e-de7580981840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc31db95-e21e-4eaf-88c1-31670f164a40",
            "compositeImage": {
                "id": "1b42d4a6-be97-426f-b34c-7400de4287e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64920d85-33b9-4a05-883e-de7580981840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdc0487f-6094-4ef4-8e0c-d7447359a4a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64920d85-33b9-4a05-883e-de7580981840",
                    "LayerId": "b4ae5bc7-5761-4bf2-9970-1987fb2417ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b4ae5bc7-5761-4bf2-9970-1987fb2417ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc31db95-e21e-4eaf-88c1-31670f164a40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}