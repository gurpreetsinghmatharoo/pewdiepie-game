{
    "id": "77318fa1-e283-46bd-811b-e65fa3d4fc25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTeleporter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66688463-078a-4d9b-9464-34ad4d797348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77318fa1-e283-46bd-811b-e65fa3d4fc25",
            "compositeImage": {
                "id": "fbaaeac7-54b4-4d38-96ae-d0009929fab5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66688463-078a-4d9b-9464-34ad4d797348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8409f562-b6ce-4b03-8116-bf8924cce348",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66688463-078a-4d9b-9464-34ad4d797348",
                    "LayerId": "285767f0-daad-43f6-a62a-f9e0676bc13e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "285767f0-daad-43f6-a62a-f9e0676bc13e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77318fa1-e283-46bd-811b-e65fa3d4fc25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}