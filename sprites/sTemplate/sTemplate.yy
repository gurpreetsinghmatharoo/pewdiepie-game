{
    "id": "038790e5-a731-487a-9460-f95aad4e38c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTemplate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 12,
    "bbox_right": 20,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6133f991-d859-48f3-8723-c57444f226a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "038790e5-a731-487a-9460-f95aad4e38c2",
            "compositeImage": {
                "id": "86cce878-9b29-4e2d-8a28-9de123f41169",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6133f991-d859-48f3-8723-c57444f226a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19c91b8a-6dee-462e-8469-8cfcdc9653a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6133f991-d859-48f3-8723-c57444f226a2",
                    "LayerId": "cdf098d4-bd6a-4450-ac61-930669f33e7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cdf098d4-bd6a-4450-ac61-930669f33e7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "038790e5-a731-487a-9460-f95aad4e38c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}