{
    "id": "7814e3d3-4a07-4dfc-96bf-52b7fcbd96d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTileBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 29,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ef29296-2154-4ca5-a55d-b00e9b58a921",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7814e3d3-4a07-4dfc-96bf-52b7fcbd96d3",
            "compositeImage": {
                "id": "fffdbe2b-e5f9-41e8-b991-a9aebd3e2501",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ef29296-2154-4ca5-a55d-b00e9b58a921",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f38ed092-c145-42b4-a8de-19002a753f0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ef29296-2154-4ca5-a55d-b00e9b58a921",
                    "LayerId": "f007a110-5b21-44f1-b196-b4f2a5f0e4fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f007a110-5b21-44f1-b196-b4f2a5f0e4fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7814e3d3-4a07-4dfc-96bf-52b7fcbd96d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}