{
    "id": "eedd4a1f-f72a-4102-bbfb-8b849ec4f1eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTileHouse1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 16,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9101d11-5b65-4e8a-858b-d8c42b6c3418",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eedd4a1f-f72a-4102-bbfb-8b849ec4f1eb",
            "compositeImage": {
                "id": "dcc4c1a4-e16c-48e8-9ba4-76e455e56880",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9101d11-5b65-4e8a-858b-d8c42b6c3418",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7f5ae3d-cfa6-46f2-adb7-446ba5e73419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9101d11-5b65-4e8a-858b-d8c42b6c3418",
                    "LayerId": "9630cc1e-8978-44db-afb9-656c2219bb23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9630cc1e-8978-44db-afb9-656c2219bb23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eedd4a1f-f72a-4102-bbfb-8b849ec4f1eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}