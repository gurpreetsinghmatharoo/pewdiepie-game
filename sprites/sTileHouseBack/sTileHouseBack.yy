{
    "id": "ce69689e-6a50-4928-9479-6c6f84a848f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTileHouseBack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 16,
    "bbox_right": 111,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e85e84e-1374-43f3-98e4-0c2be4341839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce69689e-6a50-4928-9479-6c6f84a848f0",
            "compositeImage": {
                "id": "789a484e-d6e8-406d-a754-5172606887b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e85e84e-1374-43f3-98e4-0c2be4341839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d133774-736f-4883-8dfc-18b5da3ca3d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e85e84e-1374-43f3-98e4-0c2be4341839",
                    "LayerId": "1e36485b-d1f9-4a24-b9c3-bea0e46a3a1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1e36485b-d1f9-4a24-b9c3-bea0e46a3a1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce69689e-6a50-4928-9479-6c6f84a848f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}