{
    "id": "aea5db95-0aab-477e-bc9d-a3d143569343",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTileHouseItems",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 16,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfb726ab-1953-4a3c-b735-1a697e283de5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aea5db95-0aab-477e-bc9d-a3d143569343",
            "compositeImage": {
                "id": "73f4fd76-2904-4c86-8071-7e48601b29d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfb726ab-1953-4a3c-b735-1a697e283de5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49dbd2d0-0485-47e5-a8e9-602373e74ba0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfb726ab-1953-4a3c-b735-1a697e283de5",
                    "LayerId": "8f690786-ac59-4213-84ed-5f3f9488333e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "8f690786-ac59-4213-84ed-5f3f9488333e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aea5db95-0aab-477e-bc9d-a3d143569343",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}