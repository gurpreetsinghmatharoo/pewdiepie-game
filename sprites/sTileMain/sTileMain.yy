{
    "id": "076757dd-acbc-47ae-b22b-eef1808c28f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTileMain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 16,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53aab12b-2116-4828-b2b1-1aa146b6864d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "076757dd-acbc-47ae-b22b-eef1808c28f9",
            "compositeImage": {
                "id": "b255812a-4458-4283-807b-2591f0c9540e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53aab12b-2116-4828-b2b1-1aa146b6864d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67fb45ed-118e-4072-933c-b4bdee18f02e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53aab12b-2116-4828-b2b1-1aa146b6864d",
                    "LayerId": "5b74d841-b396-4d0b-aa16-f98bad7d7cd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "5b74d841-b396-4d0b-aa16-f98bad7d7cd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "076757dd-acbc-47ae-b22b-eef1808c28f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}