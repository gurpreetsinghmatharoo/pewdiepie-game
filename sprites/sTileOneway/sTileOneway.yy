{
    "id": "1b7fb1d6-64e5-4743-9e29-263a1c46d6d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTileOneway",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 16,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d14f86f-9684-4044-bb0d-ecd96305fcbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b7fb1d6-64e5-4743-9e29-263a1c46d6d4",
            "compositeImage": {
                "id": "f3b4b8cc-4049-4a82-82c6-910b657e3cd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d14f86f-9684-4044-bb0d-ecd96305fcbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b65f2ef-b24d-44fc-aa0e-3492810c7da6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d14f86f-9684-4044-bb0d-ecd96305fcbc",
                    "LayerId": "35d8658d-0f78-4801-8027-9d6f6a04b889"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "35d8658d-0f78-4801-8027-9d6f6a04b889",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b7fb1d6-64e5-4743-9e29-263a1c46d6d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}