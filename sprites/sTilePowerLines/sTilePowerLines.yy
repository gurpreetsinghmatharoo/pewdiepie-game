{
    "id": "cf723da9-904b-44b4-870f-98ed790a9eea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTilePowerLines",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 19,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6551cd8-ffaf-4381-b780-8a3e979c90b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf723da9-904b-44b4-870f-98ed790a9eea",
            "compositeImage": {
                "id": "280b3dca-6ab4-41de-bb4a-c3b02061fb96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6551cd8-ffaf-4381-b780-8a3e979c90b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d0b5cee-03ce-4d9f-b36a-a8ee7faf34f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6551cd8-ffaf-4381-b780-8a3e979c90b4",
                    "LayerId": "28e52297-040e-42b8-bf7d-6f3a119e07bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "28e52297-040e-42b8-bf7d-6f3a119e07bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf723da9-904b-44b4-870f-98ed790a9eea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}