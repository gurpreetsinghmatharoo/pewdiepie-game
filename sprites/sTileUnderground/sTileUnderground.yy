{
    "id": "4026ccf9-f62f-4125-8b40-f0dbabe85623",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTileUnderground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 16,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7bb7f84-6f80-4451-9c67-59b1f105bd2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4026ccf9-f62f-4125-8b40-f0dbabe85623",
            "compositeImage": {
                "id": "61fe29c2-b743-403f-aa80-5b6148f2dd65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7bb7f84-6f80-4451-9c67-59b1f105bd2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f6fad9b-39d0-4eda-84fa-1d480668248f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7bb7f84-6f80-4451-9c67-59b1f105bd2a",
                    "LayerId": "91f51d51-0e14-4576-8b5d-418005190e15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "91f51d51-0e14-4576-8b5d-418005190e15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4026ccf9-f62f-4125-8b40-f0dbabe85623",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}