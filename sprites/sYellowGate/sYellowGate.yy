{
    "id": "78092670-6569-43c2-a1fa-40086801d85f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sYellowGate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4940dd80-5522-4ce4-8b65-08ef520ba969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78092670-6569-43c2-a1fa-40086801d85f",
            "compositeImage": {
                "id": "a332f601-4931-4a96-bb24-ad37dd1a8255",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4940dd80-5522-4ce4-8b65-08ef520ba969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4b5fc92-04c8-484d-ae4b-8f72cb970128",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4940dd80-5522-4ce4-8b65-08ef520ba969",
                    "LayerId": "fc41d98f-8e2d-47ed-bee4-e66364b75634"
                }
            ]
        },
        {
            "id": "7a4de1c7-59db-491d-8d09-d08c4c1410f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78092670-6569-43c2-a1fa-40086801d85f",
            "compositeImage": {
                "id": "873881d9-307e-4dea-a5e4-8d5718ac8ecc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a4de1c7-59db-491d-8d09-d08c4c1410f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bc3f328-a408-4f48-bbe7-38893e1fbe64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a4de1c7-59db-491d-8d09-d08c4c1410f5",
                    "LayerId": "fc41d98f-8e2d-47ed-bee4-e66364b75634"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "fc41d98f-8e2d-47ed-bee4-e66364b75634",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78092670-6569-43c2-a1fa-40086801d85f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 0
}