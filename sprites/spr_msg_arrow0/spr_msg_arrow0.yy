{
    "id": "243b2433-3283-4f48-91fa-9552bdcd9b9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_arrow0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db8f50b7-3ef0-4972-b661-f41546cce285",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "243b2433-3283-4f48-91fa-9552bdcd9b9e",
            "compositeImage": {
                "id": "4f99ee9a-cda3-43aa-88a3-637d9d41fcb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db8f50b7-3ef0-4972-b661-f41546cce285",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b57f8916-be0b-4a3a-a09b-ebbe50fcef19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db8f50b7-3ef0-4972-b661-f41546cce285",
                    "LayerId": "9b719aff-1afb-4c34-bfda-856ffddec160"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9b719aff-1afb-4c34-bfda-856ffddec160",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "243b2433-3283-4f48-91fa-9552bdcd9b9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}