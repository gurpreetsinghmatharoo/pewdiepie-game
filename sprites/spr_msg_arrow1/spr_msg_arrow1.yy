{
    "id": "0cfc22c5-bbe2-41d5-ba49-3c414dbabb20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_arrow1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba20707e-7739-4999-86aa-3acff7053d7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cfc22c5-bbe2-41d5-ba49-3c414dbabb20",
            "compositeImage": {
                "id": "6f804623-02cf-4b1d-87b3-67bb3b2b3c73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba20707e-7739-4999-86aa-3acff7053d7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "340afdbe-8e3d-4778-bc10-4d61f61263d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba20707e-7739-4999-86aa-3acff7053d7e",
                    "LayerId": "36d1a845-b109-4521-8cf8-97784df401cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "36d1a845-b109-4521-8cf8-97784df401cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cfc22c5-bbe2-41d5-ba49-3c414dbabb20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 0
}