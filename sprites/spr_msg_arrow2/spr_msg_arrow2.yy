{
    "id": "f417dd1b-5e5d-4d5c-b81f-3f08dc517dbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_arrow2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb20d83b-2afa-41f0-981f-0270df4b8da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f417dd1b-5e5d-4d5c-b81f-3f08dc517dbb",
            "compositeImage": {
                "id": "539f54a1-6d95-4afe-a4d4-34f469d1d97e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb20d83b-2afa-41f0-981f-0270df4b8da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "161b8aee-0d6d-4830-a3ac-7fafb1a96fcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb20d83b-2afa-41f0-981f-0270df4b8da1",
                    "LayerId": "47100df7-1a43-4cb0-b12e-f5728a2d43d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "47100df7-1a43-4cb0-b12e-f5728a2d43d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f417dd1b-5e5d-4d5c-b81f-3f08dc517dbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 23
}