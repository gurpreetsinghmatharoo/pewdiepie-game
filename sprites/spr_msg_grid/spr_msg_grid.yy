{
    "id": "58da1cb3-da6b-4b76-88b2-65e9c1cd8cce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_grid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 5,
    "bbox_right": 18,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3e85799-37c8-4753-9425-50b989c74bbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58da1cb3-da6b-4b76-88b2-65e9c1cd8cce",
            "compositeImage": {
                "id": "76ecc378-c602-4b1c-a332-a339f80dc97d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3e85799-37c8-4753-9425-50b989c74bbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf1c741d-2f18-4b15-87a7-5a5273ab9f26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3e85799-37c8-4753-9425-50b989c74bbf",
                    "LayerId": "1811ef4d-d6ca-46f9-b5ef-6473d7f9352c"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 24,
    "layers": [
        {
            "id": "1811ef4d-d6ca-46f9-b5ef-6473d7f9352c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58da1cb3-da6b-4b76-88b2-65e9c1cd8cce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}