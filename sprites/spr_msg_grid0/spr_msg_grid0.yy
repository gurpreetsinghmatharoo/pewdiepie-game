{
    "id": "25feb9ce-6f54-4851-b52e-0d4ec1f19635",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_grid0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2fe709ac-491f-4e1e-8bc7-80faf7c8049b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25feb9ce-6f54-4851-b52e-0d4ec1f19635",
            "compositeImage": {
                "id": "a19cdf3c-cf95-47fc-8d83-7678764774d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fe709ac-491f-4e1e-8bc7-80faf7c8049b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4189711e-afdc-4046-a8dc-2514b898279a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fe709ac-491f-4e1e-8bc7-80faf7c8049b",
                    "LayerId": "002f7666-fe24-4c91-8f48-071f30cb26cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "002f7666-fe24-4c91-8f48-071f30cb26cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25feb9ce-6f54-4851-b52e-0d4ec1f19635",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}