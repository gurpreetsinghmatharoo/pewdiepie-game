{
    "id": "b0d86c2d-399f-4908-9bf5-83c7428fb10f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_grid1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b402d585-72c8-4680-a107-b56ec8983dc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d86c2d-399f-4908-9bf5-83c7428fb10f",
            "compositeImage": {
                "id": "8cb0b24c-4ec0-4b25-ae84-2e6a0ba62f76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b402d585-72c8-4680-a107-b56ec8983dc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9838e7af-70ec-4e79-9eb8-2892c72544c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b402d585-72c8-4680-a107-b56ec8983dc6",
                    "LayerId": "c2ec0990-6f4e-4fb5-b5e2-f84bd196b1af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "c2ec0990-6f4e-4fb5-b5e2-f84bd196b1af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0d86c2d-399f-4908-9bf5-83c7428fb10f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}