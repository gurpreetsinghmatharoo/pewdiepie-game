{
    "id": "8777d0df-9b7f-4a27-a7a8-4ac6f754974b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_grid2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 17,
    "bbox_right": 78,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c03d5ca-ab4b-4f2b-b55b-3c7697afbe64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8777d0df-9b7f-4a27-a7a8-4ac6f754974b",
            "compositeImage": {
                "id": "5d2f7637-5e39-4a92-aef4-ea7b33d46c83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c03d5ca-ab4b-4f2b-b55b-3c7697afbe64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bd5a4c7-ad7b-4f52-af7b-c671d3d12eff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c03d5ca-ab4b-4f2b-b55b-3c7697afbe64",
                    "LayerId": "ee360fd2-f74c-4233-b7ba-c5214c89017f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "ee360fd2-f74c-4233-b7ba-c5214c89017f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8777d0df-9b7f-4a27-a7a8-4ac6f754974b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}