{
    "id": "c77327a4-1c94-4136-b702-5c4c0fcd8591",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_more",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "395bdeaf-a852-4722-abab-85f7731c4687",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c77327a4-1c94-4136-b702-5c4c0fcd8591",
            "compositeImage": {
                "id": "14f4b34a-8ecd-4a1d-80c9-9ec6022004a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "395bdeaf-a852-4722-abab-85f7731c4687",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97df9f1d-4909-43ab-aa6b-502174ef350b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "395bdeaf-a852-4722-abab-85f7731c4687",
                    "LayerId": "5f772478-1474-40a1-9ff4-a7184c699092"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5f772478-1474-40a1-9ff4-a7184c699092",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c77327a4-1c94-4136-b702-5c4c0fcd8591",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}