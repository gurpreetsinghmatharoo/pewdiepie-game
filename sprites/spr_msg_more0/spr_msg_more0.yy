{
    "id": "788cbcfd-3264-43b4-9a79-3371556e4e5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_more0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2487378-961d-43b0-90b3-321fcb131407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788cbcfd-3264-43b4-9a79-3371556e4e5c",
            "compositeImage": {
                "id": "17d92c2f-b6d2-4316-9a98-8f9b74becca4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2487378-961d-43b0-90b3-321fcb131407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce216b61-ef89-4ad5-9776-4fd3f3a01e41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2487378-961d-43b0-90b3-321fcb131407",
                    "LayerId": "52f9190f-a3b3-46d4-8c24-833b793f77a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "52f9190f-a3b3-46d4-8c24-833b793f77a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "788cbcfd-3264-43b4-9a79-3371556e4e5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}