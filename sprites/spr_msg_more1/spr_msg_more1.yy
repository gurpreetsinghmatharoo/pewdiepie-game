{
    "id": "712cdcbc-1456-4c57-8775-7130c27fe996",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_more1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7151e78-774a-4ab8-b7e3-d125bc41dd6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "712cdcbc-1456-4c57-8775-7130c27fe996",
            "compositeImage": {
                "id": "35bb0822-8633-4d6a-a8c1-37eb297ac82e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7151e78-774a-4ab8-b7e3-d125bc41dd6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c365ab3-4929-4b08-bc61-e19a19766813",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7151e78-774a-4ab8-b7e3-d125bc41dd6c",
                    "LayerId": "3859af51-1090-4e17-9511-3ee135eee479"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3859af51-1090-4e17-9511-3ee135eee479",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "712cdcbc-1456-4c57-8775-7130c27fe996",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}