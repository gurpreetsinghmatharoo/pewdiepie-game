{
    "id": "587895f5-f883-4ede-aba1-cc25668b1e21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_more2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62d9a891-59b7-4a76-ab41-f68d839fa4fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "587895f5-f883-4ede-aba1-cc25668b1e21",
            "compositeImage": {
                "id": "e8bd5d17-1583-4ef8-beb8-db70c1bf823c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62d9a891-59b7-4a76-ab41-f68d839fa4fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3de03ae3-2f9c-4d8b-9d7a-90cb875f7596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62d9a891-59b7-4a76-ab41-f68d839fa4fe",
                    "LayerId": "9e0ad503-391c-4e84-9003-a5444d0b1a38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9e0ad503-391c-4e84-9003-a5444d0b1a38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "587895f5-f883-4ede-aba1-cc25668b1e21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}